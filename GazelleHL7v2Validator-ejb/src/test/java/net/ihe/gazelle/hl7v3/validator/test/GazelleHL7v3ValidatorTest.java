package net.ihe.gazelle.hl7v3.validator.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.SAXParserFactory;

import net.ihe.gazelle.hl7v3.validator.core.GazelleHL7v3Validator;
import net.ihe.gazelle.hl7v3.validator.core.HL7V3ValidatorType;
import net.ihe.gazelle.hl7v3.validator.util.XMLValidation;
import net.ihe.gazelle.validation.DetailedResult;

public class GazelleHL7v3ValidatorTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader( new FileReader (new File("/home/aberge/workspace/GazelleHL7v2Validator/GazelleHL7v2Validator-ejb/src/test/resources/XCPD.Response.Multiple.xml")));
	    String line = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    String ls = System.getProperty("line.separator");

	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }
		String xsdLocation = "/home/aberge/Development/xsd/HL7V3/NE2008/multicacheschemas/PRPA_IN201306UV02.xsd";
		SAXParserFactory factory = XMLValidation.initializeFactory(xsdLocation);
	    GazelleHL7v3Validator validator = new GazelleHL7v3Validator(factory, xsdLocation);
	    DetailedResult result = validator.validate(stringBuilder.toString(), HL7V3ValidatorType.XCPD_QUERYRESPONSE);
	    if (result != null){
	    	System.out.println(GazelleHL7v3Validator.getDetailedResultAsString(result));
	    }else{
	    	System.out.println("result is null");
	    }
	}

}
