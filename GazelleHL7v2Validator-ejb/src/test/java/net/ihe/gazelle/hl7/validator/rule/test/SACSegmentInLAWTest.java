/**
 * 
 */
package net.ihe.gazelle.hl7.validator.rule.test;

import ca.uhn.hl7v2.model.Message;
import net.ihe.gazelle.hl7.validator.core.IHEValidationContext;
import net.ihe.gazelle.hl7.validator.rules.law.SACSegmentInLAW;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * @author aberge
 * 
 */
public class SACSegmentInLAWTest {

	private static final IHEValidationContext validationContext = IHEValidationContext.SINGLETON;
	private static SACSegmentInLAW rule = new SACSegmentInLAW();

	@Test
	public void testRetrieveRuleForSAC3InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-3").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC4InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-4").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC10InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER[0]/SAC/SAC-10").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC11InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-11").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC13InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-13").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC14InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-14").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC15InOML() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.178",
                "OML_O33/SPECIMEN/SPECIMEN_CONTAINER/SAC/SAC-15").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC3InOUL() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.182",
                "OUL_R22/SPECIMEN/CONTAINER/SAC/SAC-3").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC4InOUL() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.182",
                "OUL_R22/SPECIMEN/CONTAINER/SAC/SAC-4").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC3InORL() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.181",
                "ORL_O34/RESPONSE/PATIENT/SPECIMEN/SAC/SAC-3").isEmpty());
	}

	@Test
	public void testRetrieveRuleForSAC4InORL() {
		assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.181",
                "ORL_O34/RESPONSE/PATIENT/SPECIMEN/SAC/SAC-4").isEmpty());
	}

	@Test
	public void test_OUL_SAC3Populated() {
		String messageToValidate = "MSH|^~\\&|ALIS_AM|ALIS_HOSPITAL|OM_LAB_OF|IHE|20130409012808||OUL^R22^OUL_R22|196|P|2.5\r"
				+ "PID|||41^^^ALIS_HOSPITAL^PI||SOYLU^SERTAN||19790211|M||||||||||3096707^^^ALIS_HOSPITAL\r"
				+ "PV1||O|||||||||||||||||12^^^ALIS_HOSPITAL||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|100065||BLD|||||||P\r"
				+ "SAC|||2401040202^OP^OID^ISO\r"
				+ "OBR|1|77804|77804|85027^BLOOD COUNT^LN||||||||||||^SERTAN|||||||||R\r"
				+ "ORC|SC|77804|||||||20130409012808\r"
				+ "OBX|1|ST|85027^BLOOD COUNT^LN||5||||||R|||20130409012343||ynt^ILHAN^GURKAN\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/SAC[0]/SAC-3"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OUL_SAC4Populated() {
		String messageToValidate = "MSH|^~\\&|ALIS_AM|ALIS_HOSPITAL|OM_LAB_OF|IHE|20130409012808||OUL^R22^OUL_R22|196|P|2.5\r"
				+ "PID|||41^^^ALIS_HOSPITAL^PI||SOYLU^SERTAN||19790211|M||||||||||3096707^^^ALIS_HOSPITAL\r"
				+ "PV1||O|||||||||||||||||12^^^ALIS_HOSPITAL||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|100065||BLD|||||||P\r"
				+ "SAC||||2401040202^OP^OID^ISO\r"
				+ "OBR|1|77804|77804|85027^BLOOD COUNT^LN||||||||||||^SERTAN|||||||||R\r"
				+ "ORC|SC|77804|||||||20130409012808\r"
				+ "OBX|1|ST|85027^BLOOD COUNT^LN||5||||||R|||20130409012343||ynt^ILHAN^GURKAN\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/SAC[0]/SAC-3"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OUL_SAC3AndSAC4_Populated() {
		String messageToValidate = "MSH|^~\\&|ALIS_AM|ALIS_HOSPITAL|OM_LAB_OF|IHE|20130409012808||OUL^R22^OUL_R22|196|P|2.5\r"
				+ "PID|||41^^^ALIS_HOSPITAL^PI||SOYLU^SERTAN||19790211|M||||||||||3096707^^^ALIS_HOSPITAL\r"
				+ "PV1||O|||||||||||||||||12^^^ALIS_HOSPITAL||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|100065||BLD|||||||P\r"
				+ "SAC|||2401040202^OP^OID^ISO|123\r"
				+ "OBR|1|77804|77804|85027^BLOOD COUNT^LN||||||||||||^SERTAN|||||||||R\r"
				+ "ORC|SC|77804|||||||20130409012808\r"
				+ "OBX|1|ST|85027^BLOOD COUNT^LN||5||||||R|||20130409012343||ynt^ILHAN^GURKAN\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/SAC[0]/SAC-4"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OUL_SAC3AndSAC4_Empty() {
		String messageToValidate = "MSH|^~\\&|ALIS_AM|ALIS_HOSPITAL|OM_LAB_OF|IHE|20130409012808||OUL^R22^OUL_R22|196|P|2.5\r"
				+ "PID|||41^^^ALIS_HOSPITAL^PI||SOYLU^SERTAN||19790211|M||||||||||3096707^^^ALIS_HOSPITAL\r"
				+ "PV1||O|||||||||||||||||12^^^ALIS_HOSPITAL||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|100065||BLD|||||||P\r"
				+ "SAC|||\r"
				+ "OBR|1|77804|77804|85027^BLOOD COUNT^LN||||||||||||^SERTAN|||||||||R\r"
				+ "ORC|SC|77804|||||||20130409012808\r"
				+ "OBX|1|ST|85027^BLOOD COUNT^LN||5||||||R|||20130409012343||ynt^ILHAN^GURKAN\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No exception", rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/SAC[0]/SAC-4"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * test SAC segment is properly extracted from OML message
	 */
	@Test
	public void test_OML_SAC3Populated() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-4"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * test SAC segment is properly extracted from ORL message
	 */
	@Test
	public void test_ORL_SAC3Populated() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|AM_Systelab_MODULAB|Systelab|20130403153542||ORL^O34^ORL_O34|20130403153542|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "MSA|AA|LAB281364996142326\r"
				+ "PID|1||NHC212215||NEBOT^DANIELUX^^^^^L||19710706120000+0100|M|||Calle pex, 7^^SABADELL^BARCELONA^08203^ESP||^^^^^^625154737||||PRE2013ICU004|||||||||C||||N\r"
				+ "SPM|1|2403040201&OP&OID&ISO^53&IHE_OM_ANALYZER&1.3.6.1.4.1.12559.11.1.2.2.4.5&ISO||SER^SUERO^2.16.840.1.113883.12.70|||||||P^Patient specimen^1.3.6.1.4.1.21367.100.1|||||||||N||||||1\r"
				+ "SAC|||2403040201^OP^OID^ISO\r"
				+ "ORC|CR|||PRE2013POG005&OP&OID&ISO|CA||||20130403123525+0200||||||||||||Anestesia^^^^^^FI^^^NORTH_HOSPITAL\r"
				+ "OBR|1|2237.1.107^OP^OID^ISO||LAB91^Colesterol total^1.3.6.1.4.1.21367.100.1||||||||||||5303^BAJWA^AMNA^^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised",
					rule.test(message, "ORL_O34/RESPONSE[0]/PATIENT[0]/SPECIMEN[0]/SAC[0]/SAC-3"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_badComponentPath() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|AM_Systelab_MODULAB|Systelab|20130403153542||ORL^O34^ORL_O34|20130403153542|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "MSA|AA|LAB281364996142326\r"
				+ "PID|1||NHC212215||NEBOT^DANIELUX^^^^^L||19710706120000+0100|M|||Calle pex, 7^^SABADELL^BARCELONA^08203^ESP||^^^^^^625154737||||PRE2013ICU004|||||||||C||||N\r"
				+ "SPM|1|2403040201&OP&OID&ISO^53&IHE_OM_ANALYZER&1.3.6.1.4.1.12559.11.1.2.2.4.5&ISO||SER^SUERO^2.16.840.1.113883.12.70|||||||P^Patient specimen^1.3.6.1.4.1.21367.100.1|||||||||N||||||1\r"
				+ "SAC|||2403040201^OP^OID^ISO\r"
				+ "ORC|CR|||PRE2013POG005&OP&OID&ISO|CA||||20130403123525+0200||||||||||||Anestesia^^^^^^FI^^^NORTH_HOSPITAL\r"
				+ "OBR|1|2237.1.107^OP^OID^ISO||LAB91^Colesterol total^1.3.6.1.4.1.21367.100.1||||||||||||5303^BAJWA^AMNA^^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception",
					rule.test(message, "ORL_O34/RESPONSE[0]/PATIENT[0]/SPECIMEN[0]/CONTAINER[0]/SAC[0]/SAC-3"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-3 == null && SAC-4 != null && SAC-13 == null
	 */
	@Test
	public void test_OML_SAC10MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Rack2\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-10"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-3 == null && SAC-4 != null && SAC-10 == null
	 */
	@Test
	public void test_OML_SAC13MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||Tray1\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-13"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-3 == null && SAC-4 != null && SAC-13 == null
	 */
	@Test
	public void test_OML_SAC10MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-10"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-3 == null && SAC-4 != null && SAC-10 == null
	 */
	@Test
	public void test_OML_SAC13MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-13"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC10NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-10"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC13NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-13"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC10NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||||123||||Rack3|||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-10"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC13NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||||||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-13"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC11MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Rack1|1||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-11"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC11MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Rack1|||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-11"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC11NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-11"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void est_OML_SAC11NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||1||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-11"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC14MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Rack1|1||Tray0|1\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-14"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC14MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Rack1|||Tray0\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-14"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC14NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-14"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC14NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||1|||1\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-14"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-10 != null && SAC-13 == null
	 */
	@Test
	public void test_OML_SAC15RequiredOrEmptyAndPresent_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO||||||Carrier1|1||||Carrier1-1\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-15"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * SAC-10 == null && SAC-13 != null
	 */
	@Test
	public void test_OML_SAC15RequiredOrEmptyAndPresent_2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||Tray1|1|Tray1-1\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-15"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC15NotSupportedAndNotPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-15"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC15NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||redFiole\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-15"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC29GoodNum2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE~LAW_CONTAINER\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||redFiole||||||||||||||^^^456|\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNull("Exception raised", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-29"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC29WrongNum2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||redFiole||||||||||||||^^^1|\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-29"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC29NullNum2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||redFiole||||||||||||||^1^^|\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-29"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void test_OML_SAC29NullMalFormattedNum2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
				+ "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
				+ "SAC||||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||redFiole||||||||||||||^2^^|\r"
				+ "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			assertNotNull("No Exception", rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-29"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
