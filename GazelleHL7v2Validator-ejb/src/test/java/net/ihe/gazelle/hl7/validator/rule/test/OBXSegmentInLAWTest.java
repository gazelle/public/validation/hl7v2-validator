package net.ihe.gazelle.hl7.validator.rule.test;

import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.rules.law.OBXSegmentInLAW;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.model.Message;

public class OBXSegmentInLAWTest {

	OBXSegmentInLAW rule = new OBXSegmentInLAW();

	private static Logger log = LoggerFactory.getLogger(OBXSegmentInLAWTest.class);

	/**
	 * OBX-2 = SN OBX-11 = F OBX-5 populated --> OK OBX-6 populated --> OK
	 */
	@Test
	public void test_OBX2ValuedWithNM() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-2"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-5"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	/**
	 * OBX-2 = ST OBX-11 = D OBX-5 populated --> KO OBX-6 populated --> KO
	 */
	@Test
	public void test_OBX2ValuedWithST() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|ST|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||D|\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-2"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-5"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	/**
	 * OBX-2 = SN --> KO (since OBX-5 empty) OBX-11 = F OBX-5 not populated --> KO OBX-6 not populated --> KO
	 */
	@Test
	public void test_OBX2ValuedWithSN() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|SN|123-1231^1.2.3^LN|||||L|||F|\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-2"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-5"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	/**
	 * OBX-2 not populated --> OK (since OBX-5 not populated) OBX-11 = D OBX-5 not populated --> OK OBX-6 not populated --> OK
	 */
	@Test
	public void test_OBX2Empty() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1||123-1231^1.2.3^LN|||||L|||D|\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-2"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-5"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}

	/**
	 * 1st OBX: OBX-2 empty --> OK (since OBX-5 populated) OBX-11 = F OBX-5 populated --> OK OBX-6 not populated --> OK 
	 */
	@Test
	public void test_OBX() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r"
					+ "OBX|2|NM|123-1231^1.2.3^LN||123^1.2.3^LN|||L|||F|\r" 
					+ "OBX|3|ST|123-1231^1.2.3^LN|||||L|||F|\r"
					+ "OBX|4|ST|123-1231^1.2.3^LN|||cm||L|||F|\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void test_OBX18() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|||||||Model^Manufacturer^123-456^ISO~SerialNumber^Manufacturer^123-456^ISO~Titi^Toto\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[0]/EI-3"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[0]/EI-4"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[1]/EI-3"));
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[1]/EI-4"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[2]/EI-3"));
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-18[2]/EI-4"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}
	
	/**
	 * Predicate for OBX-8-3 is that OBX-8-1 is not NULL
	 * 1 - OBX-8-1 = "" and OBX-8-3 is empty => true
	 * 2 - OBX-8-1 = "" and OBX-8-3 is populated => false
	 * 3 - OBX-8-1 is valued and OBX-8-3 is empty => false
	 * 1 - OBX-8-1 is valued and OBX-8-3 is populated => true
	 */
	@Test
	public void test_OBX8Predicate1() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||\"\"|||F|||||||Model^Manufacturer^123-456^ISO~SerialNumber^Manufacturer^123-456^ISO~Titi^Toto\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-8[0]/CWE-3"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}
	
	@Test
	public void test_OBX8Predicate2() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||\"\"^null^NULL|||F|||||||Model^Manufacturer^123-456^ISO~SerialNumber^Manufacturer^123-456^ISO~Titi^Toto\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-8[0]/CWE-3"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}
	
	@Test
	public void test_OBX8Predicate3() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L^low|||F|||||||Model^Manufacturer^123-456^ISO~SerialNumber^Manufacturer^123-456^ISO~Titi^Toto\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-8[0]/CWE-3"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}
	
	@Test
	public void test_OBX8Predicate4() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L^low^HL70078|||F|||||||Model^Manufacturer^123-456^ISO~SerialNumber^Manufacturer^123-456^ISO~Titi^Toto\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-8[0]/CWE-3"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			Assert.fail("Cannot parse message");
		}
	}
}
