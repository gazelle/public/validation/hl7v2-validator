package net.ihe.gazelle.hl7.validator.test;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.conf.ProfileException;
import ca.uhn.hl7v2.conf.parser.ProfileParser;
import ca.uhn.hl7v2.conf.spec.RuntimeProfile;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.hl7.validation.context.ValidationType;
import net.ihe.gazelle.hl7.validator.core.GazelleValidator;
import net.ihe.gazelle.hl7.validator.core.ResourceStoreFactory;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Test {

 	
	private static Logger log = LoggerFactory.getLogger(Test.class);

	public static void main(String[] args) throws IOException, HL7Exception {
        System.setProperty(DOMImplementationRegistry.PROPERTY, org.apache.xerces.dom.DOMImplementationSourceImpl.class.getCanonicalName());
//		test_1();
//		test_2();
		validateAllPAMFR();
//        testGroupROL();
	}


	private static String getProfileIdentifier(String subFolder, String subEventFolder){

		String profileIdentifier = null;
		if(subEventFolder.equals("ADT_A01")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.1";
		}else if(subEventFolder.equals("ADT_A02")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.2";
		}else if(subEventFolder.equals("ADT_A03")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.3";
		}else if(subEventFolder.equals("ADT_A04")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.4";
		}else if(subEventFolder.equals("ADT_A05")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.5";
		}else if(subEventFolder.equals("ADT_A06")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.6";
		}else if(subEventFolder.equals("ADT_A07")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.7";
		}else if(subEventFolder.equals("ADT_A08")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.8";
		}else if(subEventFolder.equals("ADT_A09")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.9";
		}else if(subEventFolder.equals("ADT_A10")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.10";
		}else if(subEventFolder.equals("ADT_A11")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.11";
		}else if(subEventFolder.equals("ADT_A12")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.12";
		}else if(subEventFolder.equals("ADT_A13")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.13";
		}else if(subEventFolder.equals("ADT_A14")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.14";
		}else if(subEventFolder.equals("ADT_A15")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.15";
		}else if(subEventFolder.equals("ADT_A16")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.16";
		}else if(subEventFolder.equals("ADT_A21")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.17";
		}else if(subEventFolder.equals("ADT_A22")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.18";
		}else if(subEventFolder.equals("ADT_A25")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.20";
		}else if(subEventFolder.equals("ADT_A26")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.21";
		}else if(subEventFolder.equals("ADT_A27")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.22";
		}else if(subEventFolder.equals("ADT_A28")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.23";
		}else if(subEventFolder.equals("ADT_A31")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.24";
		}else if(subEventFolder.equals("ADT_A32")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.25";
		}else if(subEventFolder.equals("ADT_A33")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.26";
		}else if(subEventFolder.equals("ADT_A38")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.28";
		}else if(subEventFolder.equals("ADT_A40") && subFolder.equals("PES")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.29";
		}else if(subEventFolder.equals("ADT_A40")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.30";
		}else if(subEventFolder.equals("ADT_A44")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.31";
		}else if(subEventFolder.equals("ADT_A47")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.32";
		}else if(subEventFolder.equals("ADT_A52")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.33";
		}else if(subEventFolder.equals("ADT_A53")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.34";
		}else if(subEventFolder.equals("ADT_A54")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.35";
		}else if(subEventFolder.equals("ADT_A55")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.36";
		}else if(subEventFolder.equals("ADT_Z80")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.37";
		}else if(subEventFolder.equals("ADT_Z81")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.38";
		}else if(subEventFolder.equals("ADT_Z82")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.39";
		}else if(subEventFolder.equals("ADT_Z83")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.40";
		}else if(subEventFolder.equals("ADT_Z84")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.41";
		}else if(subEventFolder.equals("ADT_Z85")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.42";
		}else if(subEventFolder.equals("ADT_Z86")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.43";
		}else if(subEventFolder.equals("ADT_Z87")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.44";
		}else if(subEventFolder.equals("ADT_Z88")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.45";
		}else if(subEventFolder.equals("ADT_Z89")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.46";
		}else if(subEventFolder.equals("ADT_Z99")){
			profileIdentifier = "2.16.840.1.113883.2.8.3.1.47";
		}

		return profileIdentifier;

	}

	private static void validateAllPAMFR() {

		log.info("--- STARTING validating ALL PAM FR messages ---");
		String packageName = "net.ihe.gazelle.pam.fr.hl7v2.model.v25.message";

		String repertoireString = (System.getenv("GZL_HL7_VALIDATOR_ETC")==null?"/home/aberge/workspace/Data/HL7MessageProfiles/IHE_FR/":System.getenv("GZL_HL7_VALIDATOR_ETC"));

		File repertoire = new File(repertoireString);
		String[] eventFolders = null;
		String[] transactionFolders = null;
		String[] folders = repertoire.list();
		for(String subFolder : folders){
			transactionFolders = new File(repertoireString+subFolder).list();
			for(String subTransactionFolder : transactionFolders){
				eventFolders =  new File(repertoireString+subFolder+"/"+subTransactionFolder).list();
				if (eventFolders==null) {
					log.error(repertoireString+subFolder+"/"+subTransactionFolder);
				} else {
					for(String subEventFolder : eventFolders) {
						File samples = new File(repertoireString + subFolder + "/" + subTransactionFolder + "/" + subEventFolder + "/sample");
						if (samples.isDirectory()) {
							String[] files = samples.list();
							for (String fname : files) {
								if (fname.endsWith("ok.hl7")) {
									String sampleLink = repertoireString + subFolder + "/" + subTransactionFolder + "/" + subEventFolder + "/sample/" + fname;
									log.debug(sampleLink);
									String profilePath = repertoireString + subFolder + "/" + subTransactionFolder + "/" + subEventFolder + "/profile/" + subEventFolder + ".xml";
									String profileIdentifier = getProfileIdentifier(subFolder, subEventFolder);
									try {
										String messageToValidate = new String(Files.readAllBytes(Paths.get(sampleLink)));
										messageToValidate = messageToValidate.replace('\n', '\r');
										validate(messageToValidate, profilePath, profileIdentifier, packageName, true);
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				}
			}
		}

	}

	public static void testGroupROL() throws IOException, HL7Exception {
	    String profilePath = "/home/aberge/workspace/Data/HL7MessageProfiles/IHE_FR/PES/ITI-31/ADT_A01/profile/ADT_A01.xml";
	    String profileIdentifier = "2.16.840.1.113883.2.8.3.1.1";
	    String sampleLink = "/home/aberge/Development/HL7Tests/adta01-2ROL.txt";
        String packageName = "net.ihe.gazelle.pam.fr.hl7v2.model.v25.message";
        String messageToValidate = new String(Files.readAllBytes(Paths.get(sampleLink)));
        messageToValidate = messageToValidate.replace('\n','\r');
        validate(messageToValidate, profilePath, profileIdentifier, packageName, true);
    }

	public static void test_2() {
		log.info("--- STARTING test_2 ---");
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|ABBOTT_IVD|ABBOTT|20130617085951||OML^O33^OML_O33|Message-1000|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
				+ "SPM|1|||SER|||||||P\r"
				+ "SAC|||PATIENT-0001\r"
				+ "ORC|NW||||||||20130617085951\r"
				+ "OBR||ORDER#1000||241^Anti-Hcv^ABBOTT_IVD\r";

		String profilePath = "/home/xfs/Dev/Gazelle/Data/HL7MessageProfiles/ANALYZER_MGR/LAB-28/OML_O33/profile/OML_O33.xml";
		String profileIdentifier = "1.3.6.1.4.12559.11.1.1.178";
		String packageName = "net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message";
		validate(messageToValidate, profilePath, profileIdentifier, packageName, true);
//
//		try {
//			Message omlMessage = PipeParser.getInstanceWithNoValidation().parseForSpecificPackage(messageToValidate,
//					packageName);
//			
//		} catch (HL7Exception e) {
//			log.error(e.getMessage(), e);
//		}
	}

	public static void test_1() {
		log.info("--- STARTING test_1 ---");
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
				+ "PID|||||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
				+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
				+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
				+ "SAC|||SPID1\r"
				+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
				+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20121207120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBR||258^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|33^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB39^aPTT^1.3.6.1.4.1.21367.100.1||||||||||||6303^LOPEZ^GABRIELLA^^^DR|^^PH^^^^58745||||||||F|||6303^LOPEZ^GABRIELLA^^^DR\r"
				+ "ORC|SC|258^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|33^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1871^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|6303^LOPEZ^GABRIELLA^^^DR||6303^LOPEZ^GABRIELLA^^^DR|||||922229-30^IHE-CARD^1.3.6.1.4.1.21367.100.1\r"
				+ "TQ1|||||||20121207120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
				+ "OBX|1|NM|14749-6^LAW Glucose [Moles/Volume], Serum/Plasma^LN||5|mmol/L|||||F||C|20130326120000||21235^Davies^Siân^L^^Dr|||20130326120000\r";
		String profilePath = "/home/aberge/workspace/Data/HL7MessageProfiles/ANALYSER/LAB-29/OUL_R22/profile/OUL_R22.xml";
		String profileIdentifier = "1.3.6.1.4.12559.11.1.1.182";
		String packageName = "net.ihe.gazelle.laboratory.law.hl7v2.model.v25.message.OUL_R22";
		log.info(messageToValidate);
		validate(messageToValidate, profilePath, profileIdentifier, packageName, true);
	}

	private static void validate(String messageToValidate, String profilePath, String profileIdentifier,
			String packageName, boolean displayReports) {
		try {
			PipeParser parser = PipeParser.getInstanceWithNoValidation();
			Message message = null;
            System.out.println(profilePath);
			if(packageName == null || profilePath.contains("A08")){
				message = parser.parse(messageToValidate);
			}else {
				message = parser.parseForSpecificPackage(messageToValidate, packageName);
			}


			// Message message = parser.parseForSpecificPackage(messageToValidate, packageName);
			HL7v2ValidationReport reporter = new HL7v2ValidationReport();
			reporter.setResults(new ValidationResults(ValidationType.WARNING, ValidationType.WARNING, ValidationType.ERROR, ValidationType.ERROR));
			GazelleValidator validator = new GazelleValidator(new ResourceStoreFactory(), reporter.getResults());
			File profileFile = new File(profilePath);

			FileInputStream fis = new FileInputStream(profileFile);
			byte[] buffer = new byte[(int) profileFile.length()];
			fis.read(buffer);
			fis.close();

			ProfileParser profileParser = new ProfileParser(false);
			RuntimeProfile runtimeProfile = null;
			try {
				runtimeProfile = profileParser.parse(new String(buffer));
				runtimeProfile.getMessage().setIdentifier(profileIdentifier);
			} catch (ProfileException e) {
				log.error("Cannot parse profile at location " + profilePath);
				e.printStackTrace();
				System.exit(-1);
			}
			validator.validate(message, runtimeProfile.getMessage(), runtimeProfile.getHL7Version());
//			System.out.println(reporter.toString());
			System.out.println("ERRORS = " + reporter.getResults().getErrorCounter());
			System.out.println("WARNING = " + reporter.getResults().getWarningCounter());
			System.out.println("EXCEPTION = " + reporter.getResults().getExceptionCounter());
			System.out.println("NOTES = " + reporter.getResults().getReportCounter());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
