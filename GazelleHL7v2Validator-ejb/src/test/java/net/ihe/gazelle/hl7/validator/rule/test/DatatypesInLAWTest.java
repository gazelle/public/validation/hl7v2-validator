/*
 * Apache2 license - Copyright (c) IHE-Europe 2015
 *
 */

package net.ihe.gazelle.hl7.validator.rule.test;


import ca.uhn.hl7v2.model.Message;
import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.rules.law.DatatypesInLAW;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * <b>Class Description : </b>DatatypesInLAWTest<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/06/15
 *
 *
 *
 */
public class DatatypesInLAWTest {

    private static Logger log = Logger.getLogger(DatatypesInLAWTest.class);

    DatatypesInLAW rule = new DatatypesInLAW();

    /**
     * in SAC-24: CE-3 is required if CE-1 is present, X otherwise
     * <p/>
     * 1. CE-1 populated && CE-3 present ==> true
     * 2. CE-1 populated && CE-3 empty ==> false
     * 3. CE-1 !populated && CE-3 present ==> false
     * 4. CE-1 !populated && CE-3 empty ==> true
     */

    @Test
    public void testSAC24Case1() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("SAC-24",
                    rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-24[0]/CE-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSAC24Case2() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("SAC-24",
                    rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-24[0]/CE-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSAC24Case3() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("SAC-24",
                    rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-24[0]/CE-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSAC24Case4() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||^millilitre|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("SAC-24",
                    rule.test(message, "OML_O33/SPECIMEN[0]/SPECIMEN_CONTAINER[0]/SAC[0]/SAC-24[0]/CE-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * in OBR-16: XCN-9-3 is required if XCN-9-2 is present, X otherwise
     * <p/>
     * 1. XCN-9-2 populated && XCN-9-3 present ==> true
     * 2. XCN-9-2 populated && XCN-9-3 empty ==> false
     * 3. XCN-9-2 !populated && XCN-9-3 present ==> false
     * 4. XCN-9-2 !populated && XCN-9-3 empty ==> true
     */

    @Test
    public void testOBR16Case1() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR^^^Gazelle&1.2.3.4&ISO||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("OBR-16",
                    rule.test(message, "OML_O33/SPECIMEN[0]/ORDER[0]/OBSERVATION_REQUEST[0]/OBR[0]/OBR-16[0]/XCN-9/HD-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBR16Case2() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR^^^Gazelle&1.2.3.4||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("OBR-16",
                    rule.test(message, "OML_O33/SPECIMEN[0]/ORDER[0]/OBSERVATION_REQUEST[0]/OBR[0]/OBR-16[0]/XCN-9/HD-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBR16Case3() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR^^^Gazelle&&ISO||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("OBR-16",
                    rule.test(message, "OML_O33/SPECIMEN[0]/ORDER[0]/OBSERVATION_REQUEST[0]/OBR[0]/OBR-16[0]/XCN-9/HD-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBR16Case4() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152756||OML^O33^OML_O33|20130507152756|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-28^IHE\r"
                    + "PID|||DDS-43212^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                    + "PV1||E||||||5101^NELL^FREDERICK^P^^DR|||||||||||2456^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|4017&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^2605&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|456789&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO|AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1|Source_Site^Text^System|Site_Modifier^Text^System||P|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1|container_type^text^system\r"
                    + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||||||||||||||||||mL^millilitre^UCUM|\r"
                    + "ORC|NW|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||4019^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                    + "TQ1|||||||20130507120000||A^ASAP^1.3.6.1.4.1.21367.100.1\r"
                    + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO||LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR^^^Gazelle||||||||||||7101^GLASER^GREGORY^P^^DR\r"
                    + "TCD|battery_usi^text^sytem|^1^:|^1^:^3|^1^:^4|^1^:^5|N|N|O^Original^HL70389||15|D11^text^system";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("OBR-16",
                    rule.test(message, "OML_O33/SPECIMEN[0]/ORDER[0]/OBSERVATION_REQUEST[0]/OBR[0]/OBR-16[0]/XCN-9/HD-3"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * in OBX-6 CE-3 must be populated if CE-1 is populated
     * <p/>
     * 1. CE-1 populated && CE-3 present ==> true
     * 2. CE-1 populated && CE-3 empty ==> false
     * 3. CE-1 !populated && CE-3 present ==> false
     * 4. CE-1 !populated && CE-3 empty ==> true
     */

    /**
     * in OBX-3 CE-5 must be populated if CE-4 is populated
     * <p/>
     * 1. CE-5 populated && CE-4 present ==> true
     * 2. CE-5 populated && CE-4 empty ==> false
     * 3. CE-5 !populated && CE-4 present ==> false
     * 4. CE-5 !populated && CE-4 empty ==> true
     */

    /**
     * in OBX-3 CE-6 must be populated if CE-4 is populated
     * <p/>
     * 1. CE-6 populated && CE-4 present ==> true
     * 2. CE-6 populated && CE-4 empty ==> false
     * 3. CE-6 !populated && CE-4 present ==> false
     * 4. CE-6 !populated && CE-4 empty ==> true
     */

    @Test
    public void testOBX6AndOBX3Case1() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6/CE-3"));
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-5"));
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-6"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBX6AndOBX3Case2() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateId||123^1.2.3^LN|cm^centimetre||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6/CE-3"));
            Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-5"));
            Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-6"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBX6AndOBX3Case3() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^^alternateText^alternateCodeSystem||123^1.2.3^LN|^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("OBX-6", rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6/CE-3"));
            Assert.assertNotNull("OBX-3/CE-5", rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-5"));
            Assert.assertNotNull("OBX-3/CE-6", rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-6"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testOBX6AndOBX3Case4() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|^centimetre||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-6/CE-3"));
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-5"));
            Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/ORDER[0]/RESULT[0]/OBX[0]/OBX-3/CE-6"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * in SPM-3-1 & SPM-2-1 Either EI-2 or both EI-3 and EI-4 are required. EI-2, EI-3 and EI-4 can be populated at the same time
     * <p/>
     * 1. none of the 3 sub-component are present ==> false
     * 2. EI-2 populated && EI3 and EI-4 empty ==> true
     * 3. EI-2 !populated && EI-3 and EI-4 present ==> true
     * 4. EI-4 populated && EI-3 empty ==> false
     * 5. EI-3 populated && EI-4 empty ==> false
     */

    @Test
    public void testSPM3AndSPM2Case1() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868^1774|12345|SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("SPM-3", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-3[0]/EIP-1/EI-2"));
            Assert.assertNotNull("SPM-2", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-2[0]/EIP-1/EI-2"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSPM3AndSPM2Case2() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&namespaceID|12345&NamespaceID|SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("SPM-3", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-3[0]/EIP-1/EI-2"));
            Assert.assertNull("SPM-2", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-2[0]/EIP-1/EI-2"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSPM3AndSPM2Case3() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&&universalID&ISO|12345&&universalID&ISO|SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("SPM-3", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-3[0]/EIP-1/EI-2"));
            Assert.assertNull("SPM-2", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-2[0]/EIP-1/EI-2"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSPM3AndSPM2Case4() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&&&ISO|12345&&&ISO|SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("SPM-3", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-3[0]/EIP-1/EI-2"));
            Assert.assertNotNull("SPM-2", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-2[0]/EIP-1/EI-2"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSPM3AndSPM2Case5() {
        try {
            String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
                    + "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
                    + "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
                    + "SPM|1|1868&&universalID|12345&Gazelle&universalID|SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
                    + "SAC|||||SPID1\r"
                    + "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
                    + "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
                    + "OBX|1|NM|123-1231^1.2.3^LN^alternateID^alternateText^alternateCodeSystem||123^1.2.3^LN|cm^centimetre^UCUM||L|||F|\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("SPM-3", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-3[0]/EIP-1/EI-2"));
            Assert.assertNotNull("SPM-2", rule.test(message, "OUL_R22/SPECIMEN[0]/SPM[0]/SPM-2[0]/EIP-1/EI-2"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
