/*
 * Apache2 license - Copyright (c) IHE-Europe 2015
 *
 */

package net.ihe.gazelle.hl7.validator.rule.test;

import ca.uhn.hl7v2.model.Message;
import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.rules.law.ORLO34RulesInLAW;
import org.junit.Test;

/**
 * <b>Class Description : </b>ORLO34RulesInLAWTest<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/06/15
 *
 *
 *
 */
public class ORLO34RulesInLAWTest {

    ORLO34RulesInLAW rule = new ORLO34RulesInLAW();

    /**
     * ERR segment is required when MSA-1 != AA,not supported otherwise
     * <p/>
     * 1. MSA-1 = AA && ERR present ==> false
     * 2. MSA-1 = AA && ERR empty ==> true
     * 3. MSA-1 = AE && ERR present ==> true
     * 4. MSA-1 = AE && ERR empty ==> false
     */

    /**
     * RESPONSE group is required or empty when MSA-1 = AA,not supported otherwise
     * <p/>
     * 1. MSA-1 = AA && RESPONSE present ==> true
     * 2. MSA-1 = AA && RESPONSE empty ==> true
     * 3. MSA-1 = AE && RESPONSE present ==> false
     * 4. MSA-1 = AE && RESPONSE empty ==> true
     */

    @Test
    public void testERRCase1() {
        try {
            String messageToValidate =
                    "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152758||ORL^O34^ORL_O42|20130507152758|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
                            + "MSA|AA|20130507152756\r"
                            + "ERR|testEERRSegment\r"
                            + "PID|||DDS-43212^^^DDS&amp;1.3.6.1.4.1.12559.11.1.4.1.2&amp;ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                            + "SPM|1|4017&amp;IHE_OM_OP&amp;1.3.6.1.4.1.12559.11.1.2.2.4.2&amp;ISO^2605&amp;IHE_OM_OF&amp;1.3.6.1.4.1.12559.11.1.2.2.4.3&amp;ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
                            + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO\r"
                            + "ORC|OK|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|56^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|4019&amp;IHE_OM_OP&amp;1.3.6.1.4.1.12559.11.1.2.2.4.2&amp;ISO|SC||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                            + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("ERR", rule.test(message, "ORL_O42/ERR[0]"));
            Assert.assertNull("RESPONSE", rule.test(message, "ORL_O42/RESPONSE[0]"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testERRCase2() {
        try {
            String messageToValidate =
                    "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152758||ORL^O34^ORL_O42|20130507152758|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
                            + "MSA|AA|20130507152756\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("ERR", rule.test(message, "ORL_O42/ERR[0]"));
            Assert.assertNull("RESPONSE", rule.test(message, "ORL_O42/RESPONSE[0]"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testERRCase3() {
        try {
            String messageToValidate =
                    "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152758||ORL^O34^ORL_O42|20130507152758|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
                            + "MSA|AE|20130507152756\r"
                            + "ERR|testEERRSegment\r"
                            + "PID|||DDS-43212^^^DDS&amp;1.3.6.1.4.1.12559.11.1.4.1.2&amp;ISO^PI||Headrick^Isiah^^^^^L|Sturdivant^^^^^^M|19820501025300|M|||West Commonwealth Avenue^^Fullerton^California^92832^USA||||||||||||||||||||N\r"
                            + "SPM|1|4017&amp;IHE_OM_OP&amp;1.3.6.1.4.1.12559.11.1.2.2.4.2&amp;ISO^2605&amp;IHE_OM_OF&amp;1.3.6.1.4.1.12559.11.1.2.2.4.3&amp;ISO||AMN^Amniotic fluid^2.16.840.1.113883.12.70|||LNV^Line, Venous^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||INJ^Injury Hazard^1.3.6.1.4.1.21367.100.1|20130507120000|20130507120000||N||||||1\r"
                            + "SAC|||4018^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO\r"
                            + "ORC|OK|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|56^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|4019&amp;IHE_OM_OP&amp;1.3.6.1.4.1.12559.11.1.2.2.4.2&amp;ISO|SC||||20130507120000|7101^ESTRADA^JAIME^P^^DR||7101^ESTRADA^JAIME^P^^DR|||||C^XX SVS Connectathon Code^1.3.6.1.4.1.21367.100.1\r"
                            + "OBR||264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|264^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|LAB156^Gram Stain^1.3.6.1.4.1.21367.100.1||||||||||||7101^ESTRADA^JAIME^P^^DR||||||||||||7101^GLASER^GREGORY^P^^DR\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNull("ERR", rule.test(message, "ORL_O42/ERR[0]"));
            Assert.assertNotNull("RESPONSE", rule.test(message, "ORL_O42/RESPONSE[0]"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testERRCase4() {
        try {
            String messageToValidate =
                    "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152758||ORL^O34^ORL_O42|20130507152758|P|2.5.1||||||UNICODE UTF-8|||LAB-28^IHE\r"
                            + "MSA|AE|20130507152756\r";
            Message message = TestUtil.parseStringMessage(messageToValidate);
            Assert.assertNotNull("ERR", rule.test(message, "ORL_O42/ERR[0]"));
            Assert.assertNull("RESPONSE", rule.test(message, "ORL_O42/RESPONSE[0]"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
