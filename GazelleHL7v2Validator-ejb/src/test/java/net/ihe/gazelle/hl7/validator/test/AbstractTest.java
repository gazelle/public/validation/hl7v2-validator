package net.ihe.gazelle.hl7.validator.test;

import java.io.File;
import java.io.FileInputStream;

import net.ihe.gazelle.hl7.validation.context.ValidationType;
import net.ihe.gazelle.hl7.validator.core.GazelleValidator;
import net.ihe.gazelle.hl7.validator.core.ResourceStoreFactory;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResults;
import net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.conf.ProfileException;
import ca.uhn.hl7v2.conf.parser.ProfileParser;
import ca.uhn.hl7v2.conf.spec.RuntimeProfile;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;

public abstract class AbstractTest {

	public static final ValidationType ERROR = ValidationType.ERROR;
	public static final ValidationType IGNORE = ValidationType.IGNORE;
	private static Logger log = LoggerFactory.getLogger(AbstractTest.class);
	private String ignore;

	protected HL7v2ValidationReport validate(String packageName, String messageToValidate, String profilePath) {
		try {
			PipeParser parser = PipeParser.getInstanceWithNoValidation();
			Message message = null;
			if (packageName == null) {
				message = parser.parse(messageToValidate);
			} else {
				message = parser.parseForSpecificPackage(messageToValidate, packageName);
			}
			HL7v2ValidationReport reporter = new HL7v2ValidationReport();
			reporter.setOverview(new ValidationResultsOverview("Test"));
			ValidationResults results = new ValidationResults(IGNORE, IGNORE, ERROR, ERROR);
			reporter.setResults(results);
			GazelleValidator validator = new GazelleValidator(new ResourceStoreFactory(), reporter.getResults());
			File profileFile = new File(profilePath);

			FileInputStream fis = new FileInputStream(profileFile);
			byte[] buffer = new byte[(int) profileFile.length()];
			fis.read(buffer);
			fis.close();

			ProfileParser profileParser = new ProfileParser(false);
			RuntimeProfile runtimeProfile = null;
			try {
				runtimeProfile = profileParser.parse(new String(buffer));
				runtimeProfile.getMessage().setIdentifier("test");
			} catch (ProfileException e) {
				log.error("Cannot parse profile at location " + profilePath);
				e.printStackTrace();
				System.exit(-1);
			}
			validator.validate(message, runtimeProfile.getMessage(), runtimeProfile.getHL7Version());
			// successful validation (no error/no warning)
			if (reporter.getResults().getErrorCounter() == 0) {
				reporter.getOverview().setValidationStatus(net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED);
			} else {
				reporter.getOverview().setValidationStatus(net.ihe.gazelle.hl7.validator.report.ValidationStatus.FAILED);
			}
			return reporter;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
