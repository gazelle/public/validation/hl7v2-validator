package net.ihe.gazelle.hl7.validator.test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class RevisionDetectionTest {
	
	
	private static Logger log = LoggerFactory.getLogger(RevisionDetectionTest.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File directory = new File("/home/aberge/workspace/Data/HL7MessageProfiles/ProfilesPerOid");
		if (directory.isDirectory()) {
			for (String profilePath : directory.list(filter)) {
				File profileFile = new File(directory, profilePath);
				try{
				String revision = getRevisionNumber(profileFile.getCanonicalPath());
				System.out.println(profilePath + " -> " + profileFile.getCanonicalPath() + " : " + revision);
				}catch(Exception e){
					log.error(e.getMessage(), e);
				}
			}
		}else{
			log.error("Cannot read directory");
		}

	}
	
	private static FilenameFilter filter = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".xml");
		}
	};
	
	private static String getRevisionNumber(String filepath) throws Exception {
		Process process = Runtime.getRuntime().exec(new String[] { "svn", "info", "--xml", filepath });
		InputStream outcome = process.getInputStream();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document svninfo = builder.parse(new InputSource(outcome));
		NodeList commitNode = svninfo.getElementsByTagName("entry");
		if (commitNode.getLength() > 0) {
			NamedNodeMap attributes = commitNode.item(0).getAttributes();
			return attributes.getNamedItem("revision").getTextContent();

		} else {
			return null;
		}
	}

}
