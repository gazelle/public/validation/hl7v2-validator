package net.ihe.gazelle.i18n;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

public class Msg {

    private static final boolean missingResourceHelper = System.getProperty("missing.resource.completion.path") != null;
    private static class PackageBundles extends HashMap<Locale,ResourceBundle> {}
    private static final Map<Package, PackageBundles> bundles = new HashMap<>();

    private static ResourceBundle getResourceBundle(Locale locale, Class<?> cls) {
        final Package pck = cls.getPackage();
        PackageBundles pb = bundles.get(pck);
        if (pb==null) {
            pb = new PackageBundles();
            bundles.put(pck,pb);
        }
        ResourceBundle b = pb.get(locale);
        if (b == null) {
            try {
                b = ResourceBundle.getBundle(pck.getName() + ".messages", locale, new UTF8Control());
                pb.put(locale, b);
            } catch (final MissingResourceException mre) {
                return null;
            }
        }
        return b;
    }

    private static final Map<Class<?>, Msg> messages = new HashMap<Class<?>, Msg>() {
        private static final long serialVersionUID = 1L;

        @Override
        public Msg get(final Object key) {
            final Class<?> c = (Class<?>) key;
            Msg m = super.get(c);
            if (m == null) {
                m = new Msg(c);
                this.put(c, m);
            }
            return m;
        }
    };

    public static Msg getMessages(final Class<?> cls) {
        return messages.get(cls);
    }

    private final Class<?> cls;

    private Msg(final Class<?> cls) {
        super();
        this.cls = cls;
    }

    private String getFormat(Locale locale, Class<?> cls, final String key) {
        ResourceBundle bundle = getResourceBundle(locale,cls);
        final String prefix = cls.getSimpleName() + ".";
        if (bundle != null && bundle.containsKey(prefix + key)) {
            return bundle.getString(prefix + key);
        } else if (bundle != null && bundle.containsKey(key)) {
            return bundle.getString(key);
        } else if (cls.getSuperclass() != null) {
            return this.getFormat(locale, cls.getSuperclass(), key);
        } else {
            return null;
        }
    }

    public String get(final String id, final Object... parameters) {
        return get(Locale.getDefault(),id,parameters);
    }
    public String get(Locale locale, final String id, final Object... parameters) {
        String frmt;
        try {
            frmt = this.getFormat(locale, cls, id);
            if (frmt == null) {
                frmt = this.getDefaultFormat(locale, cls, id, parameters);
            }
        } catch (final Throwable t) {
            frmt = this.getDefaultFormat(locale, cls, id, parameters);
        }
        return MessageFormat.format(frmt, parameters);
    }

    private String getDefaultFormat(Locale locale, Class<?> cls, final String id, final Object... parameters) {
        final StringBuilder sb = new StringBuilder(id);
        if (parameters != null) {
            sb.append(" ");
            for (int i = 0; i < parameters.length; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("{");
                sb.append(i);
                sb.append("}");
            }
        }
        if (missingResourceHelper) {
            this.missingHelper(getResourceBundle(locale,cls), cls, id, sb.toString());
        }
        return sb.toString();
    }

    private void missingHelper(final ResourceBundle bundle, final Class<?> cls, final String id, final String frmt) {
        final File missingFileCompletionDirectory = new File(System.getProperty("missing.resource.completion.path"));
        if (!(missingFileCompletionDirectory.exists() && (!missingFileCompletionDirectory.isDirectory() || !missingFileCompletionDirectory.canWrite()))) {
            final String key = cls.getSimpleName() + "." + id;
            final File resourceDirectory = new File(missingFileCompletionDirectory, cls.getPackage().getName().replace(".", "/"));
            if (!resourceDirectory.exists()) {
                resourceDirectory.mkdirs();
            }
            final File resource = new File(resourceDirectory, "messages_" + Locale.getDefault().getLanguage() + ".properties");
            BufferedWriter output = null;
            try {
                output = new BufferedWriter(new FileWriter(resource, true));
                if (!resource.exists() && bundles.get(cls.getPackage()) != null) {
                    for (final String k : bundle.keySet()) {
                        output.write(k);
                        output.write("=");
                        output.write(bundle.getString(k));
                        output.newLine();
                    }
                } else {
                    if (resource.exists()) {
                        final Properties p = new Properties();
                        p.load(new FileInputStream(resource));
                        if (p.containsKey(key)) {
                            return;
                        }
                    }
                }
                output.write(key);
                output.write("=");
                output.write(frmt);
                output.newLine();
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (final IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } else {
            // on ne peut pas écrire dans le répertoire spécifié : abandon
        }

    }

}
