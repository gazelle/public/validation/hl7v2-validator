/*
 * Apache2 license - Copyright (c) IHE-Europe 2015
 *
 */

package net.ihe.gazelle.hl7.validator.rules.law;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.group.ORL_O42_RESPONSE;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.ORL_O42;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description : </b>ORLO34RulesInLAW<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/06/15
 *
 *
 *
 */
public class ORLO34RulesInLAW extends IHEValidationRule {

    protected static final String AA = "AA";
    private static Logger log = LoggerFactory.getLogger(ORLO34RulesInLAW.class);

    @Override
    public int getSeverity() {
        return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
    }

    @Override
    public ValidationException[] test(Message msg) {
        return null;
    }

    @Override
    public ValidationException[] test(Message msg, String hl7path) {
        String path = formatHl7Path(hl7path);
        Terser terser = new Terser(msg);
        try {
            if (hl7path.contains("ERR")) {
                testERRSegment(terser, path);
            } else if (hl7path.contains("RESPONSE")) {
                testRESPONSEGroup(msg);
            }
        } catch (ValidationException e) {
            return new ValidationException[]{e};
        }
        return null;
    }

    private void testRESPONSEGroup(Message message) throws ValidationException {
        if (message instanceof ORL_O42) {
            ORL_O42 orlO42 = (ORL_O42) message;
            ORL_O42_RESPONSE response = orlO42.getRESPONSE();
            try {
                String msa1 = orlO42.getMSA().getMsa1_AcknowledgmentCode().encode();
                if (!msa1.equals(AA) && !response.isEmpty()) {
                    throw new ValidationException("Usage of RESPONSE group is forbidden when MSA-1 is valued with " + msa1);
                }
            } catch (HL7Exception e) {
                log.debug(e.getMessage(), e);
            }
        }
    }

    private void testERRSegment(Terser terser, String path) throws ValidationException {
        try {
            Segment err = terser.getSegment(path.substring(BEGIN_INDEX));
            Segment msa = terser.getSegment("/MSA(0)");
            String msa1 = msa.getField(1, 0).encode();
            if (msa1.equals(AA) && !err.isEmpty()) {
                throw new ValidationException("Usage of ERR segment is forbidden when MSA-1 is valued with AA");
            } else if (!msa1.equals(AA) && err.isEmpty()) {
                throw new ValidationException("Usage of ERR segment is required when MSA-1 is valued with " + msa1);
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage(), e);
        }
    }

    @Override
    public String getDescription() {
        return "Test conditional segments/groups in ORL^O34^ORL_O42 messages";
    }

    @Override
    public String getSectionReference() {
        return "PaLM TF-2b, Rev. 9.0 - Table 3.28.4.1.2.2-1: ORL^O34";
    }
}
