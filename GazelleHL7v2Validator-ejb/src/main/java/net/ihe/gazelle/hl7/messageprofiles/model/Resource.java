/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.model;

//JPA imports

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.ihe.gazelle.hl7.messageprofiles.api.ResourceApi;
import net.ihe.gazelle.hl7.validator.core.OIDGenerator;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.hql.HQLQueryBuilder;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
//import org.jboss.seam.resteasy.ResourceQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description : </b>Resource<br>
 * <br>
 * This class describes the Resource object. This class belongs to the InriaHL7MessageProfileRepository module.
 * <p>
 * Resource possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Resource</li>
 * <li><b>oid</b> : oid corresponding to the Resource - oid root is 1.3.6.1.4.12559.11.1.3.1</li>
 * <li><b>content</b> : XML file containing the description of the resource</li>
 * </ul>
 * </br> *
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, November 30
 */

@Entity
@Name("hl7Resource")
@Table(name = "hl7_resource", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "oid"))
@SequenceGenerator(name = "hl7_resource_sequence", sequenceName = "hl7_resource_id_seq", allocationSize = 1)
public class Resource extends CommonProperties implements java.io.Serializable, Comparable<Resource>, ResourceApi {

    private static Logger log = LoggerFactory.getLogger(Resource.class);

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 8584096795711547372L;

    // Attributes (existing in database as a Column)

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hl7_resource_sequence")
    private Integer id;

    @Column(name = "hl7_version")
    private String hl7Version;

    @Column(name = "comment")
    private String comment;

    /**
     * used to sort the tables when a profile is linked to several tables the table with the "stronger" weight is first processed and then the decreasing order is used
     */
    @Column(name = "weight")
    private Integer weight;

    @ManyToMany
    @JoinTable(name = "hl7_profile_resource", joinColumns = @JoinColumn(name = "resource_id"), inverseJoinColumns = @JoinColumn(name = "profile_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "resource_id", "profile_id"}))
    private List<Profile> profiles;

    /*******************************************************************
     * Constructors
     * *****************************************************************/

    public Resource() {
        super();
    }

    public Resource(Integer inId) {
        this.id = inId;
    }

    public Resource(String inOid) {
        this.oid = inOid;
    }

    public Resource(String inOid, byte[] inContent) {
        this.oid = inOid;
        setContent(inContent);

    }

    public Resource(Integer inId, String inOid, byte[] inContent) {
        this.id = inId;
        this.oid = inOid;
        setContent(inContent);
    }

    /**********************************************************
     * Getters and Setters
     **********************************************************/

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer inId) {
        this.id = inId;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    /***********************************************************
     * Foreign Key for table : hl7_profile_resource
     ************************************************************/

    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    /**
     * Returns true if the OID is already used by another resource
     *
     * @param inOID the OID which identifies the resource file the OID which identifies the resource file
     * @return true if the OID exists
     */
    private static boolean validateOID(String inOID) {
        HQLQueryBuilder<Resource> builder = new HQLQueryBuilder<Resource>(Resource.class);
        builder.addEq("oid", inOID);
        return (builder.getCount() > 0);
    }

    /**
     * Returns a Resource object selected by its OID
     *
     * @param inOid the OID which identifies the resource file
     * @return the resource from DB
     */
    public static Resource getResourceByOID(String inOid) {
        if ((inOid == null) || inOid.isEmpty()) {
            return null;
        } else {
            ResourceQuery query = new ResourceQuery();
            query.oid().eq(inOid);
            return query.getUniqueResult();
        }
    }

    /**
     * Generates an OID, inserts the given resource description into database and returns the OID of the new resource
     *
     * @param inXmlContent the content to be included in DB
     * @return the OID
     * @throws PersistenceException if DB error
     */
    public static String storeResource(String inXmlContent) throws PersistenceException {
        if ((inXmlContent == null) || (inXmlContent.length() == 0)) {
            log.error("Resource::storeResource(String inXmlContent): trying to set a resource without providing a XML content");
            return null;
        }

        EntityManager em = (EntityManager) Component.getInstance("entityManager");

        Resource r = new Resource();
        r.setContent(inXmlContent.getBytes(Charset.forName("UTF-8")));
        r.setOid(OIDGenerator.generateNewOIDForResource(em));
        em.merge(r);
        em.flush();
        return r.getOid();

    }

    /**
     * Inserts the given resource description for a resource with the given OID
     *
     * @param inOid        the OID which identifies the resource file
     * @param inXmlContent the content to be included in DB
     * @return true if operation succeeds
     * @throws PersistenceException if DB error
     */
    public static boolean storeResourceWithOID(String inOid, String inXmlContent) throws PersistenceException {
        if ((inOid == null) || (inOid.length() == 0) || (inXmlContent == null) || (inXmlContent.length() == 0)) {
            log.error("Resource::storeResourceWithOID trying to set a resource with oid: one parameter is missing or empty");
            return false;
        }

        if (validateOID(inOid)) {
            log.error("Resource::storeResourceWithOID The given OID " + inOid + " is already used by another resource");
            return false;
        }

        EntityManager em = (EntityManager) Component.getInstance("entityManager");

        Resource r = new Resource(inOid);

		/*if (r.getContent() == null) {
			r.setContent(inXmlContent.getBytes(Charset.forName("UTF-8")));
		}*/
        em.merge(r);
        em.flush();
        return true;

    }

    /**
     * Remove the given Resource from the database
     *
     * @param inResource the resource to delete
     * @return true if success
     * @throws PersistenceException db error
     */
    public static boolean delete(Resource inResource) throws PersistenceException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");

        if (inResource != null) {
            log.info("Resource::deleteResource -- deleting resource with OID " + inResource.getOid());
            em.remove(inResource);
            return true;
        } else {
            log.error("Resource::deleteResource -- The given resource is null !");
            return false;
        }
    }

    public String getUrl() {
        if (this.getOid() != null) {
            StringBuilder url = new StringBuilder(ApplicationConfiguration.getValueOfVariable("application_url"));
            url.append("/viewResource.seam?oid=");
            url.append(this.getOid());
            return url.toString();
        } else {
            return null;
        }
    }

    public void save() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    /**
     * Returns the resources for the given HL7 Version
     *
     * @param inVersion : version to look for
     * @return the list of resources for the given version
     */
    public static List<Resource> getResourceForHl7Version(String inVersion) {
        HQLQueryBuilder<Resource> builder = new HQLQueryBuilder<Resource>(Resource.class);
        builder.addEq("hl7Version", inVersion);
        List<Resource> resources = builder.getList();
        if ((resources != null) && !resources.isEmpty()) {
            return resources;
        } else {
            return null;
        }
    }

    // *********************************************************
    // Overrides for equals and hash code methods
    // *********************************************************

    @Override
    public int compareTo(Resource o) {
        return this.getOid().compareToIgnoreCase(o.getOid());
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return weight;
    }

    public String getDirectory() {
        return PreferenceService.getString("hl7_directory") + getPath();
    }

}
