/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.view;

import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAOImpl;
import net.ihe.gazelle.hl7.messageprofiles.model.ConformanceProfileFormat;
import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.messageprofiles.application.HL7Application;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.tf.ws.Actor;
import net.ihe.gazelle.tf.ws.Domain;
import net.ihe.gazelle.tf.ws.Hl7MessageProfile;
import net.ihe.gazelle.tf.ws.Transaction;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>HL7MessageProfileCreationManager<br>
 * <br>
 * This class implements the local interface HL7MessageProfileCreationManager dedicated to the creation of new HL7 Message Profile, stored in the
 * database of this application and referenced in Gazelle
 * Master Model
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, December 21st
 */
@Name("creationManagerBean")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class HL7MessageProfileCreationManagerView implements Serializable {

    private static Logger log = LoggerFactory.getLogger(HL7MessageProfileCreationManagerView.class);


    // TODO : Fix injection
    //@In(create = true, value = "hl7MessageProfileApplication")

    private HL7Application hl7Application;

    /**
     * Name of the preference (cmn_application_prefenrence) to retrieve the wsdl of Hl7MessageProfile ws
     */
    private static final String HL7_MESSAGE_PROFILE_ENDPOINT_NAME = "gmm_hl7messageprofile_wsdl";

    /**
     * Serial Version UUID requested by the implementation of Serializable
     */
    private static final long serialVersionUID = 4249465584086119918L;

    /**
     * All available domains registered in GMM
     */
    private List<SelectItem> availableDomains;


    /**
     * keyword of the selected domain
     */
    private String selectedDomainKeyword;

    /**
     * All available actors registered in GMM and linked to the selected domain
     */
    private List<SelectItem> availableActors;

    /**
     * keyword of the selected actor
     */
    private String selectedActorKeyword;

    /**
     * All available transactions registered within GMM and linked to the selected actor
     */
    private List<SelectItem> availableTransactions;

    /**
     * keyword of the selected transaction
     */
    private String selectedTransactionKeyword;

    /**
     * The Hl7MessageProfile object to reference in GMM
     */
    private Hl7MessageProfile selectedReference;

    /**
     * Indicates whether the button to submit the profile must be displayed or not
     */
    private boolean displaySubmitButton;

    private String profilePath;

    private String javaPackage;

    private ConformanceProfileFormat conformanceProfileFormat;


    /**
     *
     */
    private String linkedTF;

    /**
     * Indicates whether the file can be stored in database or not No data must be stored in database until this attribute is set to true this
     * attribute is not used in XHTML pages
     */
    private boolean isFileValid;

    public HL7MessageProfileCreationManagerView() throws ControllerException {
        this.hl7Application = new HL7Application();
        // TODO Use injection
        this.hl7Application.setMessageProfilesDAO(new MessageProfilesDAOImpl());

    }

    /**
     * <p>Getter for the field <code>availableDomains</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<SelectItem> getAvailableDomains() {
        return availableDomains;
    }

    /**
     * <p>Setter for the field <code>availableDomains</code>.</p>
     *
     * @param availableDomains a {@link List} object.
     */
    public void setAvailableDomains(List<SelectItem> availableDomains) {
        this.availableDomains = availableDomains;
    }

    /**
     * <p>Getter for the field <code>availableActors</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<SelectItem> getAvailableActors() {
        return availableActors;
    }

    /**
     * <p>Setter for the field <code>availableActors</code>.</p>
     *
     * @param availableActors a {@link List} object.
     */
    public void setAvailableActors(List<SelectItem> availableActors) {
        this.availableActors = availableActors;
    }

    /**
     * <p>Getter for the field <code>availableTransactions</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<SelectItem> getAvailableTransactions() {
        return availableTransactions;
    }

    /**
     * <p>Setter for the field <code>availableTransactions</code>.</p>
     *
     * @param availableTransactions a {@link List} object.
     */
    public void setAvailableTransactions(List<SelectItem> availableTransactions) {
        this.availableTransactions = availableTransactions;
    }

    /**
     * <p>Getter for the field <code>selectedReference</code>.</p>
     *
     * @return a {@link Hl7MessageProfile} object.
     */
    public Hl7MessageProfile getSelectedReference() {
        return selectedReference;
    }

    /**
     * <p>Setter for the field <code>selectedReference</code>.</p>
     *
     * @param selectedReference a {@link Hl7MessageProfile} object.
     */
    public void setSelectedReference(Hl7MessageProfile selectedReference) {
        this.selectedReference = selectedReference;
    }

    public String getJavaPackage() {
        return this.hl7Application.getSelectedProfile().getJavaPackage();
    }

    public void setJavaPackage(String javaPackage) {
        this.hl7Application.getSelectedProfile().setJavaPackage(javaPackage);
    }

    /**
     * <p>Setter for the field <code>displaySubmitButton</code>.</p>
     *
     * @param displaySubmitButton a boolean.
     */
    public void setDisplaySubmitButton(boolean displaySubmitButton) {
        this.displaySubmitButton = displaySubmitButton;
    }

    /**
     * <p>isDisplaySubmitButton.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplaySubmitButton() {
        return displaySubmitButton;
    }

    /**
     * <p>Setter for the field <code>linkedTF</code>.</p>
     *
     * @param linkedTF a {@link String} object.
     */
    public void setLinkedTF(String linkedTF) {
        this.linkedTF = linkedTF;
    }

    /**
     * <p>Getter for the field <code>linkedTF</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getLinkedTF() {
        this.linkedTF = ApplicationConfiguration.getValueOfVariable(HL7_MESSAGE_PROFILE_ENDPOINT_NAME);
        return linkedTF;
    }

    /**
     * <p>setFileValid.</p>
     *
     * @param isFileValid a boolean.
     */
    public void setFileValid(boolean isFileValid) {
        this.isFileValid = isFileValid;
    }

    /**
     * <p>Getter for the field <code>selectedDomainKeyword</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getSelectedDomainKeyword() {
        return selectedDomainKeyword;
    }

    /**
     * Getter for Profile Path
     * @return
     */
    public String getProfilePath() {
        return this.hl7Application.getSelectedProfile().getPath();
    }

    /**
     * Setter for profile path
     * @param profilePath
     */
    public void setProfilePath(String profilePath) {
        this.hl7Application.getSelectedProfile().setPath(profilePath);
    }

    /**
     * Getter for Conformance profile
     * @return
     */
    public ConformanceProfileFormat getConformanceProfileFormat() {
        return this.hl7Application.getSelectedProfile().getConformanceProfileFormat();
    }

    /**
     * Setter for conformance profile
     * @param conformanceProfileFormat
     */
    public void setConformanceProfileFormat(ConformanceProfileFormat conformanceProfileFormat) {
        this.hl7Application.getSelectedProfile().setConformanceProfileFormat(conformanceProfileFormat);
    }

    /**
     * Instanciates new object and gives appropriate values to the attributes
     */
    public void initialize() {
        this.hl7Application.initializeProfile();
        selectedReference = new Hl7MessageProfile();
        selectedActorKeyword = null;
        selectedDomainKeyword = null;
        selectedTransactionKeyword = null;
        displaySubmitButton = false;
    }

    /**
     * <p>Setter for the field <code>selectedDomainKeyword</code>.</p>
     *
     * @param selectedDomainKeyword a {@link String} object.
     */
    public void setSelectedDomainKeyword(String selectedDomainKeyword) {
        this.selectedDomainKeyword = selectedDomainKeyword;
        // Reset lists of available Actors and transactions
        loadListOfActorsForDomain();
        this.availableTransactions = null;
        //Reset previously selected values for Actor and Transaction
        this.selectedActorKeyword = null;
        this.selectedTransactionKeyword = null;
    }

    /**
     * <p>Getter for the field <code>selectedActorKeyword</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getSelectedActorKeyword() {
        return selectedActorKeyword;
    }

    /**
     * <p>Setter for the field <code>selectedActorKeyword</code>.</p>
     *
     * @param selectedActorKeyword a {@link String} object.
     */
    public void setSelectedActorKeyword(String selectedActorKeyword) {
        this.selectedActorKeyword = selectedActorKeyword;
        // Reset list of transaction
        loadListOfTransactionsForActor();
        // Reset selected transaction
        this.selectedTransactionKeyword = null;
    }

    /**
     * <p>Getter for the field <code>selectedTransactionKeyword</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getSelectedTransactionKeyword() {
        return selectedTransactionKeyword;
    }

    /**
     * <p>Setter for the field <code>selectedTransactionKeyword</code>.</p>
     *
     * @param selectedTransactionKeyword a {@link String} object.
     */
    public void setSelectedTransactionKeyword(String selectedTransactionKeyword) {
        this.selectedTransactionKeyword = selectedTransactionKeyword;
    }

    /**
     * returns a list of selectItem converted from a list of actors
     *
     * @param actors
     * @return
     */
    private ArrayList<SelectItem> convertActorsToSelectItem(Actor[] actors) {
        ArrayList<SelectItem> availableActors = null;

        if ((actors != null) && (actors.length > 0)) {
            availableActors = new ArrayList<>();

            for (Actor actor : actors) {
                availableActors.add(new SelectItem(actor.getKeyword(), actor.getName()));
            }
        }
        return availableActors;

    }

    /**
     * Convert Domains to SelectItem
     *
     * @param domains
     * @return
     */
    private ArrayList<SelectItem> convertDomainsToSelectItem(Domain[] domains) {
        ArrayList<SelectItem> availableDomains = null;
        if ((domains != null) && (domains.length > 0)) {
            availableDomains = new ArrayList<>();
            for (Domain domain : domains) {
                availableDomains.add(new SelectItem(domain.getKeyword(), domain.getName()));
            }
            log.info("domains: " + availableDomains.size());
        }
        return availableDomains;
    }

    /**
     * Convert Transaction into SelectItem
     *
     * @param transactions
     * @return
     */
    private ArrayList<SelectItem> convertTransactionToSelectItem(Transaction[] transactions) {
        ArrayList<SelectItem> availableTransactions = null;
        if ((transactions != null) && (transactions.length > 0)) {
            availableTransactions = new ArrayList<>();
            for (Transaction t : transactions) {
                availableTransactions.add(new SelectItem(t.getKeyword(), t.getKeyword()));
            }
        }
        return availableTransactions;
    }

    /**
     * Display the  error message
     *
     * @param exception
     */
    private void dealControllerException(ControllerException exception) {
        FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, exception.getKey(), exception.getParam());
    }

    /**
     * Retrieves the list of actors for the selected domain (stored in Hl7MessageProfile object)
     */
    public void loadListOfActorsForDomain() {
        try {
            Actor[] actors = this.hl7Application.getListOfActorsForDomain(selectedDomainKeyword, selectedReference);
            this.setAvailableActors(this.convertActorsToSelectItem(actors));
        } catch (ControllerException e) {
            dealControllerException(e);
        }
    }

    /**
     * Retrieves the list of domains
     *
     * @throws ControllerException
     */
    public void loadListOfDomains() {
        try {
            Domain[] domains = this.hl7Application.getListOfDomains();
            this.setAvailableDomains(this.convertDomainsToSelectItem(domains));
        } catch (ControllerException e) {
            dealControllerException(e);
        }
    }

    /**
     * Retrieves the list of all transactions available in GMM and related to the selected actor by calling the web service IHEConcepts
     */
    public void loadListOfTransactionsForActor() {
        try {
            Transaction[] transactions = this.hl7Application.getListOfTransactionsForActor(selectedActorKeyword,
                    selectedReference);
            this.setAvailableTransactions(this.convertTransactionToSelectItem(transactions));
        } catch (ControllerException e) {
            dealControllerException(e);
        }
    }

    /**
     * <p>enableSubmit.</p>
     */
    public void enableSubmit() {
        if ((selectedTransactionKeyword != null) && !selectedTransactionKeyword.isEmpty()) {
            try {
                this.hl7Application.getTransaction(selectedTransactionKeyword, selectedReference);
            } catch (ControllerException exception) {
                dealControllerException(exception);
            }

            if ((selectedReference.getTransaction() != null) && isFileValid) {
                displaySubmitButton = true;
            } else {
                displaySubmitButton = false;
            }
        } else {
            displaySubmitButton = false;
        }

    }

    /**
     * <p>getANewOID.</p>
     */
    public void getANewOID() {
        this.hl7Application.getAnewOID(selectedReference);
    }


    /**
     * if the reference is properly filled and submit the profile: 1. gets the related resources 2. stores the Profile object in database 3. sends
     * the new reference (Hl7MessageProfile object) to GMM
     */
    public void submitNewProfile() {
        try {
            this.hl7Application.submitNewProfile(selectedReference);
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.hl7mpr.submitSucceeded",
                    selectedReference.getOid());

        } catch (ControllerException e) {
            dealControllerException(e);
        }

    }

    /**
     * Method called when the user upload a file using the rich:fileUpload component if the XML file is not empty, well-formed and valid, we can
     * add it to the database
     *
     * @param event a {@link FileUploadEvent} object.
     */
    public void uploadEventListener(FileUploadEvent event) {
        // TODO
        try {
            if (this.hl7Application.uploadEventListener(event)) {
                enableSubmit();
            }
        } catch (ControllerException controllerException) {
            dealControllerException(controllerException);
        }
    }

    /**
     * Generate a relative path when we register new profiles
     */

    public void generatePath() {
        this.hl7Application.generatePath(selectedReference);

    }


}






