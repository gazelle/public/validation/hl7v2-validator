package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)
 * <p>
 * Assertion ID: INS-001 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-001)
 * predicate: Le matricule INS doit être renseigné uniquement si l'identité du patient a été qualifiée, ce qui se traduit par PID.32 = VALI dans le message.
 * Prescription level: Mandatory / Required / Shall
 * Page: 6
 * Section: 6.7.1
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:08 by aberge
 * Commentaire: (PID-3 contient au moins un identifiant avec CX-4-2 dans [1.2.250.1.213.1.4.8, 1.2.250.1.213.1.4.9, 1.2.250.1.213.1.4.10, 1.2.250.1.213.1.4.11]) xor (PID-32 != VALI)
 *
 * @author pdt
 */

public class INS001RuleInITI extends INSValidationRule {

    protected ValidationException[] validate(PID pid) throws HL7Exception {
        // Si (PID-3 contient au moins un identifiant
        // avec CX-4-2 dans [1.2.250.1.213.1.4.8, 1.2.250.1.213.1.4.9, 1.2.250.1.213.1.4.10, 1.2.250.1.213.1.4.11]) => INS
        // Alors (PID-32 != VALI)
        if (hasMatriculeINS(pid) && !isIdentiteQualifiee(pid)) {
            return new ValidationException[]{new INSValidationException()};
        }
        return null;
    }

    @Override
    public int getSeverity() {
        return GazelleErrorCode.COCONSTRAINT_FAILURE.getCode();
    }


}
