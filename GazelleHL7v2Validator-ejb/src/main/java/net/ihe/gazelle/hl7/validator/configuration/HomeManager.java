package net.ihe.gazelle.hl7.validator.configuration;

import java.io.Serializable;

import net.ihe.gazelle.hl7.validator.model.Home;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.Lob;

/**
 * <p>HomeManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1111092307583013570L;

	private net.ihe.gazelle.hl7.validator.model.Home selectedHome;
	private boolean mainContentEditMode;

	/**
	 * <p>Getter for the field <code>selectedHome</code>.</p>
	 *
	 * @return a net$ihe$gazelle$hl7$validator$model$Home object.
	 */
	public net.ihe.gazelle.hl7.validator.model.Home getSelectedHome() {
		return selectedHome;
	}

	/**
	 * <p>Setter for the field <code>selectedHome</code>.</p>
	 *
	 * @param selectedHome a net$ihe$gazelle$hl7$validator$model$Home object.
	 */
	public void setSelectedHome(Home selectedHome) {
		this.selectedHome = selectedHome;
	}

	/**
	 * <p>isMainContentEditMode.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isMainContentEditMode() {
		return mainContentEditMode;
	}

	/**
	 * <p>Setter for the field <code>mainContentEditMode</code>.</p>
	 *
	 * @param mainContentEditMode a boolean.
	 */
	public void setMainContentEditMode(boolean mainContentEditMode) {
		this.mainContentEditMode = mainContentEditMode;
	}

	/**
	 * <p>initializeHome.</p>
	 */
	public void initializeHome() {
		selectedHome = Home.getHomeForSelectedLanguage();
	}

	/**
	 * <p>editMainContent.</p>
	 */
	public void editMainContent() {
		mainContentEditMode = true;
	}

	/**
	 * <p>save.</p>
	 */
	public void save() {
		selectedHome = selectedHome.save();
		mainContentEditMode = false;
	}

	/**
	 * <p>cancel.</p>
	 */
	public void cancel() {
		mainContentEditMode = false;
	}

}
