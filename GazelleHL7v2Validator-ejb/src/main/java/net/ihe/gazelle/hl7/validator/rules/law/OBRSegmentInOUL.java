package net.ihe.gazelle.hl7.validator.rules.law;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

public class OBRSegmentInOUL extends IHEValidationRule {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(OBRSegmentInOUL.class);

	@Override public ValidationException[] test(Message msg) {
		return null;
	}

	@Override public ValidationException[] test(Message msg, final String hl7path) {
		String path = formatHl7Path(hl7path);
		String shortenPath = path.substring(7, path.lastIndexOf('/'));
		Terser terser = new Terser(msg);
		ValidationException exception = null;
		try {
			Segment obr = terser.getSegment(shortenPath);
			String obr11 = obr.getField(11, 0).encode();
			if (path.contains("OBR-26")){
				String obr26 = obr.getField(26, 0).encode();
				if (!obr11.equals("G") && !obr26.isEmpty()) {
					exception = new ValidationException(
							"Usage of OBR-26 is forbidden when the order is not a reflex order");
				}
			}
		} catch (HL7Exception e) {
			exception = new ValidationException("Error when parsing message for validation: " + e.getMessage());
		}
		if (exception != null) {
			log.debug(exception.getMessage());
			return new ValidationException[] { exception };
		} else {
			return null;
		}
	}

	@Override public String getDescription() {
		return "Rules that apply to OBR segment in the context of the LAW profile";
	}

	@Override public String getSectionReference() {
		return "PaLM TF-2b Rev. 9.0, Section C.6 OBR Segment";
	}

	@Override public int getSeverity() {
		return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
	}

}
