package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

import java.util.regex.Pattern;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-007 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-007)
 * predicate: Dans le cas où l'on véhicule l'INS et que l'identité est qualifiée, le sexe du patient peut prendre la valeur "M" ou "F".
 * Prescription level: Mandatory / Required / Shall
 * Page: 8
 * Section: 6.7.5
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:24 by aberge
 * Commentaire: Si PID-32 alors PID-8 dans [M, F]

 * @author pdt
 *
 */
public class INS007RuleInITI extends INSValidationRule {
	private final Pattern sexes = Pattern.compile("^[MF]$");
	protected ValidationException[] validate(PID pid) throws HL7Exception {
		if (hasMatriculeINS(pid) && !sexes.matcher(pid.getAdministrativeSex().getValueOrEmpty()).matches()) {
			return new ValidationException[] {new INSValidationException(pid.getAdministrativeSex().getValueOrEmpty())};
		}
		return null;
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.WRONG_CONSTANT_VALUE.getCode();
	}

}
