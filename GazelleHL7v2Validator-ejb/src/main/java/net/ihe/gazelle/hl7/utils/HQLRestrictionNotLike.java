package net.ihe.gazelle.hl7.utils;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;

public class HQLRestrictionNotLike implements HQLRestriction {

	private String path;
	private String value;
	private HQLRestrictionLikeMatchMode matchMode;

	public HQLRestrictionNotLike(String path, String value, HQLRestrictionLikeMatchMode matchMode) {
		super();
		this.path = path;
		this.value = value.toLowerCase();
		this.matchMode = matchMode;
	}

	public HQLRestrictionNotLike(String path, String value) {
		super();
		this.path = path;
		this.value = value.toLowerCase();
		this.matchMode = HQLRestrictionLikeMatchMode.ANYWHERE;
	}

	@Override
	public void toHQL(HQLQueryBuilder<?> queryBuilder, HQLRestrictionValues values, StringBuilder sb) {
		sb.append("lower(");
		sb.append(queryBuilder.getShortProperty(path));
		sb.append(") not like :");
		sb.append(values.addValue(path, matchMode.toHQL(value)));
	}

}
