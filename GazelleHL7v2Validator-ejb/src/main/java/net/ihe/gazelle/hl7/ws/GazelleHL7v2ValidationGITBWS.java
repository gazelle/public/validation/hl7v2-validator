package net.ihe.gazelle.hl7.ws;

import com.gitb.core.*;
import com.gitb.tr.ObjectFactory;
import com.gitb.tr.ValidationCounters;
import com.gitb.tr.*;
import com.gitb.vs.Void;
import com.gitb.vs.*;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7.assertion.GazelleHL7Assertion;
import net.ihe.gazelle.hl7.exception.GazelleHL7Exception;
import net.ihe.gazelle.hl7.validator.core.HL7Validator;
import net.ihe.gazelle.hl7.validator.report.Error;
import net.ihe.gazelle.hl7.validator.report.*;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.apache.commons.codec.binary.Base64;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URLConnection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Class used to implement the HL7v2 validation service as a GITB compliant validation service.
 * <p/>
 * This class reuses the core validation logic by adding pre-processing for the extraction of
 * the required validation parameters, and post-processing to formulate results in a GITB
 * compliance manner.
 *
 * @author Constantinos Simatos (constantinos.simatos@trasysgroup.com)
 * @version $Id: $Id
 */
@Stateless
@Name("gazelleHL7v2ValidationGITBWS")
@WebService(name = "gazelleHL7v2ValidationGITBWS", serviceName = "gazelleHL7v2ValidationGITBWSService")
@GenerateInterface(value = "GazelleHL7v2ValidationGITBWSRemote", isLocal = false, isRemote = true)
public class GazelleHL7v2ValidationGITBWS implements GazelleHL7v2ValidationGITBWSRemote, ValidationService  {

    MetadataServiceProvider metadataServiceProvider;
    private static String INPUT_METADATA = "xmlValidationMetadata";
    private static String INPUT_CONTEXT = "xmlValidationContext";
    private static String INPUT_MESSAGE = "messageToValidate";


    @Resource
    WebServiceContext jaxwsContext;

    /**
     * {@inheritDoc}
     * Operation that is used to define the manner in which the validation service should be used.
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(@WebParam(name = "GetModuleDefinitionRequest", targetNamespace = "http://www.gitb" +
            ".com/vs/v1/", partName = "parameters") Void aVoid) {
        GetModuleDefinitionResponse response = new GetModuleDefinitionResponse();
        response.setModule(new ValidationModule());
        response.getModule().setId("gazelleHL7v2ValidationGITBWSService");
        response.getModule().setOperation("V");
        response.getModule().setMetadata(new Metadata());
        response.getModule().getMetadata().setName(ValidationResultsOverview.TOOL_NAME);
        response.getModule().getMetadata().setVersion(getMetadataServiceProvider().getMetadata().getVersion());
        response.getModule().getMetadata().setDescription(ValidationResultsOverview.DISCLAIMER);
        response.getModule().setInputs(new TypedParameters());
        TypedParameter xmlValidationMetadataInput = new TypedParameter();
        xmlValidationMetadataInput.setName(INPUT_METADATA);
        xmlValidationMetadataInput.setUse(UsageEnumeration.O);
        xmlValidationMetadataInput.setKind(ConfigurationType.SIMPLE);
        xmlValidationMetadataInput.setDesc("The metadata to consider for the message validation. This is an XML string.");
        response.getModule().getInputs().getParam().add(xmlValidationMetadataInput);
        TypedParameter xmlValidationContextInput = new TypedParameter();
        xmlValidationContextInput.setName(INPUT_CONTEXT);
        xmlValidationContextInput.setUse(UsageEnumeration.R);
        xmlValidationContextInput.setKind(ConfigurationType.SIMPLE);
        xmlValidationContextInput.setDesc("The context information required for the message validation. This is an XML string.");
        response.getModule().getInputs().getParam().add(xmlValidationContextInput);
        TypedParameter messageToValidateInput = new TypedParameter();
        messageToValidateInput.setName(INPUT_MESSAGE);
        messageToValidateInput.setUse(UsageEnumeration.R);
        messageToValidateInput.setKind(ConfigurationType.SIMPLE);
        messageToValidateInput.setDesc("The message to validate. This is a string.");
        response.getModule().getInputs().getParam().add(messageToValidateInput);
        return response;
    }

    /**
     * {@inheritDoc}
     * Operation that executes the HL7v2 validation in a GITB compliant manner.
     */
    @Override
    public ValidationResponse validate(@WebParam(name = "ValidateRequest", targetNamespace = "http://www.gitb.com/vs/v1/", partName = "parameters") ValidateRequest validateRequest) {
        System.setProperty(DOMImplementationRegistry.PROPERTY, org.apache.xerces.dom.DOMImplementationSourceImpl.class.getCanonicalName());
        if (validateRequest == null
                || validateRequest.getInput() == null
                || validateRequest.getInput().isEmpty()) {
            throw new IllegalArgumentException("You must provide the message to validate");
        }
        String xmlValidationMetadata = null;
        String xmlValidationContext = null;
        String messageToValidate = null;
        for (AnyContent content : validateRequest.getInput()) {
            if (content != null) {
                if (INPUT_METADATA.equals(content.getName())) {
                    xmlValidationMetadata = extractContent(content);
                } else if (INPUT_CONTEXT.equals(content.getName())) {
                    xmlValidationContext = extractContent(content);
                } else if (INPUT_MESSAGE.equals(content.getName())) {
                    messageToValidate = extractContent(content);
                }
            }
        }
        if (xmlValidationContext == null || xmlValidationContext.isEmpty()) {
            throw new IllegalArgumentException("You must provide the message context (XML string)");
        }
        if (messageToValidate == null || messageToValidate.isEmpty()) {
            throw new IllegalArgumentException("You must provide the message to validate");
        }
        Lifecycle.beginCall();
        HttpServletRequest hRequest = (HttpServletRequest) jaxwsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        HL7Validator validator = new HL7Validator(hRequest.getRemoteAddr(), xmlValidationMetadata, messageToValidate,
                xmlValidationContext, getMetadataServiceProvider());
        validator.validate();
        ValidationResponse result = new ValidationResponse();
        result.setReport(toTestReport(validateRequest, validator.getReport()));
        Lifecycle.endCall();
        return result;
    }

    /**
     * Extract the String represented by the provided content.
     *
     * @param content The content to process.
     *
     * @return The content's String representation.
     */
    private String extractContent(AnyContent content) {
        String stringContent = null;
        if (content != null && content.getValue() != null) {
            switch (content.getEmbeddingMethod()) {
                case STRING:
                    // Use string as-is.
                    stringContent = content.getValue();
                    break;
                case URI:
                    // Read the string from the provided URI.
                    URI uri = URI.create(content.getValue());
                    Proxy proxy = null;
                    List<Proxy> proxies = ProxySelector.getDefault().select(uri);
                    if (proxies != null && !proxies.isEmpty()) {
                        proxy = proxies.get(0);
                    }
                    BufferedReader in = null;
                    try {
                        URLConnection connection;
                        if (proxy == null) {
                            connection = uri.toURL().openConnection();
                        } else {
                            connection = uri.toURL().openConnection(proxy);
                        }
                        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuilder builder = new StringBuilder();
                        while ((inputLine = in.readLine()) != null) {
                            builder.append(inputLine);
                        }
                        stringContent = builder.toString();
                    } catch (IOException e) {
                        throw new IllegalArgumentException("Unable to read provided URI", e);
                    } finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e) {
                                // Ignore.
                            }
                        }
                    }
                    break;
                default: // BASE_64
                    // Construct the string from its BASE64 encoded bytes.
                    stringContent = new String(Base64.decodeBase64(content.getValue()));
                    break;
            }
        }
        return stringContent;
    }

    /**
     * Produce the GITB test report based on the received request and the processing outcome.
     *
     * @param validateRequest The request that was received by the validation service.
     * @param reportFromTool  The deserialised HL7 message including its validation outcome.
     *
     * @return The validation report in GITB TRL.
     */
    private TAR toTestReport(ValidateRequest validateRequest, HL7v2ValidationReport reportFromTool)  {
        TAR report = new TAR();
        // Validation request context.
        report.setContext(new AnyContent());
        for (AnyContent input : validateRequest.getInput()) {
            report.getContext().getItem().add(input);
        }
        if (reportFromTool != null) {
            // Counters.
            report.setCounters(new ValidationCounters());
            report.getCounters().setNrOfAssertions(BigInteger.valueOf(reportFromTool.getResults().getReportCounter()));
            report.getCounters().setNrOfErrors(BigInteger.valueOf(reportFromTool.getResults().getErrorCounter()));
            report.getCounters().setNrOfWarnings(BigInteger.valueOf(reportFromTool.getResults().getWarningCounter() +reportFromTool.getResults().getExceptionCounter() ));
            // Date.
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            try {
                report.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException e) {
                throw new IllegalStateException("Unable to construct validation date", e);
            }
            // Overview information.
            if (reportFromTool.getOverview() != null) {
                report.setOverview(new ValidationOverview());
                switch (reportFromTool.getOverview().getValidationStatus()) {
                    case PASSED:
                        report.setResult(TestResultType.SUCCESS);
                        break;
                    case FAILED:
                        report.setResult(TestResultType.FAILURE);
                        break;
                    case ABORTED :
                        throw new IllegalArgumentException(reportFromTool.getOverview().getValidationAbortedReason());
                    case INVALID_REQUEST:
                        throw new IllegalArgumentException(reportFromTool.getOverview().getValidationAbortedReason());
                    default:
                        report.setResult(TestResultType.UNDEFINED);
                        break;
                }
                report.getOverview().setProfileID(reportFromTool.getOverview().getProfileOid());
                report.getOverview().setNote(reportFromTool.getOverview().getDisclaimer());
                report.getOverview().setValidationServiceName(reportFromTool.getOverview().getValidationServiceName());
                report.getOverview().setValidationServiceVersion(reportFromTool.getOverview().getValidationServiceVersion());
                report.getOverview().setTransactionID(reportFromTool.getOverview().getMessageOid());

            }
            // Results.
            report.setReports(new TestAssertionGroupReportsType());
            ObjectFactory trFactory = new ObjectFactory();

            if (reportFromTool.getResults() != null) {
                List<Object> notifications = reportFromTool.getResults().getNotifications();
                for (Object notification : notifications) {
                    if (notification instanceof Error) {
                        addError(report.getReports(), (Error) notification, trFactory);
                    } else if (notification instanceof Warning) {
                        addWarning(report.getReports(), (Warning) notification, trFactory);
                    } else if (notification instanceof GazelleHL7Assertion) {
                        addAssertion(report.getReports(), (GazelleHL7Assertion) notification, trFactory);
                    } else if (notification instanceof GazelleProfileException) {
                        addException(report.getReports(), (GazelleProfileException) notification, trFactory);
                    }
                }
            }
        }
        return report;
    }

    /**
     * Add a passed assertion (at info level) to the detailed test report.
     *
     * @param reports      The report to add to.
     * @param notification The assertion to add.
     * @param trFactory    The object factory to construct the expected report type.
     */
    private void addAssertion(TestAssertionGroupReportsType reports, GazelleHL7Assertion notification, ObjectFactory trFactory) {
        BAR reportEntry = new BAR();
        reportEntry.setLocation(notification.getPath());
        reportEntry.setDescription(notification.getAssertion());
        if (notification.getType() != null) {
            reportEntry.setType(notification.getType().getLabel());
        }
        reports.getInfoOrWarningOrError().add(trFactory.createTestAssertionGroupReportsTypeInfo(reportEntry));
    }

    /**
     * Add a processing exception (at error level) to the detailed test report.
     *
     * @param reports      The report to add to.
     * @param notification The exception to add.
     * @param trFactory    The object factory to construct the expected report type.
     */
    private void addException(TestAssertionGroupReportsType reports, GazelleProfileException notification, ObjectFactory trFactory) {
        reports.getInfoOrWarningOrError().add(trFactory.createTestAssertionGroupReportsTypeWarning(createReportEntry(notification)));
    }

    /**
     * Add a warning (at warn level) to the detailed test report.
     *
     * @param reports      The report to add to.
     * @param notification The warning to add.
     * @param trFactory    The object factory to construct the expected report type.
     */
    private void addWarning(TestAssertionGroupReportsType reports, Warning notification, ObjectFactory trFactory) {
        reports.getInfoOrWarningOrError().add(trFactory.createTestAssertionGroupReportsTypeWarning(createReportEntry(notification)));
    }

    /**
     * Add an error (at error level) to the detailed test report.
     *
     * @param reports      The report to add to.
     * @param notification The error to add.
     * @param trFactory    The object factory to construct the expected report type.
     */
    private void addError(TestAssertionGroupReportsType reports, Error notification, ObjectFactory trFactory) {
        reports.getInfoOrWarningOrError().add(trFactory.createTestAssertionGroupReportsTypeError(createReportEntry(notification)));
    }

    /**
     * Create the GITB TRL report entry for the provided validation notification.
     *
     * @param notification The notification to process.
     *
     * @return The resulting GITB TRL report object.
     */
    private BAR createReportEntry(GazelleHL7Exception notification) {
        BAR reportEntry = new BAR();
        reportEntry.setLocation(notification.getHl7Path());
        reportEntry.setDescription(notification.getDescription());
        reportEntry.setType(notification.getFailureType());
        reportEntry.setAssertionID(notification.getHl7tableId());
        reportEntry.setValue(notification.getValue());
        return reportEntry;
    }

    private MetadataServiceProvider getMetadataServiceProvider() {
        if (metadataServiceProvider == null) {
            metadataServiceProvider = (MetadataServiceProvider) Component.getInstance("metadataServiceProvider");
        }
        return metadataServiceProvider;
    }
}
