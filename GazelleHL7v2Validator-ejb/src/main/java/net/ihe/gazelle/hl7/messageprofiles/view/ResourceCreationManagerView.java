package net.ihe.gazelle.hl7.messageprofiles.view;

import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAOImpl;
import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.messageprofiles.application.HL7Application;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import java.io.Serializable;

@Name("resourceCreationManager")
@Scope(ScopeType.PAGE)
public class ResourceCreationManagerView implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Resource selectedResource;

    private HL7Application hl7Application;

    public Resource getSelectedResource() {
        return this.hl7Application.getSelectedResource();
    }

    public void setSelectedResource(Resource selectedResource) {
        this.hl7Application.setSelectedResource(selectedResource);
    }

    /**
     * Constructor
     */
    public ResourceCreationManagerView() throws ControllerException {
        this.hl7Application = new HL7Application();
        this.hl7Application.setMessageProfilesDAO(new MessageProfilesDAOImpl());
    }

    /**
     * Init the view
     */
    public void initialize() {
        this.hl7Application.initializeResource();
    }

    /**
     * Generate Oid
     */
    public void generateResourceOid() {
        this.hl7Application.generateResourceOid();
    }

    /**
     * Generate a path for resource
     */
    public void generatePath() {
        this.hl7Application.generateResourcePath();
    }

    /**
     * Method called when the user upload a file using the rich:fileUpload component if the XML file is not empty, well-formed and valid, we can add
     * it to the database
     */
    public void uploadEventListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        if ((item != null) && (item.getData() != null)) {
            selectedResource.setContent(item.getData());
        } else {
            displayInfos(StatusMessage.Severity.WARN, "The uploaded file is empty");
        }
    }

    /**
     * save a Resource
     *
     * @return
     */
    public String saveResource() {
        this.hl7Application.saveNewResource();
        displayInfos(StatusMessage.Severity.INFO, "The new resource has been properly created");
        return "/browser/resourcesList.seam";
    }

    /**
     * Display infos
     *
     * @param severity
     * @param infos
     */
    void displayInfos(StatusMessage.Severity severity, String infos) {
        FacesMessages.instance().add(severity, infos);
    }

}
