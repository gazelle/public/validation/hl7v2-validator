package net.ihe.gazelle.hl7.validator.rules.iti;

/**
 *  INS rules bound on PID-7 hl7path
 *
 * @author pdt
 *
 */
public class PID7RulesInITI extends INSValidationRules {

	@Override
	protected INSValidationRule[] getRules() {
		return new INSValidationRule[] {
				new INS005RuleInITI(),
		};
	}

	@Override
	public String getHl7Path() { return "*/PID/PID-7"; }
}
