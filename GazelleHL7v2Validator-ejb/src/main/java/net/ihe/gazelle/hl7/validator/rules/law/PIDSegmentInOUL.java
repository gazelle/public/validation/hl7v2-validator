package net.ihe.gazelle.hl7.validator.rules.law;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.OUL_R22;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.validation.ValidationException;

/**
 * This rule applies to OUL^R22 messages issued by Analyzers supporting the LAW profile
 * 
 * @author aberge
 * 
 */
public class PIDSegmentInOUL extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2337250963425936406L;

	private static Logger log = LoggerFactory.getLogger(PIDSegmentInOUL.class);

	@Override
	public ValidationException[] test(Message msg) {
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			if (msg instanceof OUL_R22) {
				OUL_R22 oulMessage = (OUL_R22) msg;
				Structure[] patientGroups = oulMessage.getAll("PATIENT");
				if (patientGroups.length > 0) { // if PATIENT group is not present, there's no need to test the presence of PID-3 field
					int orderCount = oulMessage.getSPECIMEN().getORDERReps();
					String obr2 = null;
					for (int index = 0; index < orderCount; index++) {
						String localObr2 = oulMessage.getSPECIMEN().getORDER(index).getOBR()
								.getObr2_PlacerOrderNumber().encode();
						if (!localObr2.equals("\"\"")) {
							obr2 = localObr2;
						}
					}
					if ((obr2 != null)
							&& (oulMessage.getPATIENT().getPID().getPatientIdentifierList().encode().isEmpty())) {
						exceptions.add(new ValidationException(
								"The PID segment of this message SHALL contain a PID-3 field"));
					}
				}
			}
		} catch (HL7Exception e) {
			log.warn(e.getMessage(), e);
		}
		return exceptions.toArray(new ValidationException[exceptions.size()]);
	}

	@Override
	public String getDescription() {
		return "For Analyzer, usage of PID-3 is mandatory in all messages where field OBR-2 is not carrying a null (\"\") value.";
	}

	@Override
	public String getSectionReference() {
		return "PaLM TF-2b, Rev. 9.0, Section C.9 PID Segment";
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
