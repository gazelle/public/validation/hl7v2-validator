package net.ihe.gazelle.hl7.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import net.ihe.gazelle.hl7.utils.ValidationErrorHandler;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.hl7v3.validator.core.SAXParserFactoryProvider;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

@Name("applicationManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationManager implements Serializable, CSPPoliciesPreferences {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String PROFILE = "Profile";
    private static final String RESOURCE = "Resource";
    private static final String DOT_EXTENSION = ".xml";

    private static Logger log = LoggerFactory.getLogger(ApplicationManager.class);

    private ApplicationConfiguration preference = null;
    private Map<String, List<String>> validationErrorsMap = null;
    private String objectType = null;
    private boolean forceUpdate = false;

    public List<ApplicationConfiguration> getAllPreferences() {
        HQLQueryBuilder<ApplicationConfiguration> builder = new HQLQueryBuilder<ApplicationConfiguration>(
                ApplicationConfiguration.class);
        builder.addOrder("variable", true);
        return builder.getList();
    }

    public void savePreference(ApplicationConfiguration inPreference) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(inPreference);
        entityManager.flush();
        ApplicationConfigurationProvider.instance().init();
        if (inPreference.getVariable().equals("xsd_directory_location")) {
            log.info("reloading SAX Parser Factories...");
            SAXParserFactoryProvider.instance().init();
        }
        setPreference(null);
    }


    public void createNewPreference() {
        setPreference(new ApplicationConfiguration());
    }

    private static FilenameFilter filter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(DOT_EXTENSION);
        }
    };

    @Create
    public void initializeDirectoryPath() {
        hl7MessageProfileDirectoryPath = ApplicationConfiguration.getValueOfVariable("hl7_profiles_directory");
        hl7TablesDirectoryPath = ApplicationConfiguration.getValueOfVariable("hl7_table_directory");
        tosLink = ApplicationConfiguration.getValueOfVariable("link_to_cgu");
    }
    public String getTosLink(){
        return tosLink;
    }
    public void importAllProfilesFromDirectory() {
        objectType = PROFILE;
        validationErrorsMap = new HashMap<String, List<String>>();

        if ((hl7MessageProfileDirectoryPath == null) || hl7MessageProfileDirectoryPath.isEmpty()) {
            FacesMessages.instance().add(
                    StatusMessage.Severity.ERROR, "Path to the HL7 Message profiles is either null or empty. Cannot retrieve data to import");
            return;
        } else {
            ApplicationConfiguration pref = ApplicationConfiguration.getApplicationConfigurationByVariable("hl7_profiles_directory");
            if (pref == null) {
                pref = new ApplicationConfiguration();
                pref.setVariable("hl7_profiles_directory");
                pref.setValue(hl7MessageProfileDirectoryPath);
                pref.save();
            }
            File directory = new File(ApplicationConfiguration.getApplicationConfigurationByVariable("hl7_directory").getValue()+ hl7MessageProfileDirectoryPath);
            if (directory.isDirectory()) {
                Integer count = 0;
                String profileXsd = ApplicationConfiguration.getValueOfVariable("hl7_profile_xsd");
                String[] profiles = directory.list(filter);
                if (profiles == null){
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "There is no HL7 Message Profile available in " + hl7MessageProfileDirectoryPath);
                } else {
                    for (String profilePath : profiles) {
                        log.debug(profilePath);
                        File profileFile = new File(directory, profilePath);
                        String oid = profilePath.replace(DOT_EXTENSION, "");
                        if (importProfile(profileFile, true, oid, profileXsd)) {
                            count++;
                        }
                    }
                }
                FacesMessages.instance().add(StatusMessage.Severity.INFO, count + " profiles have been imported/updated");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, directory.getAbsolutePath() + " is either not available nor readable");
            }
        }
    }

    public void importfile(String filePath) {
        File file = new File(filePath);
        String oid = file.getName().replace(DOT_EXTENSION, "");
        if (objectType.equals(PROFILE)) {
            if (importProfile(file, false, oid, null)) {
                validationErrorsMap.remove(filePath);
            }
        } else {
            if (importResource(file, false, oid, null)) {
                validationErrorsMap.remove(filePath);
            }
        }
    }

    private boolean importProfile(File profileFile, boolean performChecks, String oid, String profileXsd) {
        long datetime = profileFile.lastModified();
        Date modifiedDate = new Date(datetime);

        try {
            Profile existingProfile = Profile.getProfileByOID(oid, null);
            boolean ok = isXmlWellFormedAndValid(profileFile, profileXsd);
            if (existingProfile == null) {
                if (!performChecks || ok) {
                    log.info("importing a new profile");
                    byte[] profileContent = getBytesFromFile(profileFile);
                    Profile profile = new Profile(oid, profileContent);
                    profile.setLastChanged(new Date());
                    profile.setLastModifier(Identity.instance().getCredentials().getUsername());
                    // if no check is performed that means that the profile contains error but the admin forced its import
                    profile.setImportedWithErrors(!performChecks);
                    try {
                        profile.setRevision(getRevisionNumber(profileFile.getCanonicalPath()));
                    } catch (Exception e) {
                        log.warn("Not able to detect the revision of the file " + profileFile.getCanonicalPath());
                    }
                    profile.save();
                    return true;
                }else if (!ok){
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is not a valid HL7 Message profile");
                    log.error("Cannot create profile with OID " + oid);
                    return false;
                }
            } else if (forceUpdate || modifiedDate.after(existingProfile.getLastChanged())) {
                if (!performChecks || isXmlWellFormedAndValid(profileFile, profileXsd)) {
                    log.info("File on disk is newer, updating profile content");
                    existingProfile.setContent(getBytesFromFile(profileFile));
                    existingProfile.setLastChanged(new Date());
                    existingProfile.setLastModifier(Identity.instance().getCredentials().getUsername());
                    try {
                        existingProfile.setRevision(getRevisionNumber(profileFile.getCanonicalPath()));
                    } catch (Exception e) {
                        log.warn("Not able to detect the revision of the file " + profileFile.getCanonicalPath());
                    }
                    // if no check is performed that means that the profile contains error but the admin forced its import
                    existingProfile.setImportedWithErrors(!performChecks);
                    existingProfile.save();
                    return true;
                }  else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "File " + existingProfile.getOid() + " is either not well-formed or not valid");
                }
            } else {
                log.info("Profile with OID " + oid + " is already stored in database");
                return false;
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    public void importAllResourcesFromDirectory() {
        objectType = RESOURCE;
        validationErrorsMap = new HashMap<String, List<String>>();
        if ((hl7TablesDirectoryPath == null) || hl7TablesDirectoryPath.isEmpty()) {
            FacesMessages.instance().add(
                    StatusMessage.Severity.ERROR, "Path to the HL7 tables is either null or empty. Cannot retrieve data to import");
            return;
        } else {
            ApplicationConfiguration pref = ApplicationConfiguration
                    .getApplicationConfigurationByVariable("hl7_table_directory");
            if (pref == null) {
                pref = new ApplicationConfiguration();
                pref.setVariable("hl7_table_directory");
                pref.setValue(hl7TablesDirectoryPath);
                pref.save();
            }
            File directory = new File(ApplicationConfiguration
                    .getApplicationConfigurationByVariable("hl7_directory").getValue()+ hl7TablesDirectoryPath);
            if (directory.isDirectory()) {
                Integer count = 0;
                String tableXsd = ApplicationConfiguration.getValueOfVariable("hl7_resource_xsd");
                String[] tables = directory.list(filter);
                if (tables == null){
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "There is no HL7 Resource file available in directory " + hl7MessageProfileDirectoryPath);
                }else {
                    for (String tablePath : tables) {
                        File tableFile = new File(directory, tablePath);
                        String oid = tablePath.replace(DOT_EXTENSION, "");
                        if (importResource(tableFile, true, oid, tableXsd)) {
                            count++;
                        }
                    }
                }
                FacesMessages.instance().add(StatusMessage.Severity.INFO, count + " tables have been imported/updated");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, directory.getAbsolutePath() + " does not exist or is not readable");
            }
        }
    }

    private boolean importResource(File tableFile, boolean performChecks, String oid, String tableXsd) {
        long datetime = tableFile.lastModified();
        Date modifiedDate = new Date(datetime);
        try {
            Resource existingResource = Resource.getResourceByOID(oid);
            if (existingResource == null) {
                if (!performChecks || isXmlWellFormedAndValid(tableFile, tableXsd)) {
                    byte[] tableContent = getBytesFromFile(tableFile);
                    Resource resource = new Resource(oid, tableContent);
                    resource.setLastChanged(new Date());
                    // if no check is performed that means that the resource contains error but the admin forced its import
                    resource.setImportedWithErrors(!performChecks);
                    resource.setLastModifier(Identity.instance().getCredentials().getUsername());
                    try {
                        resource.setRevision(getRevisionNumber(tableFile.getCanonicalPath()));
                    } catch (Exception e) {
                        log.warn("Not able to detect the revision of the file " + tableFile.getCanonicalPath());
                    }
                    resource.save();
                    return true;
                }
            } else if (forceUpdate || modifiedDate.after(existingResource.getLastChanged())) {
                if (!performChecks || isXmlWellFormedAndValid(tableFile, tableXsd)) {
                    log.info("File on disk is newer, updating resource content");
                    existingResource.setContent(getBytesFromFile(tableFile));
                    existingResource.setLastChanged(new Date());
                    existingResource.setLastModifier(Identity.instance().getCredentials().getUsername());
                    // if no check is performed that means that the resource contains error but the admin forced its import
                    existingResource.setImportedWithErrors(!performChecks);
                    try {
                        existingResource.setRevision(getRevisionNumber(tableFile.getCanonicalPath()));
                    } catch (Exception e) {
                        log.warn("Not able to detect the revision of the file " + tableFile.getCanonicalPath());
                    }
                    existingResource.save();
                    return true;
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "File " + existingResource.getOid() + " is either not well-formed or not valid");
                }
            } else {
                log.debug("Resource with OID " + oid + " is already up-to-date");
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    private byte[] getBytesFromFile(File file) {
        FileInputStream fis = null;
        byte[] buffer = null;
        try {
            fis = new FileInputStream(file);
            buffer = new byte[(int) file.length()];
            fis.read(buffer);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return buffer;
    }

    private boolean isXmlWellFormedAndValid(File file, String xsdLocation) {
        if (xsdLocation == null) {
            return true;
        } else {
            SAXParser parser;
            DefaultHandler saxHandler;
            // is XML well-formed
            try {
                SAXParserFactory spfactory = SAXParserFactory.newInstance();
                spfactory.setNamespaceAware(true);
                spfactory.setXIncludeAware(true);
                spfactory.setValidating(true);
                parser = spfactory.newSAXParser();
                saxHandler = new DefaultHandler();
            } catch (Exception e) {
                log.error("Error: Cannot initialize SAX parser", e);
                return false;
            }
            try {
                parser.parse(file, saxHandler);
                // is XML valid
                StreamSource xsdSource = new StreamSource(xsdLocation);
                DocumentBuilderFactory docfactory = DocumentBuilderFactory.newInstance();
                docfactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                        "http://www.w3.org/2001/XMLSchema");
                docfactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaSource", xsdSource);
                docfactory.setNamespaceAware(true);
                docfactory.setValidating(false);
                docfactory.setXIncludeAware(true);
                DocumentBuilder docBuilder = docfactory.newDocumentBuilder();
                ValidationErrorHandler handler = new ValidationErrorHandler();
                docBuilder.setErrorHandler(handler);
                Document document = docBuilder.parse(file);
                SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema schema = factory.newSchema(xsdSource);
                Validator validator = schema.newValidator();
                validator.setErrorHandler(handler);
                DOMSource domSource = new DOMSource(document);
                validator.validate(domSource);
                if (handler.getNbOfErrors() > 0) {
                    validationErrorsMap.put(file.getPath(), handler.getErrorMessages());
                    return false;
                }
                return true;
            } catch (SAXException e) {
                log.error(e.getMessage(), e);
                return false;
            } catch (ParserConfigurationException e) {
                log.error(e.getMessage(), e);
                return false;
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                return false;
            }
        }
    }

    public void setPreference(ApplicationConfiguration preference) {
        this.preference = preference;
    }

    public ApplicationConfiguration getPreference() {
        return preference;
    }

    private String hl7TablesDirectoryPath;

    private String hl7MessageProfileDirectoryPath;
    private String tosLink;

    public String getHl7TablesDirectoryPath() {
        return hl7TablesDirectoryPath;
    }

    public void setHl7TablesDirectoryPath(String hL7TablesDirectoryPath) {
        hl7TablesDirectoryPath = hL7TablesDirectoryPath;
    }

    public String getHl7MessageProfileDirectoryPath() {
        return hl7MessageProfileDirectoryPath;
    }

    public void setHl7MessageProfileDirectoryPath(String hL7MessageProfileDirectoryPath) {
        hl7MessageProfileDirectoryPath = hL7MessageProfileDirectoryPath;
    }

    public Map<String, List<String>> getValidationErrorsMap() {
        return validationErrorsMap;
    }

    public String[] getValidationErrorsMapKeys() {
        if ((validationErrorsMap != null) && !validationErrorsMap.isEmpty()) {
            return validationErrorsMap.keySet().toArray(new String[validationErrorsMap.size()]);
        } else {
            return null;
        }
    }

    @Override
    public boolean isContentPolicyActivated() {
        return (new GazelleHL7ValidatorPreferenceProvider()).getBoolean("security_policies");
    }

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(PreferencesKey.SECURITY_POLICIES.getFriendlyName(), String
                .valueOf((new GazelleHL7ValidatorPreferenceProvider()).getBoolean(PreferencesKey.SECURITY_POLICIES
                        .getFriendlyName())));
        headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
        headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(), ApplicationConfiguration
                .getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
                ApplicationConfiguration
                        .getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY
                                .getFriendlyName()));
        headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES
                        .getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
                ApplicationConfiguration
                        .getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY
                                .getFriendlyName()));
        return headers;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return (new GazelleHL7ValidatorPreferenceProvider()).getBoolean(PreferencesKey.SQL_INJECTION_FILTER_SWITCH
                .getFriendlyName());
    }

    public void resetHttpHeaders() {
        log.info("Reset http headers to default values");
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ApplicationConfiguration ap = null;
        for (PreferencesKey currentPreference : PreferencesKey.values()) {
            ApplicationConfigurationQuery applicationConfigurationQuery = new ApplicationConfigurationQuery();
            String friendlyName = currentPreference.getFriendlyName();
            applicationConfigurationQuery.variable().eq(friendlyName);
            ap = applicationConfigurationQuery.getUniqueResult();

            if (ap == null) {
                ap = new ApplicationConfiguration();
            }
            ap.setVariable(friendlyName);
            ap.setValue(currentPreference.getDefaultValue());
            entityManager.merge(ap);
            entityManager.flush();
        }
        CSPHeaderFilter.clearCache();
    }

    public void updateHttpHeaders() {
        CSPHeaderFilter.clearCache();
    }

    private String getRevisionNumber(String filepath) throws Exception {
        Process process = Runtime.getRuntime().exec(new String[] { "svn", "info", "--xml", filepath });
        InputStream outcome = process.getInputStream();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document svninfo = builder.parse(new InputSource(outcome));
        NodeList commitNode = svninfo.getElementsByTagName("entry");
        if (commitNode.getLength() > 0) {
            NamedNodeMap attributes = commitNode.item(0).getAttributes();
            return attributes.getNamedItem("revision").getTextContent();

        } else {
            return null;
        }
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getDocumentationUrl(){
        return PreferenceService.getString("application_documentation_url");
    }
}
