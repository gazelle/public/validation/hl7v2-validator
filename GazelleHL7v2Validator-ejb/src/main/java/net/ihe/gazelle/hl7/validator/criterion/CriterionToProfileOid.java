package net.ihe.gazelle.hl7.validator.criterion;

import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hql.criterion.PropertyCriterion;

public class CriterionToProfileOid extends PropertyCriterion<HL7v2ValidationLog, String> {

	public static final CriterionToProfileOid SINGLETON = new CriterionToProfileOid();

	protected CriterionToProfileOid() {
		super(String.class, "profileOid", "this", "profileOid");
	}

	@Override
	public String getSelectableLabelNotNull(String instance) {
		return instance;
	}

}
