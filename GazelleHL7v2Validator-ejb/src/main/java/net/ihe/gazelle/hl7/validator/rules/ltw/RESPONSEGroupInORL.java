package net.ihe.gazelle.hl7.validator.rules.ltw;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

/**
 * <p>RESPONSEGroupInORL class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RESPONSEGroupInORL extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7719164963690719193L;

	/** {@inheritDoc} */
	@Override
	public ValidationException[] test(Message msg) {
		Terser terser = new Terser(msg);
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			String ack = terser.get("/.MSA-1");
			if ((ack != null) && !(ack.equals("AE") || ack.equals("AR"))) {
				Structure[] responseGroup = msg.getAll("RESPONSE");
				if ((responseGroup == null) || (responseGroup.length != 1)) {
					exceptions.add(new ValidationException("This message SHALL contain exactly one RESPONSE group"));
				}
			} else {
				return null;
			}
		} catch (HL7Exception e) {
			exceptions.add(new ValidationException(e.getMessage()));
		}
		return exceptions.toArray(new ValidationException[exceptions.size()]);
	}

	/** {@inheritDoc} */
	@Override
	public String getDescription() {
		return "the RESPONSE segment group is mandatory unless in case of error (MSA-1 = AE or AR)";
	}

	/** {@inheritDoc} */
	@Override
	public String getSectionReference() {
		return "Laboratory TF-2 - Section 4.5.4";
	}

	/** {@inheritDoc} */
	@Override
	public int getSeverity() {
		return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
	}

}
