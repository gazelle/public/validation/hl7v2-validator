/*
 * Apache2 license - Copyright (c) IHE-Europe 2015
 *
 */

package net.ihe.gazelle.hl7.validator.rules.law;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.model.v251.datatype.CE;
import ca.uhn.hl7v2.model.v251.datatype.EIP;
import ca.uhn.hl7v2.model.v251.datatype.XCN;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import org.apache.log4j.Logger;

/**
 * <b>Class Description : </b>ComponentsInLAW<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/06/15
 *
 *
 *
 */
public class DatatypesInLAW extends IHEValidationRule {

    private static Logger log = Logger.getLogger(DatatypesInLAW.class);

    @Override
    public int getSeverity() {
        return GazelleErrorCode.USAGE.getCode();
    }

    @Override
    public ValidationException[] test(Message msg) {
        return null;
    }

    @Override
    public ValidationException[] test(Message msg, String hl7path) {
        String path = formatHl7Path(hl7path);
        Terser terser = new Terser(msg);
        String pathToField = path.substring(BEGIN_INDEX, path.lastIndexOf('/'));
        try {
            String pathToSegment = pathToField.substring(0, pathToField.lastIndexOf('/'));
            if (hl7path.contains("SAC-24")) {
                log.debug("segment at " + pathToSegment);
                testCE3(terser.getSegment(pathToSegment).getField(24, 0));
            } else if (hl7path.contains("OBR-16")) {
                pathToSegment = pathToSegment.substring(0, pathToSegment.lastIndexOf('/'));
                log.debug("segment at " + pathToSegment);
                testOBR16(terser.getSegment(pathToSegment));
            } else if (hl7path.contains("OBX-6")) {
                log.debug("segment at " + pathToSegment);
                testCE3(terser.getSegment(pathToSegment).getField(6, 0));
            } else if (hl7path.contains("OBX-3") && hl7path.contains("CE-5")) {
                log.debug("segment at " + pathToSegment);
                testCE5(terser.getSegment(pathToSegment).getField(3, 0));
            } else if (hl7path.contains("OBX-3") && hl7path.contains("CE-6")) {
                log.debug("segment at " + pathToSegment);
                testCE6(terser.getSegment(pathToSegment).getField(3, 0));
            } else if (hl7path.contains("SPM-3")) {
                pathToSegment = pathToSegment.substring(0, pathToSegment.lastIndexOf('/'));
                log.debug("segment at " + pathToSegment);
                Integer position = pathToField.lastIndexOf('(');
                String repetition = pathToField.substring(position + 1, position + 2);
                log.debug("repetition: " + repetition);
                testEI(terser.getSegment(pathToSegment).getField(3, Integer.valueOf(repetition)));
            } else if (hl7path.contains("SPM-2")){
                pathToSegment = pathToSegment.substring(0, pathToSegment.lastIndexOf('/'));
                log.debug("segment at " + pathToSegment);
                testEI(terser.getSegment(pathToSegment).getField(2, 0));
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage());
        } catch (ValidationException e) {
            return new ValidationException[]{e};
        }
        return null;
    }

    private void testEI(Type field) throws ValidationException {
        try {
            if (!field.isEmpty() && field instanceof EIP) {
                EIP eip = (EIP) field;
                String namespaceID = eip.getPlacerAssignedIdentifier().getNamespaceID().encode();
                String universalID = eip.getPlacerAssignedIdentifier().getUniversalID().encode();
                String universalIDType = eip.getPlacerAssignedIdentifier().getUniversalIDType().encode();
                if (namespaceID.isEmpty() && universalID.isEmpty() && universalIDType.isEmpty()) {
                    throw new ValidationException("Either Namespace ID or both Universal ID and Universal ID Type are required");
                } else if ((universalID.isEmpty() && !universalIDType.isEmpty()) || (!universalID.isEmpty() && universalIDType.isEmpty())) {
                    throw new ValidationException("Both Universal ID and Universal ID Type are required");
                }
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage(), e);
        }
    }

    private void testCE6(Type field) throws ValidationException {
        try {
            if (!field.isEmpty() && field instanceof CE) {
                CE ce = (CE) field;
                String alternateCodeSystem = ce.getNameOfAlternateCodingSystem().encode();
                String alternateIdentifier = ce.getAlternateIdentifier().encode();
                if (alternateIdentifier.isEmpty() && !alternateCodeSystem.isEmpty()) {
                    throw new ValidationException("Usage of 'Name of Alternate Coding System' is not supported when 'Alternate Identifier' is not populated");
                } else if (!alternateIdentifier.isEmpty() && alternateCodeSystem.isEmpty()) {
                    throw new ValidationException("Usage of 'Name of Alternate Coding System' is required when 'Alternate Identifier' is populated");
                }
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage());
        }
    }

    private void testCE5(Type field) throws ValidationException {
        try {
            if (!field.isEmpty() && field instanceof CE) {
                CE ce = (CE) field;
                String alternateText = ce.getAlternateText().encode();
                String alternateIdentifier = ce.getAlternateIdentifier().encode();
                if (alternateIdentifier.isEmpty() && !alternateText.isEmpty()) {
                    throw new ValidationException("Usage of 'Alternate Text' is not supported when 'Alternate Identifier' is not populated");
                } else if (!alternateIdentifier.isEmpty() && alternateText.isEmpty()) {
                    throw new ValidationException("Usage of 'Alternate Text' is required when 'Alternate Identifier' is populated");
                }
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage());
        }
    }

    private void testCE3(Type field) throws ValidationException {
        try {
            if (!field.isEmpty() && field instanceof CE) {
                CE ce = (CE) field;
                String codingSystem = ce.getNameOfCodingSystem().encode();
                String identifier = ce.getIdentifier().encode();
                if (identifier.isEmpty() && !codingSystem.isEmpty()) {
                    throw new ValidationException("Usage of 'Name of Coding System' is not supported when identifier is not populated");
                } else if (!identifier.isEmpty() && codingSystem.isEmpty()) {
                    throw new ValidationException("Usage of 'Name of Coding System' is required when identifier is populated");
                }
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage());
        }
    }

    private void testOBR16(Segment obr) throws ValidationException {
        try {
            Type obr16 = obr.getField(16, 0);
            if (!obr16.isEmpty() && obr16 instanceof XCN) {
                XCN xcn = (XCN) obr16;
                String universalIdType = xcn.getAssigningAuthority().getUniversalIDType().encode();
                String universalId = xcn.getAssigningAuthority().getUniversalID().encode();
                if (universalId.isEmpty() && !universalIdType.isEmpty()) {
                    throw new ValidationException("Usage of HD-3 is not supported when HD-2 is not populated");
                } else if (!universalId.isEmpty() && universalIdType.isEmpty()) {
                    throw new ValidationException("Usage of HD-3 is required when HD-2 is populated");
                }
            }
        } catch (HL7Exception e) {
            log.debug(e.getMessage());
        }
    }

    @Override
    public String getDescription() {
        return "Rules on components/subcomponents in datatypes in LAW profile";
    }

    @Override
    public String getSectionReference() {
        return "PaLM TF-2b Rev. 9.0 - several sections";
    }
}
