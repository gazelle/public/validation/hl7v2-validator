package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.IS;
import ca.uhn.hl7v2.model.v25.datatype.ST;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.i18n.Msg;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

/**
 * Abstract INS rule
 * @author pdt
 *
 */
public abstract class INSValidationRule extends IHEValidationRule {
	public class INSValidationException extends ValidationException {
		INSValidationException(final Object... parameters) {
			super(INSValidationRule.this.assertionTag()+msg.get("exception",parameters));
			log.debug(this.getMessage());
		}
	}

	protected final static Logger log = LoggerFactory.getLogger(INSValidationRule.class);
	protected final Pattern matriculesINS = Pattern.compile("^(1.2.250.1.213.1.4.8|1.2.250.1.213.1.4.9|1.2.250.1.213.1.4.10|1.2.250.1.213.1.4.11)$");
	protected final Pattern identifierTypeCodeINS = Pattern.compile("^(INS)$");
	protected Msg msg;

	public INSValidationRule() {
		msg = Msg.getMessages(this.getClass());
	}

	public String assertionTag() {
		return msg.get("assertion-tag", assertionId());
	}
	private String assertionId() {
		return this.getClass().getSimpleName().replaceFirst("^INS(\\d{3}).*","INS-$1");
	}

	protected boolean isEmpty(Type t) throws HL7Exception {
		return t==null || t.isEmpty();
	}

	protected boolean hasMatriculeINS(PID pid) throws HL7Exception {
		for(CX cx : pid.getPatientIdentifierList()) {
			ST matricule = cx.getCx4_AssigningAuthority().getHd2_UniversalID();
			String identifierTypeCode = cx.getCx5_IdentifierTypeCode().getValue();
			if (matriculesINS.matcher(matricule.encode()).matches() && identifierTypeCodeINS.matcher(identifierTypeCode).matches()) {
				return true;
			}
		}
		return false;
	}

	protected boolean isIdentiteQualifiee(PID pid) {
		for (IS is : pid.getIdentityReliabilityCode()) {
			if ("VALI".equals(is.getValueOrEmpty())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ValidationException[] test(Message msg) {
		try {
			Terser terser = new Terser(msg);
			PID pid = (PID)terser.getFinder().findSegment("*PID",0);
			return validate(pid);
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
			return null;
		}
	}

	protected abstract ValidationException[] validate(PID pid) throws HL7Exception;

	@Override
	public String getSectionReference() {
		return "Publication-IHE_FRANCE_PAM_National_Extension_v29, IHE Change Proposal CP-ITI-FR-2019-13 6-INS_NIR, "+msg.get("section");
	}

	@Override
	public String getDescription() {
		return msg.get("description");
	}

}
