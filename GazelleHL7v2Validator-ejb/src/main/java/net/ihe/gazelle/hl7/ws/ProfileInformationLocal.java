package net.ihe.gazelle.hl7.ws;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Local
@Path("/")
public interface ProfileInformationLocal {

	@GET
	@Path("/GetPackageForProfile")
	@Produces("text/plain")
	public String getJavaPackageForProfileByOID(@QueryParam("oid") String profileOID);
}
