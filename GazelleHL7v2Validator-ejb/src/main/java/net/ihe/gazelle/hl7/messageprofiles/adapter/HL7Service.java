/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.adapter;

import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.tf.concepts.IHEConceptsServiceStub;
import net.ihe.gazelle.tf.concepts.SOAPExceptionException;
import net.ihe.gazelle.tf.messageprofile.Hl7MessageProfilesServiceStub;
import net.ihe.gazelle.tf.ws.*;
import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

/**
 * <b>Class Description : </b>HL7MessageProfileCreationManager<br>
 * <br>
 * This class implements the local interface HL7MessageProfileCreationManager dedicated to the creation of new HL7 Message Profile, stored in the
 * database of this application and referenced in Gazelle
 * Master Model
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, December 21st
 */

public class HL7Service implements Serializable {

    private static Logger log = LoggerFactory.getLogger(HL7Service.class);

    /**
     * Name of the preference (cmn_application_preference) to retrieve the wsdl of IHEConcepts ws
     */
    private static final String IHE_CONCEPTS_ENDPOINT_NAME = "gmm_iheconcepts_wsdl";

    /**
     * Name of the preference (cmn_application_prefenrence) to retrieve the wsdl of Hl7MessageProfile ws
     */
    private static final String HL7_MESSAGE_PROFILE_ENDPOINT_NAME = "gmm_hl7messageprofile_wsdl";

    /**
     * Stub to connect to the web service which provides the list of domains, actors and transactions
     */
    private IHEConceptsServiceStub iheConceptsStub;

    /**
     * Stub to connect to the web service which provides the method to register a new profile
     */
    private Hl7MessageProfilesServiceStub hl7MessageProfilesStub;


    /**
     * Constructor ofr HL7MessageProfileCreationManager
     *
     * @throws ControllerException
     */
    public HL7Service() throws ControllerException {
        String endpoint = ApplicationConfiguration.getValueOfVariable(IHE_CONCEPTS_ENDPOINT_NAME);
        if (endpoint == null) {
            throw new ControllerException("IHE Concepts Preference Is Missing", "gazelle.hl7mpr.IHEConceptsPreferenceIsMissing",
                    IHE_CONCEPTS_ENDPOINT_NAME);
        }
        try {
            iheConceptsStub = new IHEConceptsServiceStub(endpoint);
            iheConceptsStub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
        } catch (AxisFault e) {
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotConnectToIHEConcepts");
        }


        endpoint = ApplicationConfiguration.getValueOfVariable(HL7_MESSAGE_PROFILE_ENDPOINT_NAME);
        if (endpoint == null) {
            throw new ControllerException("HL7MessageProfile Preference Is Missing", "gazelle.hl7mpr.HL7MessageProfilePreferenceIsMissing",
                    HL7_MESSAGE_PROFILE_ENDPOINT_NAME);
        }
        try {
            hl7MessageProfilesStub = new Hl7MessageProfilesServiceStub(endpoint);
            hl7MessageProfilesStub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION,
                    true);
        } catch (AxisFault axisFault) {
            throw new ControllerException(axisFault.getMessage(), "gazelle.hl7mpr.cannotConnectToHl7MessageProfile");
        }


    }

    /**
     * Retrieves a domain for selected keyword
     *
     * @param selectedDomainKeyword
     * @return the domain for selected keyword
     * @throws ControllerException
     */
    public Domain getDomain(String selectedDomainKeyword) throws ControllerException {

        GetDomainByKeyword param = new GetDomainByKeyword();
        param.setDomainKeyword(selectedDomainKeyword);
        GetDomainByKeywordE paramE = new GetDomainByKeywordE();
        paramE.setGetDomainByKeyword(param);
        try {
            return iheConceptsStub.getDomainByKeyword(paramE).getGetDomainByKeywordResponse()
                    .getReturnedDomain();
        } catch (RemoteException e) {

            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetDomains");
        } catch (SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr.CannotGetDomains");
        }

    }


    /**
     * retrieves a list of actors for a selected domain keyword
     *
     * @param selectedDomainKeyword
     * @return
     * @throws ControllerException
     */
    public Actor[] getListOfActors(String selectedDomainKeyword) throws ControllerException {

        GetListOfActorObjectsForGivenDomain lparam = new GetListOfActorObjectsForGivenDomain();
        lparam.setDomainKeyword(selectedDomainKeyword);
        GetListOfActorObjectsForGivenDomainE lparamE = new GetListOfActorObjectsForGivenDomainE();
        lparamE.setGetListOfActorObjectsForGivenDomain(lparam);

        try {
            GetListOfActorObjectsForGivenDomainResponseE responseE = iheConceptsStub
                    .getListOfActorObjectsForGivenDomain(lparamE);
            return responseE.getGetListOfActorObjectsForGivenDomainResponse().getReturnedActors();
        } catch (RemoteException e) {
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetActors");
        } catch (SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr.CannotGetActors");
        }
    }

    /**
     * Get list of domains
     *
     * @return
     * @throws ControllerException
     */
    public Domain[] getListofDomains() throws ControllerException {
        GetAllDomainsObject param = new GetAllDomainsObject();
        GetAllDomainsObjectE paramE = new GetAllDomainsObjectE();
        paramE.setGetAllDomainsObject(param);
        try {
            GetAllDomainsObjectResponseE responseE = iheConceptsStub.getAllDomainsObject(paramE);
            return responseE.getGetAllDomainsObjectResponse().getDomains();
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetDomains");
        }

    }

    /**
     * Get an actor for a selected keyword
     *
     * @param selectedActorKeyword
     * @return
     * @throws ControllerException
     */
    public Actor getActor(String selectedActorKeyword) throws ControllerException {

        GetActorByKeyword parama = new GetActorByKeyword();
        parama.setActorKeyword(selectedActorKeyword);
        GetActorByKeywordE paramaE = new GetActorByKeywordE();
        paramaE.setGetActorByKeyword(parama);
        try {
            return iheConceptsStub.getActorByKeyword(paramaE).getGetActorByKeywordResponse().getReturnedActor();
        } catch (RemoteException e) {
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        } catch (SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        }
    }

    /**
     * get a list of transactions for an actor
     *
     * @param selectedActorKeyword
     * @return
     * @throws ControllerException
     */
    public Transaction[] getListOfTransactionsForActor(String selectedActorKeyword) throws ControllerException {
        GetListOfTransactionObjectsForGivenActor param = new GetListOfTransactionObjectsForGivenActor();
        param.setActorKeyword(selectedActorKeyword);
        GetListOfTransactionObjectsForGivenActorE paramE = new GetListOfTransactionObjectsForGivenActorE();
        paramE.setGetListOfTransactionObjectsForGivenActor(param);
        try {
            GetListOfTransactionObjectsForGivenActorResponseE responseE = iheConceptsStub
                    .getListOfTransactionObjectsForGivenActor(paramE);
            return responseE.getGetListOfTransactionObjectsForGivenActorResponse()
                    .getReturnedTransactions();
        } catch (RemoteException e) {
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        } catch (SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        }
    }

    /**
     * get a transaction for a selected keyword
     *
     * @param selectedTransactionKeyword
     * @return
     * @throws ControllerException
     */
    public Transaction getTransaction(String selectedTransactionKeyword) throws ControllerException {
        GetTransactionByKeyword param = new GetTransactionByKeyword();
        param.setTransactionKeyword(selectedTransactionKeyword);
        GetTransactionByKeywordE paramE = new GetTransactionByKeywordE();
        paramE.setGetTransactionByKeyword(param);
        try {
            return iheConceptsStub.getTransactionByKeyword(paramE).getGetTransactionByKeywordResponse()
                    .getReturnedTransaction();
        } catch (RemoteException e) {
            throw new ControllerException(e.getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        } catch (SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr.CannotGetTransactions");
        }

    }


    /**
     * Save in data base new reference
     *
     * @param selectedReference
     * @return
     * @throws ControllerException
     */
    public boolean addNewReference(Hl7MessageProfile selectedReference) throws ControllerException {
        boolean isSuccessful = false;
        AddNewReference param = new AddNewReference();
        param.setHl7MessageProfile(selectedReference);
        AddNewReferenceE paramE = new AddNewReferenceE();
        paramE.setAddNewReference(param);
        try {
            AddNewReferenceResponseE responseE = hl7MessageProfilesStub.addNewReference(paramE);
            isSuccessful = responseE.getAddNewReferenceResponse().getSuccess();

        } catch (RemoteException remoteException) {
            throw new ControllerException(remoteException.getMessage(), "gazelle.hl7mpr.cannotSubmitProfile");
        } catch (net.ihe.gazelle.tf.messageprofile.SOAPExceptionException soapExceptionException) {
            throw new ControllerException(soapExceptionException.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr" +
                    ".cannotSubmitProfile");
        }
        return isSuccessful;
    }

    /**
     * @throws RemoteException
     * @throws AxisFault
     * @throws net.ihe.gazelle.tf.messageprofile.SOAPExceptionException
     */
    public Hl7MessageProfile getReferenceForSelectedProfile(String selectedOid) throws ControllerException {

        GetHL7MessageProfileByOID param = new GetHL7MessageProfileByOID();
        param.setMessageProfileOid(selectedOid);
        GetHL7MessageProfileByOIDE paramE = new GetHL7MessageProfileByOIDE();
        paramE.setGetHL7MessageProfileByOID(param);
        try {
            GetHL7MessageProfileByOIDResponseE responseE = hl7MessageProfilesStub.getHL7MessageProfileByOID(paramE);
            return responseE.getGetHL7MessageProfileByOIDResponse().getReturnedMessageProfile();
        } catch (RemoteException remoteException) {
            throw new ControllerException(remoteException.getMessage(), "gazelle.hl7mpr.cannotSubmitProfile");

        } catch (net.ihe.gazelle.tf.messageprofile.SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "gazelle.hl7mpr" +
                    ".cannotSubmitProfile");
        }

    }

    /**
     * Get All message profiles
     * @return
     * @throws ControllerException
     */
    public Hl7MessageProfile[] getAllMessageProfiles() throws ControllerException {
        GetAllMessageProfiles param = new GetAllMessageProfiles();
        GetAllMessageProfilesE paramE = new GetAllMessageProfilesE();
        paramE.setGetAllMessageProfiles(param);
        try {
            GetAllMessageProfilesResponseE responseE = hl7MessageProfilesStub.getAllMessageProfiles(paramE);
            return responseE.getGetAllMessageProfilesResponse().getMessageProfiles();
        } catch (RemoteException remoteException) {
            throw new ControllerException(remoteException.getMessage(), "gazelle.hl7mpr.cannotRetrieveListOfProfiles");
        }

    }

    /**
     * Retrieves a profile by oid
     * @param profileOid
     * @return
     * @throws ControllerException
     */
    public Hl7MessageProfile getProfileByOid(String profileOid) throws ControllerException {
        Hl7MessageProfile profile = null;
        GetHL7MessageProfileByOID params = new GetHL7MessageProfileByOID();
        params.setMessageProfileOid(profileOid);
        GetHL7MessageProfileByOIDE paramsE = new GetHL7MessageProfileByOIDE();
        paramsE.setGetHL7MessageProfileByOID(params);
        try {
            if (this.hl7MessageProfilesStub != null) {
                profile = this.hl7MessageProfilesStub.getHL7MessageProfileByOID(paramsE)
                        .getGetHL7MessageProfileByOIDResponse().getReturnedMessageProfile();

            }
        } catch (RemoteException remoteException) {
            throw new ControllerException(remoteException.getMessage(), "");
        } catch (net.ihe.gazelle.tf.messageprofile.SOAPExceptionException e) {
            throw new ControllerException(e.getFaultMessage().getSOAPException().getMessage(), "");
        }

        return profile;

    }

    /**
     * Get a list of profiles
     * @param wsParams
     * @return
     * @throws ControllerException
     */
    public List<Hl7MessageProfile> getProfileFiltered(GetListOfPossibleHL7MessageProfileForGivenContext wsParams) throws ControllerException {

        GetListOfPossibleHL7MessageProfileForGivenContextE paramsE = new GetListOfPossibleHL7MessageProfileForGivenContextE();
        paramsE.setGetListOfPossibleHL7MessageProfileForGivenContext(wsParams);
        try {
            Hl7MessageProfile[] profiles ;
            profiles = this.hl7MessageProfilesStub.getListOfPossibleHL7MessageProfileForGivenContext(paramsE)
                    .getGetListOfPossibleHL7MessageProfileForGivenContextResponse().getReturnedMessageProfile();
            return Arrays.asList(profiles);
        } catch (RemoteException e) {
            throw new ControllerException(e.getMessage(),"");
        } catch (net.ihe.gazelle.tf.messageprofile.SOAPExceptionException e) {
            throw new ControllerException( e.getFaultMessage().getSOAPException().getMessage(),"");
        }

    }


}






