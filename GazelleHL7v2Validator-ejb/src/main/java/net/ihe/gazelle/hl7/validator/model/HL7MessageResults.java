package net.ihe.gazelle.hl7.validator.model;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by xfs on 06/01/16.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("hl7MessageResults")
@Table(name = "hl7_message", schema = "public")
@SequenceGenerator(name = "hl7_message_sequence", sequenceName = "hl7_message_id_seq", allocationSize = 1)
public class HL7MessageResults implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hl7_message_sequence")
    private Integer id;


   @Lob
    @Type(type = "text")
    @Column(name = "detailed_results")
    private String detailedResults;

    /*@Transient
   private String detailedResults;*/

   /**
    * <p>Getter for the field <code>id</code>.</p>
    *
    * @return a {@link java.lang.Integer} object.
    */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>get.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String get() {
        return detailedResults;
    }

    /**
     * <p>set.</p>
     *
     * @param detailedResults a {@link java.lang.String} object.
     */
    public void set(String detailedResults) {
        this.detailedResults = detailedResults;
    }


}
