package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

import java.util.ArrayList;
import java.util.List;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-010 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-010)
 * predicate: Dans le cas où l'adresse véhiculée est celle du lieu de naissance (XAD-7 = BDL), le Code Officiel Géographique (COG) de la commune pour les personnes nées en France ou du pays pour les personnes nées à létranger doit être renseigné dans le champ XAD-9.
 * Prescription level: Mandatory / Required / Shall
 * Page: 15
 * Section: N.6.8
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:45 by aberge
 * Commentaire: Si PID-11-7 = BDL alors PID-11-9 présent et non vide (longueur 5)

 * @author pdt
 *
 */
public class INS010RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		List<ValidationException> exceptions = new ArrayList<>();
		for (XAD xad : pid.getPatientAddress()) {
			// Si PID-11-7 = BDL alors PID-11-9 présent et non vide (longueur 5)
			if ("BDL".equals(xad.getAddressType().getValueOrEmpty()) && xad.getCountyParishCode().getValueOrEmpty().length()!=5) {
				exceptions.add(new INSValidationException(xad.toString()));
			}
		}
		return exceptions.isEmpty() ? null : exceptions.toArray(new ValidationException[]{});
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
