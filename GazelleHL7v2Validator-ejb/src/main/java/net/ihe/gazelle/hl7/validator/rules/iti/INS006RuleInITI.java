package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-006 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-006)
 * predicate: Dans le cas où l'on véhicule l'INS et que l'identité est qualifiée, le sexe du patient est obligatoire.
 * Prescription level: Mandatory / Required / Shall
 * Page: 8
 * Section: 6.7.5
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:24 by aberge
 * Commentaire: Si PID-32 = VALI alors PID-8 est présent et non vide

 * @author pdt
 *
 */
public class INS006RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		if (hasMatriculeINS(pid) && isEmpty(pid.getAdministrativeSex())) {
			return new ValidationException[] {new INSValidationException()};
		}
		return null;
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
