package net.ihe.gazelle.hl7.validator.criterion;

import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hql.criterion.PropertyCriterion;

/**
 * <p>CriterionToTestResult class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class CriterionToTestResult extends PropertyCriterion<HL7v2ValidationLog, String> {

	/** Constant <code>SINGLETON</code> */
	public static final CriterionToTestResult SINGLETON = new CriterionToTestResult();

	/**
	 * <p>Constructor for CriterionToTestResult.</p>
	 */
	protected CriterionToTestResult() {
		super(String.class, "testResult", "this", "testResult");
	}

	/** {@inheritDoc} */
	@Override
	public String getSelectableLabelNotNull(String instance) {
		return instance;
	}
}
