package net.ihe.gazelle.hl7.validator.core;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.conf.ProfileException;
import ca.uhn.hl7v2.conf.parser.ProfileParser;
import ca.uhn.hl7v2.conf.spec.RuntimeProfile;
import ca.uhn.hl7v2.conf.store.CodeStore;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.exception.GazelleHL7Exception;
import net.ihe.gazelle.hl7.messageprofiles.api.ResourceApi;
import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.validation.context.ValidationContext;
import net.ihe.gazelle.hl7.validator.api.GenericHL7Validator;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.UsedResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class HapiValidator extends GenericHL7Validator {

    private static final Logger LOG = LoggerFactory.getLogger(HapiValidator.class);

    public HapiValidator(Profile profile, ValidationContext validationContext, String message) {
        super(profile, validationContext, message);
    }


    /**
     * Perform validation of current message and calculate report without persisting.
     */
    @Override
    public void validateMessage(HL7v2ValidationReport report) {
        ResourceStoreFactory resourceStoreFactory = new ResourceStoreFactory();
        List<UsedResource> resources = listUsedResources(getSelectedProfile().getResources());
        report.setResources(resources);
        // Do not validate the profile against the DTD !
        ProfileParser profileParser = new ProfileParser(false);
        RuntimeProfile runtimeProfile = parseRuntimeProfile(report, profileParser);
        if (runtimeProfile != null) {
            // declare tables
            registerHL7Tables(report, resourceStoreFactory, runtimeProfile);

            // parse the message
            Parser parser = PipeParser.getInstanceWithNoValidation();

            Message hapiMessage = parseMessage(report, parser);
            if (hapiMessage != null) {
                // Validation performed here !
                GazelleValidator validator = new GazelleValidator(resourceStoreFactory, report.getResults());
                validator.validate(hapiMessage, runtimeProfile.getMessage(), runtimeProfile.getHL7Version());

                // successful validation (no error/no warning)
                report.computeResult();
            }
        }
    }

    private Message parseMessage(HL7v2ValidationReport report, Parser parser) {
        Message hapiMessage = null;
        try {
            String javaPackage = getSelectedProfile().getJavaPackage();
            if (javaPackage != null) {
                hapiMessage = parser.parseForSpecificPackage(getMessageToValidate(), javaPackage);
            } else {
                hapiMessage = parser.parse(getMessageToValidate());
            }
        } catch (HL7Exception e) {
            report.getOverview().setValidationAbortedReason("The HL7 message is not parsable");
            report.getOverview().setValidationStatus(net.ihe.gazelle.hl7.validator.report.ValidationStatus.ABORTED);
            report.getResults().addNotification(e.getMessage(), GazelleErrorCode.FORMAT, null, null);
            ((GazelleHL7Exception) report.getResults().getLastNotification()).getHl7Exception().setLocation(
                    e.getLocation());
        }
        return hapiMessage;
    }

    private RuntimeProfile parseRuntimeProfile(HL7v2ValidationReport report, ProfileParser profileParser) {
        RuntimeProfile runtimeProfile = null;
        try {
            runtimeProfile = profileParser.parse(new String(getSelectedProfile().getContent(), StandardCharsets.UTF_8));
            runtimeProfile.getMessage().setIdentifier(getSelectedProfile().getOid());
        } catch (ProfileException e) {
            report.getOverview().setValidationStatus(net.ihe.gazelle.hl7.validator.report.ValidationStatus.ABORTED);
            report.getOverview().setValidationAbortedReason("The HL7 message profile is not parsable");
            LOG.error(e.getMessage(), e);
            return null;
        }
        return runtimeProfile;
    }

    private void registerHL7Tables(HL7v2ValidationReport report, ResourceStoreFactory resourceStoreFactory, RuntimeProfile runtimeProfile) {
        if ((getSelectedProfile().getResources() != null) && !getSelectedProfile().getResources().isEmpty()) {
            for (ResourceApi resource : getSelectedProfile().getResources()) {
                try {
                    CodeStore codeStore = new ResourceCodeStore(resource.getContent(), resource.getOid());
                    resourceStoreFactory.addCodeStore(codeStore, runtimeProfile.getMessage().getIdentifier(),
                            resource.getWeight());
                } catch (Exception e) {
                    report.getResults().addProfileException(
                            "Resource " + resource.getOid() + " : " + e.getMessage(), null, null);
                }
            }
        } else {
            report.getResults().addProfileException(
                    "No resources are available for profile: " + getSelectedProfile().getOid(), null, null);
        }
    }

    private List<UsedResource> listUsedResources(List<ResourceApi> resources) {
        List<UsedResource> list = new ArrayList<>();
        for (ResourceApi resource: resources){
            list.add(new UsedResource(resource.getOid(), resource.getRevision()));
        }
        return list;
    }




}
