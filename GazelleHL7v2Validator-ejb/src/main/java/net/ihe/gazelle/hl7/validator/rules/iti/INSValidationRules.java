package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract group of INS Rules bound by the same hl7path
 *
 * @author pdt
 *
 */
public abstract class INSValidationRules extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception{
		List<ValidationException> exceptions = new ArrayList<>();
		for (INSValidationRule rule: getRules()) {
			ValidationException[] ve = rule.validate(pid);
			if (ve!=null) {
				for (ValidationException e:ve) {
					exceptions.add(e);
				}
			}
		}
		return exceptions.isEmpty() ? null : exceptions.toArray(new ValidationException[]{});
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.PREDICATE_FAILURE.getCode();
	}

	protected abstract INSValidationRule[] getRules();

	public abstract String getHl7Path();


}
