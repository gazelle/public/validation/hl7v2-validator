package net.ihe.gazelle.hl7.admin;

import java.util.Date;

import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.preferences.PreferenceProvider;

import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;

/**
 * <p>GazelleHL7ValidatorPreferenceProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@MetaInfServices(PreferenceProvider.class)
public class GazelleHL7ValidatorPreferenceProvider implements PreferenceProvider {

	/** {@inheritDoc} */
	@Override
	public int compareTo(PreferenceProvider o) {
		return getWeight().compareTo(o.getWeight());
	}

	/** {@inheritDoc} */
	@Override
	public Boolean getBoolean(String key) {
		String prefAsString = getString(key);
		if ((prefAsString != null) && !prefAsString.isEmpty()) {
			return Boolean.valueOf(prefAsString);
		} else {
			return false;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Date getDate(String arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public Integer getInteger(String key) {
		String prefAsString = getString(key);
		if ((prefAsString != null) && !prefAsString.isEmpty()) {
			try {
				return Integer.decode(prefAsString);
			} catch (NumberFormatException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Object getObject(Object arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public String getString(String key) {
		return ApplicationConfiguration.getValueOfVariable(key);
	}

	/** {@inheritDoc} */
	@Override
	public Integer getWeight() {
		if (Contexts.isApplicationContextActive()) {
			return -100;
		} else {
			return 100;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setBoolean(String arg0, Boolean arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setDate(String arg0, Date arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setInteger(String arg0, Integer arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setObject(Object arg0, Object arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setString(String arg0, String arg1) {
	}

	/**
	 * <p>Constructor for GazelleHL7ValidatorPreferenceProvider.</p>
	 */
	public GazelleHL7ValidatorPreferenceProvider() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o){
		if (o == null){
			return false;
		}
		if (o instanceof PreferenceProvider){
			PreferenceProvider pp = (PreferenceProvider)o;
			return getWeight().equals(pp.getWeight());
		}else{
			return false;
		}
	}

	/** {@inheritDoc} */
	@Override public int hashCode() {
		final int prime = 78;
		int result = super.hashCode();
		result = prime * result + ((getWeight() == null) ? 0 : getWeight());
		return result;
	}
}
