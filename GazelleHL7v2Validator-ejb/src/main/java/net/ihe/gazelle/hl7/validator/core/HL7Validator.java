package net.ihe.gazelle.hl7.validator.core;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.GenericParser;
import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.validation.context.ValidationContext;
import net.ihe.gazelle.hl7.validator.api.GenericHL7Validator;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResults;
import net.ihe.gazelle.hl7.validator.report.ValidationStatus;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import org.jboss.seam.contexts.Contexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;


/**
 * <p>HL7Validator class.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */

public class HL7Validator {

    MetadataServiceProvider metadataServiceProvider;
    private static final Logger LOG = LoggerFactory.getLogger(HL7Validator.class);

    private String callerIP;
    private String xmlMessageMetadata;
    private String messageToValidate;
    private ValidationContext validationContext;
    private HL7v2ValidationReport report;

    public HL7Validator(String callerIP, String xmlMessageMetaData, String inMessage,
                        String xmlValidationContext, MetadataServiceProvider metadataServiceProvider) {
        this.callerIP = callerIP;
        this.xmlMessageMetadata = xmlMessageMetaData;
        setValidationContext(xmlValidationContext);
        this.messageToValidate = inMessage;
        this.metadataServiceProvider = metadataServiceProvider;
    }

    private void setValidationContext(String xmlValidationContext) {
        this.validationContext = ValidationContext.createFromString(xmlValidationContext);
    }

    private void prepareMessageForValidation() throws GazelleValidationException {
        // replace newline character to match HL7 standard
        String message = messageToValidate.replace('\n', '\r');
        // make sure we are using ER7 encoding
        String encoding = this.validationContext.getEncoding(message);
        if (encoding == null) {
            throw new GazelleValidationException("Unrecognize message encoding. Shall be ER7 or XML");
        } else if (ValidationContext.XML.equals(this.validationContext.getEncoding(message))) {
            convertXMLMessageToER7(message);
        } else {
            this.messageToValidate = message;
        }
    }

    private void convertXMLMessageToER7(String message) throws GazelleValidationException {
        Message messageObject;
        GenericParser parser = GenericParser.getInstanceWithNoValidation();
        parser.setXMLParserAsPrimary();
        try {
            messageObject = parser.parse(message);
            parser.setPipeParserAsPrimary();
            this.messageToValidate = parser.encode(messageObject);
        } catch (HL7Exception e) {
            throw new GazelleValidationException("Cannot convert XML message to ER7: " + e.getMessage());
        }
    }

    public String validate() {
        String xslUrl = null;
        if (Contexts.isApplicationContextActive()) {
            xslUrl = ApplicationConfiguration.getValueOfVariable("report_xsl_location");
        }

        initializeReporter();
        if (validationContext.getProfileOid() == null) {
            report.getOverview().setValidationStatus(ValidationStatus.INVALID_REQUEST);
            report.getOverview().setValidationAbortedReason("No profile OID provided");
        } else {
            // look for the conformance profile
            Profile hl7MessageProfile = Profile.getProfileByOID(validationContext.getProfileOid(), null);
            if (hl7MessageProfile == null) {
                report.getOverview().setValidationStatus(ValidationStatus.INVALID_REQUEST);
                report.getOverview().setValidationAbortedReason("The given profile OID does not match any HL7 message profile");
            } else {
                try {
                    prepareMessageForValidation();
                } catch (GazelleValidationException e) {
                    report.getOverview().setValidationStatus(ValidationStatus.ABORTED);
                    report.getOverview().setValidationAbortedReason(e.getMessage());
                    return report.toString(xslUrl);
                }

                GenericHL7Validator validator = null;

                switch (hl7MessageProfile.getConformanceProfileFormat()) {
                    case ConformanceProfile:
                        String gvtReport = validateWithGVT(hl7MessageProfile);
                        report.createFromXmlTest(gvtReport);
                        break;
                    default:
                        validator = new HapiValidator(hl7MessageProfile, validationContext, messageToValidate);
                        validator.validateMessage(report);
                        break;

                }
                report.getOverview().setProfileOid(hl7MessageProfile.getOid());
                report.getOverview().setProfileRevision(hl7MessageProfile.getRevision());
                String testResult = report.getOverview().getValidationTestResult();
                String profileOid = validationContext.getProfileOid();
                HL7v2ValidationLog statistic = new HL7v2ValidationLog(callerIP, testResult, profileOid);
                statistic.save();

            }
        }

        return report.toString(xslUrl);
    }

    private String validateWithGVT(Profile hl7MessageProfile) {
        // write the content of messageToValidate in a temp file
        File temp = writeTempFile(messageToValidate);
        if (temp == null) return null;

        //replace by call to the external JAR
        InputStream gvtReportStream = null;
        String outputString = null;
        try {
            gvtReportStream = runGVT(hl7MessageProfile, temp.getAbsolutePath());
            //read standard output to retrieve the report as String
            outputString = asString(gvtReportStream);
        } catch (Exception e) {
            LOG.error("Cannot read validation report: {}", e.getMessage());
        } finally {
            try {
                if (gvtReportStream != null) {
                    gvtReportStream.close();
                }
            } catch (IOException e) {
                LOG.error("Cannot close stream: {}", e.getMessage());
            }
        }
        // delete the temp file containing the message to validate
        temp.delete();
        return outputString;
    }

    private File writeTempFile(String messageToValidate1) {
        File temp = new File("Empty");
        BufferedWriter bw = null;

        try {
            temp = File.createTempFile("tempfile", ".txt");
        } catch (IOException e) {
            LOG.error("Cannot create temp file to store the message: {}", e.getMessage());
            return null;
        }

        try {
            bw = new BufferedWriter(new FileWriter(temp));
            bw.write(messageToValidate1);
        } catch (IOException e) {
            LOG.error("Cannot write message into file: {}", e.getMessage());
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return temp;
    }


    /**
     * Turn an inputStream into a string
     *
     * @param in : standard InputStream comed from the GVT validator
     * @return the report in string
     * @throws IOException
     */
    public String asString(InputStream in) throws IOException {
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(in, StandardCharsets.UTF_8)
        );
        String line;
        StringBuilder builder = new StringBuilder();
        while ((line = rd.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    /**
     * Call to the external GVT jar
     *
     * @param p            : the profil used for validation
     * @param tempFilePath : the message to validate
     * @return the standard output of the jar which is the validation report
     * @throws IOException
     */

    public InputStream runGVT(Profile p, String tempFilePath) throws IOException {
        String command = "/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar "
                + ApplicationConfiguration.getValueOfVariable("jar_path")
                + " -profileDirectory "
                + ApplicationConfiguration.getValueOfVariable("hl7_directory")
                + p.getPath() + "/ -message "
                + tempFilePath;


        Process pr = Runtime.getRuntime().exec(command);


        return pr.getInputStream();
    }


    private void initializeReporter() {
        report = new HL7v2ValidationReport();
        ValidationResults results = new ValidationResults(validationContext.getLength(), validationContext.getDataValue(),
                validationContext.getDatatype(), validationContext.getMessageStructure());
        report.setResults(results);
        report.getOverview().setMessageOid(generateNextOid());
        report.getOverview().setValidationServiceVersion(metadataServiceProvider.getMetadata().getVersion());

    }

    private String generateNextOid() {
        try {
            return OIDGenerator.generateNewOIDForMessage(EntityManagerService.provideEntityManager());
        } catch (Exception e) {
            return "testOID";
        }
    }

    public HL7v2ValidationReport getReport() {
        return this.report;
    }
}
