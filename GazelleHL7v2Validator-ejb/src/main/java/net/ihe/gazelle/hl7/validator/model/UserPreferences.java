package net.ihe.gazelle.hl7.validator.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.servlet.http.HttpServletRequest;

import net.ihe.gazelle.hql.HQLQueryBuilder;

import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Name;

@Entity
/**
 * <p>UserPreferences class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("userPreferences")
@Table(name = "user_preferences", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@SequenceGenerator(name = "user_preferences_sequence", sequenceName = "user_preferences_id_seq", allocationSize = 1)
public class UserPreferences implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "user_preferences_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@NotNull
	@Column(name = "username")
	private String username;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private List<IPAddress> allowedIPAddresses;

	/**
	 * <p>Constructor for UserPreferences.</p>
	 */
	public UserPreferences() {

	}

	/**
	 * <p>Constructor for UserPreferences.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 * @param allowedIps a {@link java.util.List} object.
	 */
	public UserPreferences(String username, List<IPAddress> allowedIps) {
		this.username = username;
		this.allowedIPAddresses = allowedIps;
	}

	/**
	 * <p>getPreferencesForUsername.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7.validator.model.UserPreferences} object.
	 */
	public static UserPreferences getPreferencesForUsername(String username) {
		HQLQueryBuilder<UserPreferences> preferences = new HQLQueryBuilder<UserPreferences>(UserPreferences.class);
		preferences.addEq("username", username);
		return preferences.getUniqueResult();
	}

	/**
	 * <p>getAllowedIpsForUser.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<String> getAllowedIpsForUser(String username) {
		UserPreferencesQuery query = new UserPreferencesQuery();
		query.username().eq(username);
		return query.allowedIPAddresses().ipAddress().getListDistinct();
	}

	/**
	 * <p>getCurrentlyUsedIp.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public static String getCurrentlyUsedIp() {
		FacesContext context = FacesContext.getCurrentInstance();
		return ((HttpServletRequest) context.getExternalContext().getRequest()).getRemoteAddr();
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>username</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * <p>Setter for the field <code>username</code>.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * <p>Getter for the field <code>allowedIPAddresses</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<IPAddress> getAllowedIPAddresses() {
		return allowedIPAddresses;
	}

	/**
	 * <p>Setter for the field <code>allowedIPAddresses</code>.</p>
	 *
	 * @param allowedIPAddresses a {@link java.util.List} object.
	 */
	public void setAllowedIPAddresses(List<IPAddress> allowedIPAddresses) {
		this.allowedIPAddresses = allowedIPAddresses;
	}
}
