package net.ihe.gazelle.hl7.messageprofiles.adapter;

import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.messageprofiles.model.ProfileQuery;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import net.ihe.gazelle.hl7.validator.core.OIDGenerator;
import net.ihe.gazelle.tf.ws.Hl7MessageProfile;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.EntityManager;

@Name("messageProfilesDAO")
public class MessageProfilesDAOImpl implements MessageProfilesDAO {

    private EntityManager entityManager;

    private EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = (EntityManager) Component.getInstance("entityManager");
        }
        return entityManager;
    }

    public Profile  queryProfileByOid(String oid) {

        ProfileQuery query = new ProfileQuery();
        query.oid().eq(oid);
        return  query.getUniqueResult();

    }

    @Override
    public String generateProfileOID() {
        return OIDGenerator.generateNewOIDForProfile(getEntityManager());
    }

    @Override
    public Profile saveProfile(Profile selectedProfile) {
        return getEntityManager().merge(selectedProfile);
    }

    @Override
    public void saveResource(Resource selectedResource) {
        getEntityManager().merge(selectedResource);

    }

    @Override
    public String generateResourceOID() {
        return OIDGenerator.generateNewOIDForResource(getEntityManager());
    }

    @Override
    public Profile getProfileByOid(Hl7MessageProfile profileReference) {
        return Profile.getProfileByOID(profileReference.getOid(),getEntityManager());
    }


}
