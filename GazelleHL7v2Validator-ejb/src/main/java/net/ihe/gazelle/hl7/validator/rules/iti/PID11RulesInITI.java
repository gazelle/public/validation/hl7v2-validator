package net.ihe.gazelle.hl7.validator.rules.iti;

/**
 * INS rules bound on PID-11 hl7path
 *
 * @author pdt
 *
 */
public class PID11RulesInITI extends INSValidationRules {

	@Override
	protected INSValidationRule[] getRules() {
		return new INSValidationRule[] {
				new INS010RuleInITI()
		};
	}

	@Override
	public String getHl7Path() { return "*/PID/PID-11"; }
}
