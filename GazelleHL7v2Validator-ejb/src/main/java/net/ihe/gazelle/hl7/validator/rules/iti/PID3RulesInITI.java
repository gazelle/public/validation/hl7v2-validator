package net.ihe.gazelle.hl7.validator.rules.iti;

/**
 *  INS rules bound on PID-3-4-2 hl7path
 *
 * @author pdt
 *
 */
public class PID3RulesInITI extends INSValidationRules {

	@Override
	protected INSValidationRule[] getRules() {
		return new INSValidationRule[] {
				new INS001RuleInITI(),
				new INS002RuleInITI(),
				new INS003RuleInITI(),
				new INS011RuleInITI(),
				new INS012RuleInITI(),
		};
	}

	@Override
	public String getHl7Path() { return "*/PID/PID-3/CX-4/HD-2"; }
}
