/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.hl7.validator.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Name("HL7v2ValidationLog")
@Table(name = "hl7_message")
@SequenceGenerator(name = "hl7_message_sequence", sequenceName = "hl7_message_id_seq", allocationSize = 1)

public class HL7v2ValidationLog implements Serializable {

    private static final long serialVersionUID = -315076050894161909L;
    public static final String ERROR = "ERROR";
    public static final String WARNING = "WARNING";
    public static final String IGNORE = "IGNORE";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hl7_message_sequence")
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "validation_date")
    private Date validationDate;

    /**
     * for use by admin only, indicates if the validation of this message has raised profile exceptions or not
     */

    @Column(name = "caller_ip")
    String callerIP; // Stores the IP Address of the webservice caller

    @Column(name = "test_result")
    String testResult; // Stores the result of the validation for easy retrieve
    // in the database.

    @Column(name = "profile_oid")
    String profileOid;


    /**
     * Constructors
     */
    public HL7v2ValidationLog() {

    }

    public HL7v2ValidationLog(String callerIp, String testResult, String profileOid )
    {
        this.validationDate = new Date();
        this.callerIP = callerIp ;
        this.testResult = testResult;
        this.profileOid = profileOid;
    }

    public static HL7v2ValidationLog getMessageByOid(String inOid) {
        HQLQueryBuilder<HL7v2ValidationLog> builder = new HQLQueryBuilder<HL7v2ValidationLog>(HL7v2ValidationLog.class);
        builder.addEq("oid", inOid);
        // not logged in user are only allowed to view the message validate using their current IP address
        String callerIP = UserPreferences.getCurrentlyUsedIp();
        if (!Identity.instance().isLoggedIn()) {
            builder.addEq("callerIP", callerIP);
        }
        // logged in user (not admin) are allowed to see validation results from a set of defined ip
        else if (!Identity.instance().hasRole("admin_role")) {
            List<String> allowedIps = UserPreferences.getAllowedIpsForUser(Identity.instance().getCredentials()
                    .getUsername());
            if ((allowedIps != null) && !allowedIps.isEmpty()) {
                builder.addIn("callerIP", allowedIps);
            } else {
                builder.addEq("callerIP", callerIP);
            }
        }
        return builder.getUniqueResult();
    }

    public void deleteMessage() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        HL7v2ValidationLog message = entityManager.find(HL7v2ValidationLog.class, this.id);
        entityManager.remove(message);
        entityManager.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Message has been permanently deleted");
    }

    public void save() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    /**
     * Getters and Setters
     */


    public String getCallerIP() {
        return callerIP;
    }

    public void setCallerIP(String callerIP) {
        this.callerIP = callerIP;
    }

    public Date getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Date lastChanged) {
        this.validationDate = lastChanged;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public String getProfileOid() {
        return profileOid;
    }

    public void setProfileOid(String profileOid) {
        this.profileOid = profileOid;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result ;//+ ((oid == null) ? 0 : oid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        return true;
    }
}
