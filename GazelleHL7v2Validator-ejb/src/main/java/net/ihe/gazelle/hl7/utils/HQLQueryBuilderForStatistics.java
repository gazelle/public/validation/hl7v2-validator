package net.ihe.gazelle.hl7.utils;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLOrder;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HQLQueryBuilderForStatistics<T> extends HQLQueryBuilder<T> {

	private static Logger log = LoggerFactory.getLogger(HQLQueryBuilderForStatistics.class);

	public HQLQueryBuilderForStatistics(Class<T> clazz) {
		super(clazz);
	}

	public Date getMinDate() {
		StringBuilder sb = new StringBuilder("select min(validationDate) ");
		// First build where, to get all paths in from
		StringBuilder sbWhere = new StringBuilder();
		HQLRestrictionValues values = buildQueryWhere(sbWhere);
		// Build order, to get all paths in from
		buildQueryOrder(sbWhere);

		// add order columns if needed, cannot sort without it!
		// will be removed at the end (result list will be Object[] then)
		for (HQLOrder order : getOrders()) {
			sb.append(", ").append(order.getPath());
		}
		sb.append(buildQueryFrom());
		sb.append(sbWhere);

		Query query = createRealQuery(sb);

		buildQueryWhereParameters(query, values);
		try {
			return (Date) query.getSingleResult();
		} catch (NoResultException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

}
