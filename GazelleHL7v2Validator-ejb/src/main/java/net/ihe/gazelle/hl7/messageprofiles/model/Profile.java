/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.model;

// JPA imports

import net.ihe.gazelle.hl7.messageprofiles.api.ProfileApi;
import net.ihe.gazelle.hl7.messageprofiles.api.ResourceApi;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>Profile<br>
 * <br>
 * This class describes the Profile object. This class belongs to the InriaHL7MessageProfileRepository module.
 * <p>
 * Profile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Profile</li>
 * <li><b>oid</b> : oid corresponding to the Profile - oid root is 1.3.6.1.4.12559.11.1.1.</li>
 * <li><b>content</b> : XML file containing the description of the profile</li>
 * </ul>
 * </br> *
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, November 30
 */

@Entity
@Name("hl7Profile")
@Table(name = "hl7_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "oid"))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "hl7_profile_sequence", sequenceName = "hl7_profile_id_seq", allocationSize = 1)
public class Profile extends CommonProperties implements java.io.Serializable, Comparable<Profile>, ProfileApi {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 9110924423557888115L;

    @Enumerated(EnumType.STRING)
    @Column(name = "conformance_profile_format")
    private ConformanceProfileFormat conformanceProfileFormat;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hl7_profile_sequence")
    private Integer id;

    @Column(name = "java_package")
    private String javaPackage;

    @ManyToMany(mappedBy = "profiles", targetEntity = Resource.class)
    private List<ResourceApi> resources;

    /*******************************************************************
     * Constructors
     * *****************************************************************/

    public Profile() {
        super();
    }

    public Profile(Integer inId) {
        this.id = inId;
    }

    public Profile(String inOid) {
        this.oid = inOid;
    }

    public Profile(String inOid, byte[] profileContent) {
        this.oid = inOid;
        if (profileContent != null) {
            this.content = profileContent.clone();
        }
    }

    public Profile(String inOid, String path) {
        this.oid = inOid;
        if (path != null) {
            this.path = path;
        }
    }

    public Profile(Integer inId, String inOid, String inContent) {
        this.id = inId;
        this.oid = inOid;
        setContent(inContent);
    }

    /**********************************************************
     * Getters and Setters
     **********************************************************/

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer inId) {
        this.id = inId;
    }

    public ConformanceProfileFormat getConformanceProfileFormat() {
        return this.conformanceProfileFormat;
    }

    public void setConformanceProfileFormat(ConformanceProfileFormat inFormat) {
        this.conformanceProfileFormat = inFormat;
    }

    public void setJavaPackage(String javaPackage) {
        this.javaPackage = javaPackage;
    }

    public String getJavaPackage() {
        return javaPackage;
    }

    /***********************************************************
     * Foreign Key for table : hl7_profile_resource
     ************************************************************/

    public List<ResourceApi> getResources() {
        if (resources == null) {
            resources = new ArrayList<ResourceApi>();
        }
        return resources;
    }

    public void setResources(List<ResourceApi> resources) {
        this.resources = resources;
    }

    /**
     * For a given OID, returns the description of the selected profile
     *
     * @param inOid
     * @param entityManager
     * @return the profile found in DB
     */
    public static Profile getProfileByOID(String inOid, EntityManager entityManager) {
        HQLQueryBuilder<Profile> builder;
        if (entityManager == null) {
            builder = new HQLQueryBuilder<Profile>(Profile.class);
        } else {
            builder = new HQLQueryBuilder<Profile>(entityManager, Profile.class);
        }
        builder.addEq("oid", inOid.trim());
        return builder.getUniqueResult();
    }

    /**
     * Checks the given OID is not already used by another existing profile
     *
     * @param inOID String
     * @return true if the OID is already used
     */
    public static boolean validateOID(String inOID) {
        HQLQueryBuilder<Profile> builder = new HQLQueryBuilder<Profile>(Profile.class);
        builder.addEq("oid", inOID);
        return (builder.getCount() > 0);
    }

    public Profile save() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        Profile profile = entityManager.merge(this);
        entityManager.flush();
        return profile;
    }

    @Override
    public int compareTo(Profile o) {
        return this.getOid().compareToIgnoreCase(o.getOid());
    }

    public String getFormat() {

        if (conformanceProfileFormat != null) {
            return conformanceProfileFormat.toString();

        } else {
            return "HL7v2xConformanceProfile";
        }

    }

    /**
     * boolean to indicate whether it is an Igamt profile or not
     */
    public boolean isIgamt() {
        switch (conformanceProfileFormat) {

            case HL7v2xConformanceProfile:
                return false;

            case ConformanceProfile:
                return true;

            default:
                return false;
        }
    }

    public String getDirectory() {
        switch (conformanceProfileFormat) {
            case HL7v2xConformanceProfile:
                return PreferenceService.getString("hl7_directory") + getPath() ;

            case ConformanceProfile:
                return PreferenceService.getString("hl7_directory") + getPath() + "/Profile.xml";

            default:
                return PreferenceService.getString("hl7_directory") + getPath();

        }

    }


}
