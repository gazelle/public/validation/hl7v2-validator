/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.application;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.hl7.messageprofiles.adapter.HL7Service;
import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAO;
import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import net.ihe.gazelle.hl7.messageprofiles.model.ResourceQuery;
import net.ihe.gazelle.hl7.utils.Downloader;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tf.messageprofile.SOAPExceptionException;
import net.ihe.gazelle.tf.ws.*;
import org.apache.axis2.AxisFault;
import org.jboss.seam.annotations.In;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.util.*;

import static net.ihe.gazelle.hl7.messageprofiles.model.ConformanceProfileFormat.ConformanceProfile;

/**
 * <b>Class Description : </b>HL7MessageProfileCreationManager<br>
 * <br>
 * This class implements the local interface HL7MessageProfileCreationManager dedicated to the creation of new HL7 Message Profile, stored in the
 * database of this application and referenced in Gazelle
 * Master Model
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, December 21st
 */
public class HL7Application implements Serializable {

    private static Logger log = LoggerFactory.getLogger(HL7Application.class);

    /**
     * Serial Version UUID requested by the implementation of Serializable
     */
    private static final long serialVersionUID = 4249465584086119918L;

    private HL7Service hl7Service;

  //  @In(create = true, value = "messageProfilesDAO")
    private MessageProfilesDAO messageProfilesDAO;

    /**
     * String used to store the content of the uploaded file until we are sure it is not empty, well-formed and valid
     */
    private String fileContent;

    /**
     * OID given by the user in the h:inputText component
     */
    private String selectedOid;

    /**
     * net.ihe.gazelle.tf.ws.Hl7MessageProfile is the object used in TF to reference the profile. Contains the link with domain/actor/transaction
     * and other useful informations
     */
    private Hl7MessageProfile selectedMessageProfileReference;

    /**
     * net.ihe.gazelle.repository.hl7.inria.model.Profile is the object used in HL7MPR to store the profile content along with its OID
     */
    private Profile selectedProfile;

    /**
     * List of all message profile references contained in TF
     */
    private List<Hl7MessageProfile> messageProfilesReferences;

    /**
     * List of the format of every message profile
     */
    private List<String> messageProfilesFormat;

    /**
     * List of the path of every message profile
     */
    private List<String> messageProfilesPath;

    private Resource selectedResource;

    /**
     * boolean to indicate whether we are in edit mode (reserved to admin) or not
     */
    private boolean editMode = false;

    private transient Filter<Resource> filter;

    /**
     * related to ProfileResourceLink manager
     */
    private GetListOfPossibleHL7MessageProfileForGivenContext wsParams;
    private List<Resource> availableResources;
    private List<Resource> selectedResources;
    private List<Hl7MessageProfile> selectedProfiles;
    private String profileOid;

    /**
     ************ Getter and Setters **********
     */
    /**
     * Getters and Setters
     *
     * @return a {@link String} object.
     */
    public String getSelectedOid() {
        return selectedOid;
    }

    /**
     * <p>Setter for the field <code>selectedOid</code>.</p>
     *
     * @param selectedOid a {@link String} object.
     */
    public void setSelectedOid(String selectedOid) {
        this.selectedOid = selectedOid;
    }

    public Resource getSelectedResource() {
        return selectedResource;
    }

    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
    }

    /**
     * <p>Getter for the field <code>selectedMessageProfileReference</code>.</p>
     *
     * @return a {@link Hl7MessageProfile} object.
     */
    public Hl7MessageProfile getSelectedMessageProfileReference() {
        return selectedMessageProfileReference;
    }

    /**
     * <p>Setter for the field <code>selectedMessageProfileReference</code>.</p>
     *
     * @param selectedMessageProfile a {@link Hl7MessageProfile} object.
     */
    public void setSelectedMessageProfileReference(Hl7MessageProfile selectedMessageProfile) {
        this.selectedMessageProfileReference = selectedMessageProfile;
    }

    /**
     * <p>Getter for the field <code>selectedProfile</code>.</p>
     *
     * @return a {@link Profile} object.
     */
    public Profile getSelectedProfile() {
        return selectedProfile;
    }

    /**
     * <p>Setter for the field <code>selectedProfile</code>.</p>
     *
     * @param selectedProfile a {@link Profile} object.
     */
    public void setSelectedProfile(Profile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    /**
     * <p>Getter for the field <code>messageProfilesReferences</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<Hl7MessageProfile> getMessageProfilesReferences() {
        return messageProfilesReferences;
    }

    /**
     * <p>getAllMessageProfilesReferences.</p>
     *
     * @return a {@link GazelleListDataModel} object.
     */
    public GazelleListDataModel<Hl7MessageProfile> getAllMessageProfilesReferences() {
        return new GazelleListDataModel<Hl7MessageProfile>(messageProfilesReferences);
    }

    /**
     * <p>Setter for the field <code>messageProfilesReferences</code>.</p>
     *
     * @param profiles a {@link List} object.
     */
    public void setMessageProfilesReferences(List<Hl7MessageProfile> profiles) {
        this.messageProfilesReferences = profiles;
    }

    /**
     * Used in the profile list view to complete the format column
     *
     * @param p
     * @return the format of the HL7MessageProfile : HL7v2xConformanceProfile, ConformanceProfile
     */
    public String getFormat(Hl7MessageProfile p) {
        int index = messageProfilesReferences.indexOf(p);
        return messageProfilesFormat.get(index);
    }

    /**
     * used in the profile list to view profile content by clicking on the oid
     *
     * @param p
     * @return the path of the profile
     */
    public String getPath(Hl7MessageProfile p) {
        int index = messageProfilesReferences.indexOf(p);
        return messageProfilesPath.get(index);
    }

    /**
     * getter and setter de messageProfilesFormat
     */

    public List<String> getMessageProfilesFormat() {
        return messageProfilesFormat;
    }

    public void setMessageProfilesFormat(List<String> messageProfilesFormat) {
        this.messageProfilesFormat = messageProfilesFormat;
    }

    /**
     * <p>isEditMode.</p>
     *
     * @return a boolean.
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * <p>Setter for the field <code>editMode</code>.</p>
     *
     * @param editMode a boolean.
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }


    /**
     * <p>Setter for the field <code>fileContent</code>.</p>
     *
     * @param fileContent a {@link String} object.
     */
    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    /**
     * <p>Getter for the field <code>fileContent</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getFileContent() {
        return fileContent;
    }

    /**
     * Constructor
     *
     * @throws ControllerException
     */
    public HL7Application() throws ControllerException {
        this.hl7Service = new HL7Service();
    }

    /**
     * initialize lists of information : format and path
     */
    public void initListInfo() {
        messageProfilesFormat = new ArrayList<>();
        messageProfilesPath = new ArrayList<>();

        if (messageProfilesReferences != null && !messageProfilesReferences.isEmpty()) {
            for (Hl7MessageProfile hl7MessageProfile : messageProfilesReferences) {
                Profile p = messageProfilesDAO.queryProfileByOid(hl7MessageProfile.getOid());
                if (p != null) {
                    messageProfilesFormat.add(p.getFormat());
                    messageProfilesPath.add(p.getPath());
                } else {
                    messageProfilesFormat.add("HL7v2xConformanceProfile");
                    messageProfilesPath.add("");
                }
            }
        }
    }

    public void initializeResource() {
        selectedResource = new Resource();
        selectedResource.setLastChanged(new Date());
        selectedResource.setImportedWithErrors(false);
        selectedResource.setLastModifier(Identity.instance().getCredentials().getUsername());
    }

    /**
     * Application function
     */

    public Domain getDomain(String selectedDomainKeyword) throws ControllerException {
        return this.hl7Service.getDomain(selectedDomainKeyword);
    }

    /**
     * Retrieves the list of actors for the selected domain (stored in Hl7MessageProfile object)
     */
    public Actor[] getListOfActorsForDomain(String selectedDomainKeyword, Hl7MessageProfile selectedReference) throws ControllerException {
        Actor[] actors = null;
        Domain domain = this.hl7Service.getDomain(selectedDomainKeyword);
        if (domain != null) {
            selectedReference.setDomain(domain);
            actors = this.hl7Service.getListOfActors(selectedDomainKeyword);
        }
        return actors;

    }


    /**
     * Retrieves the list of all domains available in GMM by calling the web service IHEConcepts
     */
    public Domain[] getListOfDomains() throws ControllerException {
        return this.hl7Service.getListofDomains();

    }

    /**
     * Retrieves the list of Transaction for Actor
     *
     * @param selectedActorKeyword
     * @param selectedReference
     * @return
     * @throws ControllerException
     */
    public Transaction[] getListOfTransactionsForActor(String selectedActorKeyword, Hl7MessageProfile selectedReference) throws ControllerException {
        Transaction[] transactions = null;
        Actor actor = this.hl7Service.getActor(selectedActorKeyword);
        if (actor != null) {
            selectedReference.setActor(actor);
            transactions = this.hl7Service.getListOfTransactionsForActor(selectedActorKeyword);
        }
        return transactions;
    }

    /**
     * Get transaction
     * @param selectedTransactionKeyword
     * @param selectedReference
     * @throws ControllerException
     */
    public void getTransaction(String selectedTransactionKeyword, Hl7MessageProfile selectedReference) throws ControllerException {
        Transaction transaction = this.hl7Service.getTransaction(selectedTransactionKeyword);
        if (transaction != null) {
            selectedReference.setTransaction(transaction);
        }
    }

    /**
     * Is profile valid ?
     * @param selectedReference
     * @return
     */
    public boolean isProfileInValid(Hl7MessageProfile selectedReference) {
        return (((selectedReference.getTriggerEvent() == null) || selectedReference.getTriggerEvent().isEmpty())
                || ((selectedReference.getHl7Version() == null) || selectedReference.getHl7Version().isEmpty()));
    }


    /**
     * Instanciates new object and gives appropriate values to the attributes
     */
    public void initializeProfile() {
        selectedProfile = new Profile();
        selectedProfile.setLastChanged(new Date());
        selectedProfile.setLastModifier(Identity.instance().getCredentials().getUsername());
    }

    /**
     * Check if Oid is valid
     * @param selectedReference
     * @return
     */
    private boolean isOIDValid(Hl7MessageProfile selectedReference) {
        boolean isOIDEmpty = ((selectedReference.getOid() == null) || selectedReference.getOid().isEmpty());
        if (isOIDEmpty) {
            this.getAnewOID(selectedReference);
        }
        boolean isOIDAlreadyUsed = Profile.validateOID(selectedReference.getOid());

        if (!isOIDAlreadyUsed) {
            selectedProfile.setOid(selectedReference.getOid());
        }
        return !isOIDAlreadyUsed;

    }

    /**
     * if the reference is properly filled and submit the profile: 1. gets the related resources 2. stores the Profile object in database 3. sends
     * the new reference (Hl7MessageProfile object) to GMM
     */
    public void submitNewProfile(Hl7MessageProfile selectedReference) throws ControllerException {
        if (isProfileInValid(selectedReference)) {
            throw new ControllerException("Invalid profile", "gazelle.hl7mpr.triggerEventOrVersionMissing");
        }
        if (!isOIDValid(selectedReference)) {
            throw new ControllerException("OID already used", "gazelle.hl7mpr.oidAlreadyUsed");
        }
        if (!selectedProfile.getConformanceProfileFormat().equals(ConformanceProfile)) {
            List<Resource> resources = Resource.getResourceForHl7Version(selectedReference.getHl7Version());
            if (resources != null) {
                selectedProfile.getResources().addAll(resources);
            } else {
                throw new ControllerException("No resource for version " + selectedReference.getHl7Version(), "gazelle.hl7mpr" +
                        ".noResourceForThisVersion");
            }
        }
        Profile savedProfile = this.messageProfilesDAO.saveProfile(selectedProfile);
        if (savedProfile == null || savedProfile.getId() == null) {
            throw new ControllerException("Can not Store Profile In Data Base", "gazelle.hl7mpr.cannotStoreProfileInDatabase");
        }

        boolean isSubmitSuccessfull = this.hl7Service.addNewReference(selectedReference);
        if (!isSubmitSuccessfull) {
            throw new ControllerException("Can not add new Reference", "gazelle.hl7mpr.submitFailed");
        }
    }


    /**
     * Method called when the user upload a file using the rich:fileUpload component if the XML file is not empty, well-formed and valid, we can
     * add it to the database
     *
     * @param event a {@link FileUploadEvent} object.
     */
    public boolean uploadEventListener(FileUploadEvent event) throws ControllerException {

        UploadedFile item = event.getUploadedFile();
        if (item != null) {

            fileContent = new String(item.getData(), Charset.forName("UTF-8"));
            if ((fileContent == null) || fileContent.isEmpty()) {
                throw new ControllerException("The file is empty", "gazelle.hl7mpr.fileIsEmpty");
            }

            if (isFileWellFormed()) {
                if (isFileValid()) {
                    selectedProfile.setContent(fileContent);

                } else {
                    throw new ControllerException("The file is not valid", "gazelle.hl7mpr.fileIsNotValid");
                }
            } else {
                throw new ControllerException("The file is not well formed", "gazelle.hl7mpr.fileIsNotWellFormed");
            }
            return true;
        }
        return false;
    }

    private boolean isFileWellFormed() {
        // TODO
        return true;
    }

    /**
     * Checks whether the uploaded file respects the XSD file (HL7RegistrationSchema.xsd) or not
     *
     * @return true if the file is valid, false otherwise
     */
    private boolean isFileValid() {
        // TODO
        return true;
    }

    /**
     * Generate a relative path when we register new profiles
     */
    public void generatePath(Hl7MessageProfile selectedReference) {

        switch (selectedProfile.getConformanceProfileFormat()) {

            case HL7v2xConformanceProfile:
                selectedProfile.setPath(PreferenceService.getString("hl7_profiles_directory")
                        + "/" + selectedReference.getOid() + ".xml");
                break;

            case ConformanceProfile:
                selectedProfile.setPath(PreferenceService.getString("IGAMT_directory")
                        + "/" + selectedReference.getOid());
                break;

            default:
                selectedProfile.setPath(PreferenceService.getString("hl7_profiles_directory")
                        + "/" + selectedReference.getOid() + ".xml");
                break;
        }

    }

    /**
     * <p>getANewOID.</p>
     */
    public void getAnewOID(Hl7MessageProfile selectedReference) {
        selectedReference.setOid(messageProfilesDAO.generateProfileOID());
    }

    /**
     * Setter for Message Profile DAO
     *
     * @param messageProfilesDAO
     */
    public void setMessageProfilesDAO(MessageProfilesDAO messageProfilesDAO) {
        this.messageProfilesDAO = messageProfilesDAO;
    }


    /**
     * @throws RemoteException
     * @throws AxisFault
     * @throws SOAPExceptionException
     */
    public void getReferenceForSelectedProfile() throws ControllerException {
        this.setSelectedMessageProfileReference(this.hl7Service.getReferenceForSelectedProfile(selectedOid));
    }

    public List<Hl7MessageProfile> getAllMessageProfiles() throws ControllerException {

        Hl7MessageProfile[] hl7MessageProfiles = this.hl7Service.getAllMessageProfiles();
        if ((hl7MessageProfiles == null) || (hl7MessageProfiles.length == 0)) {
            throw new ControllerException("No profile Found", "gazelle.hl7mpr.noProfileFound");
        }
        return Arrays.asList(hl7MessageProfiles);

    }

    /**
     * Call service to save profile
     */
    public void saveProfile() {
        selectedProfile.setLastChanged(new Date());
        selectedProfile.setLastModifier(Identity.instance().getCredentials().getUsername());
        this.messageProfilesDAO.saveProfile(selectedProfile);
        setEditMode(false);
    }

    /**
     * <p>downloadSelectedProfile.</p>
     *
     * @param inProfile a {@link Hl7MessageProfile} object.
     */
    public void downloadSelectedProfile(Hl7MessageProfile inProfile) throws ControllerException {
        if ((inProfile == null) || (inProfile.getOid() == null)) {
            throw new ControllerException("Invalid Profil", "gazelle.hl7mpr.InvalidProfile");
        }
        selectedMessageProfileReference = inProfile;
        selectedProfile = Profile.getProfileByOID(selectedMessageProfileReference.getOid(), null);
        downloadSelectedProfile();

    }

    /**
     * <p>downloadSelectedProfile.</p>
     */
    public void downloadSelectedProfile() throws ControllerException {
        if ((selectedProfile == null) || (selectedProfile.getContent() == null)) {
            throw new ControllerException("Invalid Profil", "gazelle.hl7mpr.InvalidProfile");
        }
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("text/xml");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + selectedProfile.getOid().concat(".xml"));

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(selectedProfile.getContent());
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * <p>selectProfileByOid.</p>
     */
    public void selectProfileByOid() throws ControllerException {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedOid = params.get("oid");
        selectedProfile = Profile.getProfileByOID(selectedOid, null);
        if (selectedProfile == null) {
            throw new ControllerException("Invalid Profile", "gazelle.hl7mpr.InvalidProfile");
        }
        getReferenceForSelectedProfile();

        if (Identity.instance().hasRole("admin_role") && params.containsKey("editMode")) {
            setEditMode(Boolean.parseBoolean(params.get("editMode")));
        } else {
            setEditMode(false);
        }

    }


    /**
     * <p>displayProfileContent.</p>
     */
    public void displayProfileContent() {

        log.info("displayProfileContent()");
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedOid = params.get("oid");
        selectedProfile = Profile.getProfileByOID(selectedOid, null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        ServletOutputStream servletOutputStream;

        try {
            servletOutputStream = response.getOutputStream();
            if (selectedProfile.isIgamt()) {
                response.setContentType("text/html");
                String baseDirectory = PreferenceService.getString("hl7_directory");
                String linkToDisplayContent = baseDirectory.concat(selectedProfile.getPath().concat("/Display.html"));
                File displayHtml = new File(linkToDisplayContent);
                if (displayHtml.exists()) {
                    byte[] content = Files.readAllBytes(displayHtml.toPath());
                    servletOutputStream.write(content);
                } else {
                    log.error("Cannot access file at " + linkToDisplayContent);
                }
            } else {
                response.setContentType("text/xml");
                servletOutputStream.write(Downloader.updateStylesheetUrl(selectedProfile.getContent(), "profile_xsl_url"));
            }
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (IOException e) {
            log.error("Cannot write file in servlet !", e);
        }

        context.responseComplete();
    }



    /**
     * generate oid for selected resource
     */
    public void generateResourceOid() {

        selectedResource.setOid(this.messageProfilesDAO.generateResourceOID());
    }

    /**
     * generate path for resource
     */
    public void generateResourcePath() {

        selectedResource.setPath(PreferenceService.getString("hl7_table_directory")
                + "/" + selectedResource.getOid() + ".xml");
    }

    /**
     * Save resource
     */
    public void saveNewResource() {
        this.messageProfilesDAO.saveResource(selectedResource);
    }


    /**
     * select Resource by oid
     *
     * @throws ControllerException
     */
    public void selectResourceByOid() throws ControllerException {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String selectedOid = params.get("oid");
        selectedResource = Resource.getResourceByOID(selectedOid);
        if (selectedResource == null) {
            throw new ControllerException("This oid does not point to a known resource", "");
        } else if (Identity.instance().hasRole("admin_role") && params.containsKey("editMode")) {
            editMode = Boolean.parseBoolean(params.get("editMode"));
        } else {
            editMode = false;
        }
    }

    /**
     * save resource
     */
    public void saveResource() {
        selectedResource.setLastChanged(new Date());
        selectedResource.setLastModifier(Identity.instance().getCredentials().getUsername());
        this.messageProfilesDAO.saveResource(selectedResource);
        setEditMode(false);
    }

    /**
     * <p>downloadSelectedResource.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public void downloadSelectedResource(Resource inResource) throws ControllerException {
        if ((inResource == null) || (inResource.getContent() == null)) {
            throw new ControllerException("Resource invalid", "gazelle.hl7mpr.InvalidResource");
        }
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment; filename=" + inResource.getOid().concat(".xml"));

        ServletOutputStream servletOutputStream;
        try {
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(inResource.getContent());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (IOException e) {
            log.error("Unable to export Resource", e);
        }
        context.responseComplete();
    }

    /**
     * <p>displayResourceContent.</p>
     */
    public void displayResourceContent() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String selectedOid = params.get("oid");
        Resource resource = Resource.getResourceByOID(selectedOid);
        if (resource == null) {
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("text/xml");
        ServletOutputStream servletOutputStream;
        try {
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(Downloader.updateStylesheetUrl(resource.getContent(), "resource_xsl_url"));
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (IOException e) {
            log.error("Cannot write file in servlet !", e);
        }

        context.responseComplete();
    }

    /**
     * Create Filters
     * @return
     */
    private HQLCriterionsForFilter<Resource> getHQLCriteriaForFilter() {
        ResourceQuery query = new ResourceQuery();
        HQLCriterionsForFilter<Resource> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("oid", query.oid());
        criteria.addPath("hl7Version", query.hl7Version());
        criteria.addPath("comment", query.comment());
        criteria.addPath("path", query.path());
        criteria.addPath("revision", query.revision());
        return criteria;
    }


    /**
     * <p>getAvailableResources.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Resource> getAvailableResources() {
        return new FilterDataModel<Resource>(getFilter()) {
            @Override
            protected Object getId(Resource hl7Message) {
                return hl7Message.getId();
            }
        };
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Resource> getFilter() {
        if (filter == null) {
            filter = new Filter<Resource>(getHQLCriteriaForFilter());
        }
        return filter;
    }


    /**
     * <p>initializePage.</p>
     */
    public void initializePage() {
        HQLQueryBuilder<Resource> builder = new HQLQueryBuilder<Resource>(Resource.class);
        availableResources = builder.getList();
        wsParams = new GetListOfPossibleHL7MessageProfileForGivenContext();
        selectedResources = new ArrayList<Resource>();
        initListInfo();
    }

    /**
     * <p>selectResource.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public void selectResource(Resource inResource) {
        availableResources.remove(inResource);
        selectedResources.add(inResource);
    }

    /**
     * <p>createLink.</p>
     *
     * @param instance
     */
    public void createLink(FacesMessages instance) {

        List<Profile> profiles = new ArrayList<Profile>();
        for (Hl7MessageProfile profileReference : selectedProfiles) {
            Profile profile = messageProfilesDAO.getProfileByOid(profileReference);
            if (profile != null) {
                profiles.add(profile);
            } else {
                instance.add(StatusMessage.Severity.WARN,
                        "Reference to profile with OID " + profileReference.getOid() + " seems to be broken");
            }
        }
        for (Resource resource : selectedResources) {
            resource = Resource.getResourceByOID(resource.getOid());
            if ((resource.getProfiles() == null) || resource.getProfiles().isEmpty()) {
                resource.setProfiles(new ArrayList<Profile>(profiles));
            } else {
                for (Profile currentProfile : profiles) {
                    if (!resource.getProfiles().contains(currentProfile)) {
                        resource.getProfiles().add(currentProfile);
                    } else {
                        instance.add(StatusMessage.Severity.INFO,
                                "profile " + currentProfile.getOid() + " was already linked to "
                                        + resource.getOid());
                    }
                }
            }
        }
        instance.add(StatusMessage.Severity.INFO, "Links have been created");
        resetFilters();


    }

    /**
     * <p>resetFilters.</p>
     */
    public void resetFilters() {
        profileOid = null;
        wsParams = new GetListOfPossibleHL7MessageProfileForGivenContext();
        selectedProfiles = null;
    }

    /**
     * <p>Getter for the field <code>profileOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProfileOid() {
        return profileOid;
    }

    /**
     * <p>Setter for the field <code>profileOid</code>.</p>
     *
     * @param profileOid a {@link java.lang.String} object.
     */
    public void setProfileOid(String profileOid) {
        this.profileOid = profileOid;
    }

    /**
     * <p>Getter for the field <code>wsParams</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
     */
    public GetListOfPossibleHL7MessageProfileForGivenContext getWsParams() {
        return wsParams;
    }

    /**
     * <p>Setter for the field <code>wsParams</code>.</p>
     *
     * @param wsParams a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
     */
    public void setWsParams(GetListOfPossibleHL7MessageProfileForGivenContext wsParams) {
        this.wsParams = wsParams;
    }


    /**
     * <p>Setter for the field <code>availableResources</code>.</p>
     *
     * @param availableResources a {@link java.util.List} object.
     */
    public void setAvailableResources(List<Resource> availableResources) {
        this.availableResources = availableResources;
    }

    /**
     * <p>Getter for the field <code>selectedResources</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Resource> getSelectedResources() {
        return selectedResources;
    }

    /**
     * <p>Setter for the field <code>selectedResources</code>.</p>
     *
     * @param selectedResources a {@link java.util.List} object.
     */
    public void setSelectedResources(List<Resource> selectedResources) {
        this.selectedResources = selectedResources;
    }

    /**
     * <p>Getter for the field <code>selectedProfiles</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Hl7MessageProfile> getSelectedProfiles() {
        return selectedProfiles;
    }

    /**
     * <p>Setter for the field <code>selectedProfiles</code>.</p>
     *
     * @param selectedProfiles a {@link java.util.List} object.
     */
    public void setSelectedProfiles(List<Hl7MessageProfile> selectedProfiles) {
        this.selectedProfiles = selectedProfiles;
    }

    /**
     * <p>Getter for the field <code>availableResources</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Resource> getAvailableHL7Resources() {
        return availableResources;
    }

    /**
     * <p>getProfileReferencesFiltered.</p>
     */
    public void getProfileReferencesFiltered() throws ControllerException {
        if ((profileOid != null) && !profileOid.isEmpty()) {
            selectedProfiles = new ArrayList<>();
            selectedProfiles.add(this.hl7Service.getProfileByOid(profileOid));
        } else {
            selectedProfiles = this.hl7Service.getProfileFiltered(wsParams);
        }
    }
}








