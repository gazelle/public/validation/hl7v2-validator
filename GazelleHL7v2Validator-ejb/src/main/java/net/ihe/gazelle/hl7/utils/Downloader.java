package net.ihe.gazelle.hl7.utils;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.hl7.admin.GazelleHL7ValidatorPreferenceProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Downloader {

	private static Logger log = LoggerFactory.getLogger(Downloader.class);

	public static void exportXmlFileInline(byte[] fileContent, FacesContext context) {
		export(fileContent, null, null, context);
	}

	public static void exportXmlFileAsAttachment(byte[] fileContent, String fileName, FacesContext context) {
		export(fileContent, "attachment", fileName, context);
	}

	protected static void export(byte[] fileContent, String exportMode, String fileName, FacesContext context) {
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.setContentType("text/xml");
		if (exportMode != null) {
			response.setHeader("Content-Disposition", exportMode + "; filename=" + fileName);
		}
		ServletOutputStream servletOutputStream;
		try {
			servletOutputStream = response.getOutputStream();
			servletOutputStream.write(fileContent);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			log.error("Unable to export Resource", e);
		}
	}

	public static byte[] updateStylesheetUrl(byte[] fileContent, String preferenceKey) {
		GazelleHL7ValidatorPreferenceProvider preferenceProvider = new GazelleHL7ValidatorPreferenceProvider();
		if (preferenceProvider.getBoolean("force_stylesheet")) {
			String contentAsString = new String(fileContent, Charset.forName("UTF-8"));
			contentAsString = contentAsString.replaceFirst("href=\"[^\"]*\"",
					"href=\"" + preferenceProvider.getString(preferenceKey) + "\"");
			return contentAsString.getBytes(Charset.forName("UTF-8"));
		} else {
			return fileContent;
		}
	}

}
