package net.ihe.gazelle.hl7v3.validator.core;

import net.ihe.gazelle.ehdsihl7v3.validator.ehdsiiti55request.EHDSIITI55REQUESTPackValidator;
import net.ihe.gazelle.ehdsihl7v3.validator.ehdsiiti55response.EHDSIITI55RESPONSEPackValidator;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hl7v3.basic.datatypesval.DATATYPESVALPackValidator;
import net.ihe.gazelle.hl7v3.ksa.validator.kpdqquery.KPDQQUERYPackValidator;
import net.ihe.gazelle.hl7v3.ksa.validator.kpdqqueryresponse.KPDQQUERYRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.hl7v3.validator.chpdqquery.CHPDQQUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.chpdqresponse.CHPDQRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.chpixfeedadd.CHPIXFEEDADDPackValidator;
import net.ihe.gazelle.hl7v3.validator.chpixfeedrevise.CHPIXFEEDREVISEPackValidator;
import net.ihe.gazelle.hl7v3.validator.chpixquery.CHPIXQUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.chpixqueryresponse.CHPIXQUERYRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.chxcpdquery.CHXCPDQUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.chxcpdresponse.CHXCPDRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.acceptacknowledgement.ACCEPTACKNOWLEDGEMENTPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti44common.ITI44COMMONPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti44patientadd.ITI44PATIENTADDPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti44patientmerge.ITI44PATIENTMERGEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti44patientrevise.ITI44PATIENTREVISEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti45query.ITI45QUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti45response.ITI45RESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.iti46updatenotification.ITI46UPDATENOTIFICATIONPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.mccimt000100uv01.MCCIMT000100UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.mccimt000200uv01.MCCIMT000200UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.mccimt000300uv01.MCCIMT000300UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.mfmimt700701uv01.MFMIMT700701UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.mfmimt700711uv01.MFMIMT700711UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.pdqv3continuation.PDQV3CONTINUATIONPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.pdqv3query.PDQV3QUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.pdqv3response.PDQV3RESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.quqimt000001uv01.QUQIMT000001UV01PackValidator;
import net.ihe.gazelle.hl7v3.validator.core.xcpddeferredresponse.XCPDDEFERREDRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.xcpdpatientdiscoveryquery.XCPDPATIENTDISCOVERYQUERYPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.xcpdpatientdiscoveryqueryparameterlist.XCPDPATIENTDISCOVERYQUERYPARAMETERLISTPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.xcpdpatientdiscoveryqueryrequest.XCPDPATIENTDISCOVERYQUERYREQUESTPackValidator;
import net.ihe.gazelle.hl7v3.validator.core.xcpdpatientdiscoveryqueryresponse.XCPDPATIENTDISCOVERYQUERYRESPONSEPackValidator;
import net.ihe.gazelle.hl7v3.validator.util.ValidatorException;
import net.ihe.gazelle.hl7v3.validator.util.XMLValidation;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.interlay.MetadataServiceProviderImpl;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import org.apache.commons.lang3.StringUtils;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GazelleHL7v3Validator {

    private static Logger log = LoggerFactory.getLogger(GazelleHL7v3Validator.class);

    static MetadataServiceProvider metadataServiceProvider = (MetadataServiceProvider) Component.getInstance("metadataServiceProvider");
    public SAXParserFactory saxParserFactory;
    public String xsdLocation;

    static {
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return PreferenceService.getString("svs_repository_url");
            }
        });
    }

    public GazelleHL7v3Validator(SAXParserFactory factory, String xsdLocation) {
        this.saxParserFactory = factory;
        this.xsdLocation = xsdLocation;
    }

    public <T> DetailedResult validate(String messageToValidate, HL7V3ValidatorType type) {
        DetailedResult result = new DetailedResult();
        // initialize list of modules with default validators
        List<ConstraintValidatorModule> validatorModules = new ArrayList<ConstraintValidatorModule>();
        validatorModules.add(new DATATYPESVALPackValidator());
        // validate against schema
        validateAgainstSchema(result, messageToValidate, type);
        // validate by module
        List<Notification> notifications = validateByModules(type.getTypeClass(), messageToValidate, validatorModules,
                type);
        if (result.getMDAValidation() == null) {
            result.setMDAValidation(new MDAValidation());
        }
        result.getMDAValidation().setResult("PASSED");
        for (Notification notification : notifications) {
            result.getMDAValidation().getWarningOrErrorOrNote().add(notification);
            if (notification instanceof Error) {
                result.getMDAValidation().setResult("FAILED");
            }
        }
        addResultHeader(result, type);
        return result;
    }

    protected <T> List<Notification> validateByModules(Class<T> clazz, String messageToValidate,
                                                       List<ConstraintValidatorModule> modules, HL7V3ValidatorType validatorType) {
        T objectToValidate = null;
        List<Notification> notifications = new ArrayList<Notification>();
        try {
            objectToValidate = HL7V3Transformer.unmarshallMessage(clazz,
                    new ByteArrayInputStream(messageToValidate.getBytes("UTF-8")));
            if (objectToValidate == null) {
                throw new Exception();
            } else {
                switch (validatorType) {
                    case PDQV3_QUERY:
                        getConstraintValidatorModulesForPDQv3Query(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPA_IN201305UV02", module, notifications);
                        }
                        break;
                    case PDQV3_RESPONSE:
                        getConstraintValidatorModulesForPDQv3Response(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPA_IN201306UV02", module, notifications);
                        }
                        break;
                    case CH_PDQCONTINUATION:
                    case PDQV3_CONTINUATION:
                        getConstraintValidatorModulesForPDQv3Continuation(modules);
                        for (ConstraintValidatorModule module : modules) {
                            QUQIIN000003UV01Type.validateByModule((QUQIIN000003UV01Type) objectToValidate,
                                    "/QUQI_IN000003UV01", module, notifications);
                        }
                        break;
                    case CH_PDQACKNOWLEDGEMENT:
                    case PDQV3_ACKNOWLEDGEMENT:
                        getConstraintValidatorModulesForAcceptAcknowledgement(modules);
                        for (ConstraintValidatorModule module : modules) {
                            MCCIIN000002UV01Type.validateByModule((MCCIIN000002UV01Type) objectToValidate,
                                    "/MCCI_IN000002UV01", module, notifications);
                        }
                        break;
                    case CH_PDQCANCELLATION:
                    case PDQV3_CANCELLATION:
                        getConstraintValidatorModulesForPDQv3Continuation(modules);
                        for (ConstraintValidatorModule module : modules) {
                            QUQIIN000003UV01Type.validateByModule((QUQIIN000003UV01Type) objectToValidate,
                                    "/QUQI_IN000003UV01_Cancel", module, notifications);
                        }
                        break;
                    case PIXV3_ADDPATIENT:
                        getConstraintValidatorModulesForAddPatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201301UV02Type.validateByModule((PRPAIN201301UV02Type) objectToValidate, "/PRPA_IN201301UV02", module, notifications);
                        }
                        break;
                    case PIXV3_REVISEPATIENT:
                        getConstraintValidatorModulesForRevisePatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201302UV02Type.validateByModule((PRPAIN201302UV02Type) objectToValidate, "/PRPA_IN201302UV02", module, notifications);
                        }
                        break;
                    case PIXV3_MERGEPATIENT:
                        getConstraintValidatorModulesForMergePatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201304UV02Type
                                    .validateByModule((PRPAIN201304UV02Type) objectToValidate, "/PRPA_IN201304UV02", module,
                                            notifications);
                        }
                        break;
                    case CH_PIXQUERY:
                        getConstraintValidatorModulesForCHPIXV3Query(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201309UV02Type.validateByModule((PRPAIN201309UV02Type) objectToValidate,
                                    "/PRPA_IN201309UV02", module, notifications);
                        }
                        break;
                    case PIXV3_QUERY:
                        getConstraintValidatorModulesForPIXV3Query(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201309UV02Type
                                    .validateByModule((PRPAIN201309UV02Type) objectToValidate, "/PRPA_IN201309UV02", module,
                                            notifications);
                        }
                        break;
                    case PIXV3_QUERYRESPONSE:
                        getConstraintValidatorModulesForPIXV3QueryResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201310UV02Type
                                    .validateByModule((PRPAIN201310UV02Type) objectToValidate, "/PRPA_IN201310UV02", module,
                                            notifications);
                        }
                        break;
                    case PIXV3_ITI44ACK:
                        getConstraintValidatorModulesForAcceptAcknowledgement(modules);
                        for (ConstraintValidatorModule module : modules) {
                            MCCIIN000002UV01Type.validateByModule((MCCIIN000002UV01Type) objectToValidate,
                                    "/MCCI_IN000002UV01", module, notifications);
                        }
                        break;
                    case CH_PIXUPDATENOTIFICATION:
                    case PIXV3_UPDATENOTIFICATION:
                        getConstraintValidatorModulesForPIXV3UpdateNotification(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201302UV02Type.validateByModule((PRPAIN201302UV02Type) objectToValidate,
                                    "/PRPA_IN201302UV02", module, notifications);
                        }
                        break;
                    case CH_PIXITI46ACK:
                    case PIXV3_ITI46ACK:
                        getConstraintValidatorModulesForAcceptAcknowledgement(modules);
                        for (ConstraintValidatorModule module : modules) {
                            MCCIIN000002UV01Type.validateByModule((MCCIIN000002UV01Type) objectToValidate,
                                    "/MCCI_IN000002UV01", module, notifications);
                        }
                        break;
                    case XCPD_QUERYREQUEST:
                        getConstraintValidatorModulesForXCDPQueryRequest(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPA_IN201305UV02", module, notifications);
                        }
                        break;
                    case XCPD_QUERYDEFERRED:
                        getConstraintValidatorModulesForXCDPQueryDeferredResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPA_IN201305UV02", module, notifications);
                        }
                        break;
                    case KSA_KPDQV3QUERY:
                        getConstraintValidatorModulesForKPDQQuery(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPA_IN201305UV02", module, notifications);
                        }
                        break;
                    case KSA_KPDQV3QUERYRESPONSE:
                        getConstraintValidatorModulesForKPDQQueryResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPA_IN201306UV02", module, notifications);
                        }
                        break;
                    case XCPD_QUERYRESPONSE:
                        getConstraintValidatorModulesForXCDPQueryResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPA_IN201306UV02", module, notifications);
                        }
                        break;
                    case CH_XCPDACK:
                    case XCPD_ACK:
                        getConstraintValidatorModulesForAcceptAcknowledgement(modules);
                        for (ConstraintValidatorModule module : modules) {
                            MCCIIN000002UV01Type.validateByModule((MCCIIN000002UV01Type) objectToValidate,
                                    "/MCCI_IN000002UV01", module, notifications);
                        }
                        break;
                    case CH_PDQQUERY:
                        getConstraintValidatorModulesForCHPDQQuery(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPAIN201305UV02Type", module, notifications);
                        }
                        break;
                    case CH_PDQRESPONSE:
                        getConstraintValidatorModulesForCHPDQResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPAIN201306UV02Type", module, notifications);
                        }
                        break;
                    case CH_PIXFEED_ACKNOWLEDGEMENT:
                        getConstraintValidatorModulesForAcceptAcknowledgement(modules);
                        for (ConstraintValidatorModule module : modules) {
                            MCCIIN000002UV01Type.validateByModule((MCCIIN000002UV01Type) objectToValidate,
                                    "/MCCIIN000002UV01Type", module, notifications);
                        }
                        break;
                    case CH_PIXFEED_ADDPATIENT:
                        getConstraintValidatorModulesForCHPIXFeedAddPatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201301UV02Type.validateByModule((PRPAIN201301UV02Type) objectToValidate,
                                    "/PRPAIN201301UV02Type", module, notifications);
                        }
                        break;
                    case CH_PIXFEED_REVISEPATIENT:
                        getConstraintValidatorModulesForCHPIXFeedRevisePatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201302UV02Type.validateByModule((PRPAIN201302UV02Type) objectToValidate,
                                    "/PRPAIN201302UV02Type", module, notifications);
                        }
                        break;
                    case CH_PIXFEED_MERGEPATIENT:
                        getConstraintValidatorModulesForCHPIXFeedMergePatient(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201304UV02Type.validateByModule((PRPAIN201304UV02Type) objectToValidate,
                                    "/PRPAIN201304UV02Type", module, notifications);
                        }
                        break;
                    case CH_PIXQUERYRESPONSE:
                        getConstraintValidatorModulesForCHPIXQueryResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201310UV02Type.validateByModule((PRPAIN201310UV02Type) objectToValidate,
                                    "/PRPAIN201310UV02Type", module, notifications);
                        }
                        break;
                    case CH_XCPDQUERYREQUEST:
                        getConstraintValidatorModulesForCHXCPDRequest(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPAIN201305UV02Type", module, notifications);
                        }
                        break;
                    case CH_XCPDQUERYDEFERRED:
                        getConstraintValidatorModulesForCHXCPDRequestDeferred(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPAIN201305UV02Type", module, notifications);
                        }
                        break;
                    case CH_XCPDQUERYRESPONSE:
                        getConstraintValidatorModulesForCHXCPDResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPAIN201306UV02Type", module, notifications);
                        }
                        break;
                    case EHDSI_XCPD_REQUEST:
                        getConstraintValidatorModulesForEHDSIXCPDRequest(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201305UV02Type.validateByModule((PRPAIN201305UV02Type) objectToValidate,
                                    "/PRPAIN201305UV02Type", module, notifications);
                        }
                        break;
                    case EHDSI_XCPD_RESPONSE:
                        getConstraintValidatorModulesForEHDSIXCPDResponse(modules);
                        for (ConstraintValidatorModule module : modules) {
                            PRPAIN201306UV02Type.validateByModule((PRPAIN201306UV02Type) objectToValidate,
                                    "/PRPAIN201306UV02Type", module, notifications);
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (ValidatorException e) {
            errorWhenExtracting(e, notifications);
        } catch (Exception e) {
            errorWhenExtracting(null, notifications);
        }
        return notifications;
    }

    private void getConstraintValidatorModulesForEHDSIXCPDRequest(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYREQUESTPackValidator());
        modules.add(new EHDSIITI55REQUESTPackValidator());
    }

    private void getConstraintValidatorModulesForEHDSIXCPDResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYRESPONSEPackValidator());
        modules.add(new EHDSIITI55RESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForCHXCPDResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYRESPONSEPackValidator());
        modules.add(new CHXCPDRESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForCHXCPDRequestDeferred(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPackValidator());
        modules.add(new XCPDDEFERREDRESPONSEPackValidator());
        modules.add(new CHXCPDQUERYPackValidator());
    }

    private void getConstraintValidatorModulesForCHXCPDRequest(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYREQUESTPackValidator());
        modules.add(new CHXCPDQUERYPackValidator());
    }

    private void getConstraintValidatorModulesForCHPIXQueryResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new ITI45RESPONSEPackValidator());
        modules.add(new CHPIXQUERYRESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForCHPIXFeedMergePatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTMERGEPackValidator());
    }

    private void getConstraintValidatorModulesForCHPIXFeedRevisePatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTREVISEPackValidator());
        modules.add(new CHPIXFEEDREVISEPackValidator());
    }

    private void getConstraintValidatorModulesForCHPIXFeedAddPatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTADDPackValidator());
        modules.add(new CHPIXFEEDADDPackValidator());
    }

    private void getConstraintValidatorModulesForCHPDQResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new PDQV3RESPONSEPackValidator());
        modules.add(new CHPDQRESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForCHPDQQuery(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new PDQV3QUERYPackValidator());
        modules.add(new CHPDQQUERYPackValidator());
    }

    private void getConstraintValidatorModulesForXCDPQueryResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYRESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForPIXV3UpdateNotification(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI46UPDATENOTIFICATIONPackValidator());
    }

    private void getConstraintValidatorModulesForPIXV3QueryResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000300UV01PackValidator());
        modules.add(new MFMIMT700711UV01PackValidator());
        modules.add(new ITI45RESPONSEPackValidator());
    }

    private void getConstraintValidatorModulesForPIXV3Query(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new ITI45QUERYPackValidator());
    }

    private void getConstraintValidatorModulesForCHPIXV3Query(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new ITI45QUERYPackValidator());
        modules.add(new CHPIXQUERYPackValidator());
    }

    private void getConstraintValidatorModulesForMergePatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTMERGEPackValidator());
    }

    private void getConstraintValidatorModulesForRevisePatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTREVISEPackValidator());
    }

    private void getConstraintValidatorModulesForAddPatient(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new MFMIMT700701UV01PackValidator());
        modules.add(new ITI44COMMONPackValidator());
        modules.add(new ITI44PATIENTADDPackValidator());
    }

    public static void addResultHeader(DetailedResult result, HL7V3ValidatorType validator) {
        Date dd = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        result.setValidationResultsOverview(new ValidationResultsOverview());
        result.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
        result.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
        result.getValidationResultsOverview().setValidationServiceName(
                "Gazelle HL7v3 Validator : " + validator.getLabel());
        String validationEngine = "GazelleHL7v2Validator";
        validationEngine = validationEngine.concat(":");
        validationEngine = validationEngine.concat(metadataServiceProvider.getMetadata().getVersion());
        result.getValidationResultsOverview().setValidationEngine(validationEngine);
        result.getValidationResultsOverview().setValidationTestResult("PASSED");
        if ((result.getDocumentValidXSD() != null) && (result.getDocumentValidXSD().getResult() != null)
                && (result.getDocumentValidXSD().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
        if ((result.getDocumentWellFormed() != null) && (result.getDocumentWellFormed().getResult() != null)
                && (result.getDocumentWellFormed().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
        if ((result.getMDAValidation() != null) && (result.getMDAValidation().getResult() != null)
                && (result.getMDAValidation().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
    }

    private void errorWhenExtracting(ValidatorException vexp, List<Notification> ln) {
        if (ln == null) {
            return;
        }
        if (vexp != null) {
            if (vexp.getDiagnostic() != null) {
                for (Notification notification : vexp.getDiagnostic()) {
                    ln.add(notification);
                }
            }
        } else {
            net.ihe.gazelle.validation.Error err = new net.ihe.gazelle.validation.Error();
            err.setTest("structure");
            err.setLocation("All the document");
            err.setDescription("The root of the element is not the one expected by the tool");
            ln.add(err);
        }
    }

    protected void getConstraintValidatorModulesForKPDQQuery(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000100UV01PackValidator());
        listConstraintValidatorModule.add(new PDQV3QUERYPackValidator());
        listConstraintValidatorModule.add(new KPDQQUERYPackValidator());
    }

    protected void getConstraintValidatorModulesForPDQv3Response(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000300UV01PackValidator());
        listConstraintValidatorModule.add(new MFMIMT700711UV01PackValidator());
        listConstraintValidatorModule.add(new PDQV3RESPONSEPackValidator());
    }

    protected void getConstraintValidatorModulesForPDQv3Continuation(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000300UV01PackValidator());
        listConstraintValidatorModule.add(new QUQIMT000001UV01PackValidator());
        listConstraintValidatorModule.add(new PDQV3CONTINUATIONPackValidator());
    }

    protected void getConstraintValidatorModulesForAcceptAcknowledgement(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000200UV01PackValidator());
        listConstraintValidatorModule.add(new ACCEPTACKNOWLEDGEMENTPackValidator());
    }

    protected void getConstraintValidatorModulesForXCDPQueryDeferredResponse(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPARAMETERLISTPackValidator());
        modules.add(new XCPDDEFERREDRESPONSEPackValidator());
    }

    protected void getConstraintValidatorModulesForXCDPQueryRequest(List<ConstraintValidatorModule> modules) {
        modules.add(new MCCIMT000100UV01PackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYPARAMETERLISTPackValidator());
        modules.add(new XCPDPATIENTDISCOVERYQUERYREQUESTPackValidator());
    }

    protected void getConstraintValidatorModulesForPDQv3Query(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000100UV01PackValidator());
        listConstraintValidatorModule.add(new PDQV3QUERYPackValidator());
    }

    protected void getConstraintValidatorModulesForKPDQQueryResponse(
            List<ConstraintValidatorModule> listConstraintValidatorModule) {
        listConstraintValidatorModule.add(new MCCIMT000300UV01PackValidator());
        listConstraintValidatorModule.add(new MFMIMT700711UV01PackValidator());
        listConstraintValidatorModule.add(new PDQV3RESPONSEPackValidator());
        listConstraintValidatorModule.add(new KPDQQUERYRESPONSEPackValidator());
    }

    protected <T> void validateAgainstSchema(DetailedResult result, String messageToValidate, HL7V3ValidatorType type) {
        XMLValidation xmlValidation = new XMLValidation(saxParserFactory, xsdLocation);
        DocumentWellFormed documentWellFormedResult = xmlValidation.isXMLWellFormed(messageToValidate);
        result.setDocumentWellFormed(documentWellFormedResult);
        if (result.getDocumentWellFormed().getResult().equals("PASSED")) {
            try {
                result.setDocumentValidXSD(xmlValidation.isXSDValid(messageToValidate));
            } catch (Exception e) {
                log.info("error when validating with the schema", e);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PrintStream ps;
                try {
                    ps = new PrintStream(baos, false, "UTF-8");
                } catch (UnsupportedEncodingException e1) {
                    ps = new PrintStream(baos);
                    log.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e1);
                }
                e.printStackTrace(ps);
                DocumentValidXSD docv = new DocumentValidXSD();
                docv.setResult("FAILED");
                docv.getXSDMessage().add(new XSDMessage());
                docv.getXSDMessage().get(0).setSeverity("error");
                String xsdMessage;
                try {
                    xsdMessage = baos.toString("UTF-8");
                } catch (UnsupportedEncodingException e1) {
                    xsdMessage = baos.toString();
                    log.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e1);
                }
                docv.getXSDMessage()
                        .get(0)
                        .setMessage(
                                "error when validating with the schema. The error is : "
                                        + StringUtils.substring(xsdMessage, 0, 300));
                result.setDocumentValidXSD(docv);
            }
        }
    }

    public static String getDetailedResultAsString(DetailedResult detailedResult) {
        if (detailedResult != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
                Marshaller m = jc.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(detailedResult, baos);
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
            }
            try {
                return baos.toString("UTF-8");
            } catch (UnsupportedEncodingException e) {
                log.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e);
                return baos.toString();
            }
        }
        return null;
    }
}
