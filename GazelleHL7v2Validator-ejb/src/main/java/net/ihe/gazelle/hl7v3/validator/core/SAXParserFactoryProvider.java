package net.ihe.gazelle.hl7v3.validator.core;

import java.io.File;

import javax.ejb.Remove;
import javax.xml.parsers.SAXParserFactory;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.hl7v3.validator.util.XMLValidation;
import net.ihe.gazelle.preferences.PreferenceService;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import javax.ejb.Startup;

/**
 * <p>SAXParserFactoryProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("saxParserFactoryProvider")
@Scope(ScopeType.APPLICATION)
@Startup
@GenerateInterface("SAXParserFactoryProviderLocal")
public class SAXParserFactoryProvider implements SAXParserFactoryProviderLocal {

	private String xsdDirectoryLocation;

	// PDQv3Query
	private SAXParserFactory prpaIN201305UV02Factory;
	private String prpaIN201305UV02Xsd;

	// PDQv3Response
	private SAXParserFactory prpaIN201306UV02Factory;
	private String prpaIN201306UV02Xsd;

	// PDQv3Continuation / PDQv3Cancel
	private SAXParserFactory quqiIN000003UV01Factory;
	private String quqiIN000003UV01Xsd;

	// AcceptAcknowledgement
	private String mcciIN000002UV01Xsd;
	private SAXParserFactory mcciIN000002UV01Factory;

	// XCPD - Patient Location Query
	private String xcpdPlqXsd;
	private SAXParserFactory xcpdPlqXsdFactory;

	// PIXV3Query
	private SAXParserFactory prpaIN201309UV02Factory;
	private String prpaIN201309UV02Xsd;

	// PIXV3 response
	private SAXParserFactory prpaIN201310UV02Factory;
	private String prpaIN201310UV02Xsd;

	// ITI-44 add
	private SAXParserFactory prpaIN201301UV02Factory;
	private String prpaIN201301UV02Xsd;


	// ITI-44 revise
	private SAXParserFactory prpaIN201302UV02Factory;
	private String prpaIN201302UV02Xsd;

	// ITI-44 merge
	private SAXParserFactory prpaIN201304UV02Factory;
	private String prpaIN201304UV02Xsd;

	@Override
	@Create
	public void init() {
		initializeFactories();
		CommonOperations.setValueSetProvider(new SVSConsumer(){
			
			@Override
			protected String getSVSRepositoryUrl(){
				return PreferenceService.getString("svs_repository_url");
			}
		});
	}

	private void initializeFactories() {
		xsdDirectoryLocation = ApplicationConfiguration.getValueOfVariable("xsd_directory_location");
		prpaIN201305UV02Xsd = buildXsdPath("PRPA_IN201305UV02.xsd");
		prpaIN201305UV02Factory = XMLValidation.initializeFactory(prpaIN201305UV02Xsd);
		prpaIN201306UV02Xsd = buildXsdPath("PRPA_IN201306UV02.xsd");
		prpaIN201306UV02Factory = XMLValidation.initializeFactory(prpaIN201306UV02Xsd);
		quqiIN000003UV01Xsd = buildXsdPath("QUQI_IN000003UV01.xsd");
		quqiIN000003UV01Factory = XMLValidation.initializeFactory(quqiIN000003UV01Xsd);
		mcciIN000002UV01Xsd = buildXsdPath("MCCI_IN000002UV01.xsd");
		mcciIN000002UV01Factory = XMLValidation.initializeFactory(mcciIN000002UV01Xsd);
		xcpdPlqXsd = ApplicationConfiguration.getValueOfVariable("xcpd_plq_xsd_location");
		xcpdPlqXsdFactory = XMLValidation.initializeFactory(xcpdPlqXsd);
		prpaIN201301UV02Xsd = buildXsdPath("PRPA_IN201301UV02.xsd");
		prpaIN201301UV02Factory = XMLValidation.initializeFactory(prpaIN201301UV02Xsd);
		prpaIN201302UV02Xsd = buildXsdPath("PRPA_IN201302UV02.xsd");
		prpaIN201302UV02Factory = XMLValidation.initializeFactory(prpaIN201302UV02Xsd);
		prpaIN201304UV02Xsd = buildXsdPath("PRPA_IN201304UV02.xsd");
		prpaIN201304UV02Factory = XMLValidation.initializeFactory(prpaIN201304UV02Xsd);
		prpaIN201309UV02Xsd = buildXsdPath("PRPA_IN201309UV02.xsd");
		prpaIN201309UV02Factory = XMLValidation.initializeFactory(prpaIN201309UV02Xsd);
		prpaIN201310UV02Xsd = buildXsdPath("PRPA_IN201310UV02.xsd");
		prpaIN201310UV02Factory = XMLValidation.initializeFactory(prpaIN201310UV02Xsd);
	}

	private String buildXsdPath(String fileNameWithExtension) {
		return xsdDirectoryLocation.concat(File.separator).concat(fileNameWithExtension);
	}

	/**
	 * <p>instance.</p>
	 *
	 * @return the bean to access the factories
	 */
	public static SAXParserFactoryProviderLocal instance() {
		return (SAXParserFactoryProviderLocal) Component.getInstance("saxParserFactoryProvider");
	}


	@Override
	@Remove
	public void destroy() {

	}


	@Override
	public String getXsdDirectoryLocation() {
		return xsdDirectoryLocation;
	}


	@Override
	public SAXParserFactory getPrpaIN201305UV02Factory() {
		return prpaIN201305UV02Factory;
	}


	@Override
	public String getPrpaIN201305UV02Xsd() {
		return prpaIN201305UV02Xsd;
	}


	@Override
	public SAXParserFactory getPrpaIN201306UV02Factory() {
		return prpaIN201306UV02Factory;
	}


	@Override
	public String getPrpaIN201306UV02Xsd() {
		return prpaIN201306UV02Xsd;
	}


	@Override
	public SAXParserFactory getQuqiIN000003UV01Factory() {
		return quqiIN000003UV01Factory;
	}


	@Override
	public String getQuqiIN000003UV01Xsd() {
		return quqiIN000003UV01Xsd;
	}


	@Override
	public String getMcciIN000002UV01Xsd() {
		return mcciIN000002UV01Xsd;
	}


	@Override
	public SAXParserFactory getMcciIN000002UV01Factory() {
		return mcciIN000002UV01Factory;
	}


	@Override
	public String getXcpdPlqXsd() {
		return xcpdPlqXsd;
	}


	@Override
	public SAXParserFactory getXcpdPlqXsdFactory() {
		return xcpdPlqXsdFactory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201301UV02Factory</code>.</p>
	 *
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public SAXParserFactory getPrpaIN201301UV02Factory() {
		return prpaIN201301UV02Factory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201302UV02Factory</code>.</p>
	 *
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public SAXParserFactory getPrpaIN201302UV02Factory() {
		return prpaIN201302UV02Factory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201304UV02Factory</code>.</p>
	 *
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public SAXParserFactory getPrpaIN201304UV02Factory() {
		return prpaIN201304UV02Factory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201309UV02Factory</code>.</p>
	 *
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public SAXParserFactory getPrpaIN201309UV02Factory() {
		return prpaIN201309UV02Factory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201310UV02Factory</code>.</p>
	 *
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public SAXParserFactory getPrpaIN201310UV02Factory() {
		return prpaIN201310UV02Factory;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201301UV02Xsd</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPrpaIN201301UV02Xsd() {
		return prpaIN201301UV02Xsd;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201302UV02Xsd</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPrpaIN201302UV02Xsd() {
		return prpaIN201302UV02Xsd;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201304UV02Xsd</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPrpaIN201304UV02Xsd() {
		return prpaIN201304UV02Xsd;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201309UV02Xsd</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPrpaIN201309UV02Xsd() {
		return prpaIN201309UV02Xsd;
	}

	/**
	 * <p>Getter for the field <code>prpaIN201310UV02Xsd</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPrpaIN201310UV02Xsd() {
		return prpaIN201310UV02Xsd;
	}
}
