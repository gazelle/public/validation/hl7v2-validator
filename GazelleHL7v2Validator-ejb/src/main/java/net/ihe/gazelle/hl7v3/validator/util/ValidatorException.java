package net.ihe.gazelle.hl7v3.validator.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.validation.Notification;

/**
 * <p>ValidatorException class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ValidatorException extends JAXBException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Notification> diagnostic;

	/**
	 * <p>Getter for the field <code>diagnostic</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Notification> getDiagnostic() {
		if (diagnostic == null){
			diagnostic = new ArrayList<Notification>();
		}
		return diagnostic;
	}

	/**
	 * <p>Setter for the field <code>diagnostic</code>.</p>
	 *
	 * @param diagnostic a {@link java.util.List} object.
	 */
	public void setDiagnostic(List<Notification> diagnostic) {
		this.diagnostic = diagnostic;
	}

	/**
	 * <p>Constructor for ValidatorException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param diagnostic a {@link java.util.List} object.
	 */
	public ValidatorException(String message, List<Notification> diagnostic) {
		super(message);
		
	}

}
