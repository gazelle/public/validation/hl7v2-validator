<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 19, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> Xavier FRANCOIS, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>


    <xsl:template match="/">
        <html>
            <head>
                <title>External Validation Report</title>
            </head>
            <body>
                <script type="text/javascript">
                    function hideOrViewValidationDetailsMB() {
                    var detaileddiv = document.getElementById('resultdetailedann');
                    if (detaileddiv != null){
                    var onn = document.getElementById('resultdetailedann_switch_on');
                    if (onn != null){
                    if (onn.style.display == 'block') onn.style.display = 'none';
                    else if (onn.style.display == 'none') onn.style.display = 'block';
                    }
                    var off = document.getElementById('resultdetailedann_switch_off');
                    if (off != null){
                    if (off.style.display == 'block') off.style.display = 'none';
                    else if (off.style.display == 'none') off.style.display = 'block';
                    }
                    var body = document.getElementById('resultdetailedann_body');
                    if (body != null){
                    if (body.style.display == 'block') body.style.display = 'none';
                    else if (body.style.display == 'none') body.style.display = 'block';
                    }
                    }
                    }

                    function hideOrUnhideMB(elem){
                    var elemToHide = document.getElementById(elem.name + '_p');
                    if (elemToHide != null){
                    if (elem.checked){
                    elemToHide.style.display = 'none';
                    }
                    else{
                    elemToHide.style.display = 'block';
                    }
                    }
                    }

                    function detectDownloadResultButton(){
                    var elemDown = document.getElementById('resultForm:downloadMBResultB');
                    if (elemDown != null) return true;
                    return false;
                    }

                    function extractDownloadResultButton(parentNoeud){
                    var elemDown = document.getElementById('resultForm:downloadMBResultB').cloneNode(true);
                    if (elemDown != null) {
                    var downloadspan = document.getElementById('downloadspan');
                    downloadspan.appendChild(elemDown);
                    }
                    }

                </script>

                <h2>External Validation Report</h2>


                <div class="panel panel-info">
                    <div class="panel-heading">General Informations (from XSL)</div>
                    <div class="panel-body">
                        <dl class="dl-horizontal">

                                <dt>Validation Date</dt>
                                <dd><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></dd>

                                <dt>Validation Service</dt>
                                <dd>
                                    <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceName"/>
                                    <xsl:if test="count(detailedResult/ValidationResultsOverview/ValidationServiceVersion) = 1">
                                        (Version : <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)
                                    </xsl:if>
                                </dd>

                                <dt>Validation Test Status</dt>
                                <dd>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </dd>

                        </dl>

                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading ">Result overview</div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <xsl:if test="count(detailedResult/DocumentWellFormed/Result) = 1">
                                <li class="active">
                                    <a data-toggle="tab" href="#wellformed">JSON (<xsl:element name="span"><xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentWellFormed/Result"/></xsl:attribute>
                                        <xsl:value-of select="detailedResult/DocumentWellFormed/Result"/></xsl:element>)</a>
                                </li>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/DocumentValidXSD/Result) = 1">
                                <li>
                                    <a data-toggle="tab" href="#xsd">XSD (<xsl:element name="span"><xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentValidXSD/Result"/></xsl:attribute></xsl:element>
                                        <xsl:value-of select="detailedResult/DocumentValidXSD/Result"/>)</a>
                                </li>
                            </xsl:if>
                            <li>
                                <xsl:if test="count(detailedResult/DocumentWellFormed/Result) = 0">
                                    <xsl:attribute name="class">active</xsl:attribute>
                                </xsl:if>
                                <a data-toggle="tab" href="#mbv">Object Checker Validation
                                    (
                                    <xsl:element name="span"><xsl:attribute name="class"><xsl:value-of select="detailedResult/MDAValidation/Result"/></xsl:attribute>
                                    <xsl:value-of select="detailedResult/MDAValidation/Result"/></xsl:element>)</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <xsl:if test="count(detailedResult/DocumentWellFormed/Result) = 1">
                                <div id="wellformed" class="tab-pane fade in active">
                                    <h3>JSON Validation Report</h3>
                                    <i>The document you have validated is supposed to be a JSON document. The validator has checked if it is well-formed, results of this validation are gathered in this part.</i>

                                    <xsl:choose>
                                        <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                            <p class="PASSED">The JSON document is well-formed</p>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <p class="FAILED">The JSON document is not well-formed, for the following reasons: </p>
                                            <xsl:for-each select="detailedResult/DocumentWellFormed">
                                                <xsl:call-template name="viewXSDFailMessages" />
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                </div>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">

                                <div id="xsd" class="tab-pane fade">
                                    <h3>XSD Validation detailed Results</h3>

                                    <i>Your JSON document has been validating about the appropriate XSD schema, here is the detail of the validation outcome.</i>
                                    <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                                        <xsl:choose>
                                            <xsl:when test="(detailedResult/DocumentValidXSD/Result='PASSED') or (detailedResult/DocumentValidXSD/nbOfErrors=0 and count(detailedResult/DocumentValidXSD/Result)=0)">
                                                <p class="PASSED">The JSON document is valid regarding the schema</p>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <p class="FAILED">The JSON document is not valid regarding the schema because of the following reasons: </p>
                                                <xsl:for-each select="detailedResult/DocumentValidXSD">
                                                    <xsl:call-template name="viewXSDFailMessages" />
                                                </xsl:for-each>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                </div>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/MDAValidation) = 1">
                                <div id="mbv" class="tab-pane fade">
                                    <xsl:if test="count(detailedResult/DocumentWellFormed/Result) = 0">
                                        <xsl:attribute name="class">active</xsl:attribute>
                                    </xsl:if>
                                    <h3>Model Based Validation details</h3>
                                    <div class="rich-stglpanel-marker">
                                        <div id="resultdetailedann_switch_on" class="rich-stglpnl-marker" style="display: block;"><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                                        <div id="resultdetailedann_switch_off" class="rich-stglpnl-marker" style="display: none;"><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                                    </div>
                                        <dl class="dl-horizontal">

                                                <dt>
                                                    <b>Result</b>
                                                </dt>
                                                <dd>
                                                    <xsl:if test="count(detailedResult/MDAValidation/Error) = 0">
                                                        <div class="PASSED">PASSED</div>
                                                    </xsl:if>
                                                    <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                                        <div class="FAILED">FAILED</div>
                                                    </xsl:if>
                                                </dd>


                                                <dt width="100px" valign="top">
                                                    <b>Summary</b>
                                                </dt>
                                                <dd>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Error) + count(detailedResult/MDAValidation/Warning) + count(detailedResult/MDAValidation/Info) + count(detailedResult/MDAValidation/Note)"/> checks <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Error)"/> errors <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Warning)"/> warning <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Info)"/> infos <br/>
                                                </dd>

                                        </dl>
                                    <b>HIDE : </b>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Errors">Errors</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Warnings">Warnings</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Infos">Infos</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Reports">Reports</input>
                                    <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                        <div id="Errors_p">
                                            <p><b>Errors</b></p>
                                            <xsl:for-each select="detailedResult/MDAValidation/Error">
                                                <xsl:call-template name="viewnotification">
                                                    <xsl:with-param name="kind">gzl-notification gzl-notification-red</xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                                        <div id="Warnings_p">
                                            <p><b>Warnings</b></p>
                                            <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                                <xsl:call-template name="viewnotification">
                                                    <xsl:with-param name="kind">gzl-notification gzl-notification-orange</xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Info) &gt; 0">
                                        <div id="Infos_p">
                                            <p><b>Infos</b></p>
                                            <xsl:for-each select="detailedResult/MDAValidation/Info">
                                                <xsl:call-template name="viewnotification">
                                                    <xsl:with-param name="kind">gzl-notification gzl-notification-green</xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
                                        <div id="Reports_p">
                                            <p><b>Reports</b></p>
                                            <xsl:for-each select="detailedResult/MDAValidation/Note">
                                                <xsl:if test="position() &lt; 101">
                                                    <xsl:call-template name="viewnotification">
                                                       <xsl:with-param name="kind">gzl-notification gzl-notification-green</xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:if>
                                            </xsl:for-each>
                                            <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 100">
                                                <div class="gzl-notification gzl-notification-green" width="98%">
                                                   <p><b>..........</b></p>
                                                </div>
                                                <br/>
                                                <div class="moore" width="98%">

                                                        <p valign="top" width="100"><b>All errors and warnings are shown above. If you want to view the complete report including all positive checks, please download the 'Model Based Validation Result'.</b>
                                                            <br/>
                                                            <span id="downloadspan"><script type="text/javascript">extractDownloadResultButton()</script></span>
                                                        </p>
                                                    </div>
                                            </xsl:if>
                                        </div>
                                    </xsl:if>

                                </div>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/nistValidation) = 1">
                                <xsl:if test="detailedResult/nistValidation != ''">
                                    <br />
                                    <div class="panel panel-info" id="resultdetailedannNist">
                                        <div class="panel-heading" onclick="hideOrViewNistValidationDetails();">
                                            Nist Validation details
                                            <div class="rich-stglpanel-marker">
                                                <div id="resultdetailedann_switch_on_nist" class="rich-stglpnl-marker" style="display: block;"><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                                                <div id="resultdetailedann_switch_off_nist" class="rich-stglpnl-marker" style="display: none;"><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                                            </div>
                                        </div>
                                        <div class="panel-body" style="display: block;" id="resultdetailedann_body_nist">
                                            <xsl:value-of select="detailedResult/nistValidation" disable-output-escaping="yes"/>
                                        </div>
                                    </div>
                                </xsl:if>
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>
        <div>
            <xsl:attribute name="class"><xsl:value-of select="$kind"/></xsl:attribute>
<dl class="dl-horizontal">
            <xsl:if test="count(Test) &gt; 0">

                    <dt><b>Test</b></dt>
                    <dd><xsl:value-of select="Test"/></dd>

            </xsl:if>
            <xsl:if test="count(Location) &gt; 0">

                    <dt><b>Location</b></dt>
                    <dd><xsl:value-of select="Location"/> <xsl:if test="$viewdown = 'true'"><img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px" onclick="gotoo(this)"/></xsl:if></dd>

            </xsl:if>

                <dt><b>Description</b></dt>
                <dd>
                    <xsl:value-of select="Description"/>
                    <xsl:if test="Identifiant and ($constraintPath != '#')">
                        [<a>
                        <xsl:attribute name="href"><xsl:value-of select="$constraintPath"/><xsl:value-of select="Identifiant"/>.html</xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute>
                        Constraint...
                    </a>]
                    </xsl:if>
                    <xsl:if test="assertion and ($assertionManagerPath != '#')">
                        [<a>
                        <xsl:attribute name="href"><xsl:value-of select="$assertionManagerPath"/>rest/testAssertion/assertion?idScheme=<xsl:value-of select="assertion/@idScheme"/>&amp;assertionId=<xsl:value-of select="assertion/@assertionId"/></xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute>
                        Assertion...
                    </a>]
                    </xsl:if>
                </dd>
        </dl>
</div>
        <br/>
    </xsl:template>

    <xsl:template name="viewXSDFailMessages">
        <ul>
            <xsl:for-each select="XSDMessage">
                <li>
                    <xsl:value-of select="Severity" />
                    <xsl:text>: </xsl:text>
                    <xsl:value-of select="Message" />
                    <xsl:if test="lineNumber">
                        <xsl:text> (see line </xsl:text>
                        <xsl:value-of select="lineNumber" />
                        <xsl:text>, column </xsl:text>
                        <xsl:value-of select="columnNumber" />
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>



</xsl:stylesheet>

