<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" id="MessageContext" version="1.0">
    <xs:element name="ValidationContext">
        <xs:complexType>
            <xs:sequence>
                <xs:choice>
                    <xs:element minOccurs="1" name="ProfileOID" type="xs:string">
                        <xs:annotation>
                            <xs:documentation>This tag should contain the OID of the HL7 message profile as reference in Gazelle Master Model at http://gazelle.ihe.net/GMM</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="Profile" type="ProfileDefinition"/>
                </xs:choice>
                <xs:element minOccurs="0" name="ValidationOptions">
                    <xs:annotation>
                        <xs:documentation>This element describes the validation options, that means, which level of log should be raised when finding error on message structure, component/field length, datatype and values</xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element minOccurs="0" name="MessageStructure" type="validationType"
                                default="ERROR">
                                <xs:annotation>
                                    <xs:documentation>Action to take when message structure is not appropriate</xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="Length" default="WARNING" minOccurs="0"
                                type="validationType">
                                <xs:annotation>
                                    <xs:documentation>Action to take when field/component length is higher than the one defined in the message profile</xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="DataType" default="ERROR" minOccurs="0"
                                type="validationType">
                                <xs:annotation>
                                    <xs:documentation>Action to take when a used datatype does not match the one defined in the profile</xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="DataValue" default="WARNING" minOccurs="0"
                                type="validationType">
                                <xs:annotation>
                                    <xs:documentation>Action to take when a value does not match any of the entries of the table</xs:documentation>
                                </xs:annotation>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
                <xs:element minOccurs="0" name="CharacterEncoding" type="xs:string"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="validationType">
        <xs:annotation>
            <xs:documentation>validation type:
	ERROR: reports an error if validation fails
	WARNING: reports a warning if validation fails
	IGNORE: does not report any comment even if validation fails</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="ERROR"/>
            <xs:enumeration value="WARNING"/>
            <xs:enumeration value="IGNORE"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="ProfileDefinition">
        <xs:sequence>
            <xs:element minOccurs="1" name="MessageSender">
                <xs:annotation>
                    <xs:documentation>If the profile OID is unknown, the user can provide the following information to the validation system.
Required keywords are available in Gazelle Master Model (http://gazelle.ihe.net/GMM)</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="DomainKeyword" type="xs:string">
                            <xs:annotation>
                                <xs:documentation>keywork of the IHE domain in which the message is sent (RAD, LAB, ITI...)</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="ActorKeyword" type="xs:string">
                            <xs:annotation>
                                <xs:documentation>keyword of the IHE actor which sends the message (PDC, ADT, LB ...)</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="TransactionKeyword" type="xs:string">
                            <xs:annotation>
                                <xs:documentation>Keyword of the IHE transaction in which this message is sent: ITI-31, RAD-3 ...</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="MessageType" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Type of the message (as described in MSH-9). ex: ADT^A01^ADT_A01, OML^O33^OML_O33 ...</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="HL7Version" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Version of the HL7 standard used in this message (2.3.1, 2.4, 2.5, 2.5.1, 2.6)</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element minOccurs="0" name="MessageOrderControlCode" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Value of ORC-1 if the value constraints the structure of the message (example for ORM^O01^ORM_O01 in radiology, NW and CA orders do not required the same segments)</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
