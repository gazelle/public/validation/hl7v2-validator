INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_url', 'http://CHANGE_URL/GazelleHL7Validator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_name', 'Gazelle HL7v2.x Validator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_admin_name', 'Anne-Gaelle Berge');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_admin_email', 'anne-gaelle@ihe-europe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'time_zone', 'UTC+01');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_issue_tracker_url', 'http://gazelle.ihe.net/jira/HLVAL');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_gazelle_release_notes_url', 'http://gazelle.ihe.net/');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'gmm_hl7messageprofile_wsdl', 'https://gazelle.ihe.net/gazelle-tm-gazelle-tf/Hl7MessageProfile?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'gmm_iheconcepts_wsdl', 'https://gazelle.ihe.net/gazelle-tm-gazelle-tf/IHEConcepts?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'xsd_location', 'HL7RegistrationSchema.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'report_xsl_location', 'https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'length_is_an_error', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'value_is_an_error', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'xsd_directory_location', '/home/gazelle/xsd/HL7V3/NE2008/multicacheschemas');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7_profiles_directory', '/HL7ConformanceProfiles/HL7MessageProfiles/ProfilesPerOid');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7_table_directory', '/HL7ConformanceProfiles/HL7Tables/TablesPerOid');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7_profile_xsd', 'https://gazelle.ihe.net/xsd/HL7MessageProfileSchema.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7_resource_xsd', 'https://gazelle.ihe.net/xsd/HL7TableSchema.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cas_enabled', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'force_stylesheet', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'profile_xsl_url', 'none');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'resource_xsl_url', 'none');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'svs_repository_url', 'https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'xcpd_plq_xsd_location', '/home/gazelle/xsd/IHE/XCPD_PLQ.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable ,value) VALUES (nextval('app_configuration_id_seq'), 'IGAMT_directory','/IGAMTConformanceProfiles');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7_directory','/opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'jar_path','/opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources/GVTValidatorResources/gvt-validation-jar-jar-with-dependencies.jar');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'link_to_cgu','cgu link that need to be changed');
SELECT pg_catalog.setval('app_configuration_id_seq', 27, true);

INSERT INTO oid_generator (id, target, root, next_value) VALUES (1, 'profile', 'to be defined.', 1);
INSERT INTO oid_generator (id, target, root, next_value) VALUES (2, 'resource','to be defined.', 1);
INSERT INTO oid_generator (id, target, root, next_value) VALUES (3, 'message', 'to be defined.', 1);
INSERT INTO oid_generator (id, target, root, next_value) VALUES (4, 'log-v3', 'to be defined.', 1);


