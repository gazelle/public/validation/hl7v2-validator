/*
Update path for hl7 message Profiles
 */
UPDATE "hl7_profile" SET path= REPLACE(path,'/profiles/HL7Profiles','/HL7ConformanceProfiles/HL7MessageProfiles');
UPDATE "hl7_profile" SET path= REPLACE(path,'/hl7-conformance-profiles','/IGAMTConformanceProfiles') ;

/*
Update path for hl7 resources
 */
UPDATE "hl7_resource" SET path = REPLACE(path,'/HL7Tables','/HL7ConformanceProfiles/HL7Tables');

/*
Update app_configuration
 */

-- UPDATE "app_configuration" SET value = REPLACE(value,'https://testing.ehealthireland.ie','https://qualification.ihe-europe.net');
UPDATE "app_configuration" SET value = '/opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources' WHERE variable ='hl7_directory' ;
UPDATE "app_configuration" SET value = '/HL7ConformanceProfiles/HL7MessageProfiles/ProfilesPerOid' WHERE variable ='hl7_profiles_directory';
UPDATE "app_configuration" SET value = '/HL7ConformanceProfiles/HL7Tables/TablesPerOid' WHERE variable ='hl7_table_directory';
UPDATE "app_configuration" SET value = '/IGAMTConformanceProfiles' WHERE variable ='IGAMT_directory';
UPDATE "app_configuration" SET value = '/opt/GazelleHL7Validator/gazelle-hl7v2-validator-resources/GVTValidatorResources/gvt-validation-jar-jar-with-dependencies.jar' WHERE variable ='jar_path';
