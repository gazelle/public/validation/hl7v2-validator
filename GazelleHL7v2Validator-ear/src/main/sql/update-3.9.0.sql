DELETE FROM app_configuration WHERE variable = 'cas_url';
DELETE FROM app_configuration WHERE variable = 'application_works_without_cas';
UPDATE app_configuration SET variable = 'cas_enabled' WHERE variable = 'cas_enable';