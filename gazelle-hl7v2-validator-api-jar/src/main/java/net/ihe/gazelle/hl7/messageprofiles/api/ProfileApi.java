package net.ihe.gazelle.hl7.messageprofiles.api;

import net.ihe.gazelle.hl7.validator.report.UsedResources;

import java.util.List;

/**
 * <p>ProfileApi interface.</p>
 *
 * @author abe
 * @version 1.0: 20/08/2019
 */
public interface ProfileApi {

    String getDirectory();

    String getFormat();

    /**
     * boolean to indicate whether it is an Igamt profile or not
     */
    boolean isIgamt();

    List<ResourceApi> getResources();

    byte[] getContent();

    String getOid();

    String getJavaPackage();
}
