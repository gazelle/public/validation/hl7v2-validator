package net.ihe.gazelle.hl7.validation.context;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * <p>ValidationType enum.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */
@XmlEnum
public enum ValidationType {

    @XmlEnumValue("ERROR") ERROR,
    @XmlEnumValue("WARNING") WARNING,
    @XmlEnumValue("IGNORE") IGNORE
}
