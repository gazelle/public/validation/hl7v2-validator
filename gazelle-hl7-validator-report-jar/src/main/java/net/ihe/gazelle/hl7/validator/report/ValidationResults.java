package net.ihe.gazelle.hl7.validator.report;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hl7.assertion.AssertionType;
import net.ihe.gazelle.hl7.assertion.GazelleHL7Assertion;
import net.ihe.gazelle.hl7.validation.context.ValidationType;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.utils.PathBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ValidationResults class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "ValidationResults")
@XmlAccessorType(XmlAccessType.NONE)
public class ValidationResults {

    private static final Integer INIT_VALUE = 0;

    @XmlElement
    private Integer errorCounter = INIT_VALUE;

    @XmlElement
    private Integer warningCounter = INIT_VALUE;

    @XmlElement
    private Integer reportCounter = INIT_VALUE;

    @XmlElement
    private Integer exceptionCounter = INIT_VALUE;

    @XmlElements({@XmlElement(name = "Warning", type = Warning.class),
            @XmlElement(name = "Error", type = Error.class),
            @XmlElement(name = "Report", type = GazelleHL7Assertion.class),
            @XmlElement(name = "ProfileException", type = GazelleProfileException.class)})
    private List<Object> notifications;

    private boolean lengthIsAnError;
    private boolean notFountValueIsAnError;
    private boolean wrongDatatypeIsAnError;
    private boolean wrongSequenceIsAnError;

    /**
     * <p>Constructor for ValidationResults.</p>
     */
    public ValidationResults() {
        this.notifications = new ArrayList<Object>();
        this.lengthIsAnError = true;
        this.notFountValueIsAnError = false;
        this.wrongDatatypeIsAnError = true;
        this.wrongSequenceIsAnError = true;
    }

    /**
     * <p>Constructor for ValidationResults.</p>
     *
     * @param actionOnLength        a {@link java.lang.String} object.
     * @param actionOnValueNotFound a {@link java.lang.String} object.
     * @param actionOnDatatype      a {@link java.lang.String} object.
     * @param actionOnSequenceError a {@link java.lang.String} object.
     */
    public ValidationResults(ValidationType actionOnLength, ValidationType actionOnValueNotFound, ValidationType actionOnDatatype,
                             ValidationType actionOnSequenceError) {
        if (ValidationType.ERROR.equals(actionOnLength)) {
            this.lengthIsAnError = true;
        } else {
            this.lengthIsAnError = false;
        }
        if (ValidationType.ERROR.equals(actionOnValueNotFound)) {
            this.notFountValueIsAnError = true;
        } else {
            this.notFountValueIsAnError = false;
        }
        if (ValidationType.ERROR.equals(actionOnDatatype)) {
            this.wrongDatatypeIsAnError = true;
        } else {
            this.wrongDatatypeIsAnError = false;
        }
        if (ValidationType.ERROR.equals(actionOnSequenceError)) {
            this.wrongSequenceIsAnError = true;
        } else {
            this.wrongSequenceIsAnError = false;
        }
        this.notifications = new ArrayList<Object>();
    }

    /**
     * <p>addNotification.</p>
     *
     * @param message     a {@link java.lang.String} object.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param value       a {@link java.lang.String} object.
     */
    public void addNotification(String message, PathBuilder pathBuilder, String value) {
        this.notifications.add(new Error(message, pathBuilder, value));
        this.errorCounter++;
    }

    public void addError(String message, int errorCode, String hl7Path){
        Error error = new Error (message, null, null);
        error.setHl7Path(hl7Path);
        error.setGazelleErrorCode(errorCode);
        this.notifications.add(error);
        this.errorCounter++;
    }

    public void addWarning(String message, int errorCode, String hl7Path){
        Warning warning = new Warning (message, null, null);
        warning.setHl7Path(hl7Path);
        warning.setGazelleErrorCode(errorCode);
        this.notifications.add(warning);
        this.warningCounter++;
    }

    /**
     * <p>addNotification.</p>
     *
     * @param message     a {@link java.lang.String} object.
     * @param tableId     a {@link java.lang.String} object.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param inValue     a {@link java.lang.String} object.
     */
    public void addNotification(String message, String tableId, PathBuilder pathBuilder, String inValue) {
        this.notifications.add(new Error(message, tableId, pathBuilder, inValue));
        this.errorCounter++;
    }

    /**
     * <p>addNotification.</p>
     *
     * @param exception   a {@link ca.uhn.hl7v2.HL7Exception} object.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param inValue     a {@link java.lang.String} object.
     */
    public void addNotification(HL7Exception exception, PathBuilder pathBuilder, String inValue) {
        if (createError(GazelleErrorCode.errorCodeFor(exception.getErrorCode()))) {
            this.notifications.add(new Error(exception, pathBuilder, inValue));
            this.errorCounter++;
        } else {
            this.notifications.add(new Warning(exception, pathBuilder, inValue));
            this.warningCounter++;
        }
    }

    /**
     * <p>addNotification.</p>
     *
     * @param cause a {@link java.lang.Throwable} object.
     */
    public void addNotification(Throwable cause) {
        this.notifications.add(new Error(cause));
        this.errorCounter++;
    }

    /**
     * <p>addNotification.</p>
     *
     * @param message     a {@link java.lang.String} object.
     * @param errorCode   a {@link net.ihe.gazelle.hl7.exception.GazelleErrorCode} object.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param inValue     a {@link java.lang.String} object.
     */
    public void addNotification(String message, GazelleErrorCode errorCode, PathBuilder pathBuilder, String inValue) {
        if (createError(errorCode)) {
            this.notifications.add(new Error(message, errorCode, pathBuilder, inValue));
            this.errorCounter++;
        } else {
            this.notifications.add(new Warning(message, errorCode, pathBuilder, inValue));
            this.warningCounter++;
        }
    }

    /**
     * <p>addNotification.</p>
     *
     * @param message     a {@link java.lang.String} object.
     * @param errorCode   a int.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param inValue     a {@link java.lang.String} object.
     */
    public void addNotification(String message, int errorCode, PathBuilder pathBuilder, String inValue) {
        if (createError(GazelleErrorCode.errorCodeFor(errorCode))) {
            this.notifications.add(new Error(message, errorCode, pathBuilder, inValue));
            this.errorCounter++;
        } else {
            this.notifications.add(new Warning(message, errorCode, pathBuilder, inValue));
            this.warningCounter++;
        }
    }

    /**
     * <p>addAssertion.</p>
     *
     * @param inAssertion a {@link java.lang.String} object.
     * @param inPath      a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param inType      a {@link net.ihe.gazelle.hl7.assertion.AssertionType} object.
     */
    public void addAssertion(String inAssertion, PathBuilder inPath, AssertionType inType) {
        this.notifications.add(new GazelleHL7Assertion(inAssertion, inPath, inType));
        this.reportCounter++;
    }

    public void addAssertion(String inAssertion, String hl7Path, AssertionType inType) {
        GazelleHL7Assertion assertion = new GazelleHL7Assertion(inAssertion, null, inType);
        assertion.setHl7Path(hl7Path);
        this.notifications.add(assertion);
        this.reportCounter++;
    }

    /**
     * <p>addProfileException.</p>
     *
     * @param message     a {@link java.lang.String} object.
     * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
     * @param value       a {@link java.lang.String} object.
     */
    public void addProfileException(String message, PathBuilder pathBuilder, String value) {
        this.notifications.add(new GazelleProfileException(message, pathBuilder, value));
        this.exceptionCounter++;
    }
    public void addProfileException(String description, String classification, int codeIgamt, String path) {
        this.notifications.add(new GazelleProfileException(description, classification, codeIgamt, path));
        this.exceptionCounter++;
    }

    public void addProfileException(String message, String hl7Path) {
        GazelleProfileException profileException = new GazelleProfileException(message, null, null);
        profileException.setHl7Path(hl7Path);
        this.notifications.add(profileException);
        this.exceptionCounter++;
    }

    /**
     * Returns true if we need to create a new Error, otherwise, we create a warning
     *
     * @param errorCode
     * @return
     */
    private boolean createError(GazelleErrorCode errorCode) {
        switch (errorCode) {
            case LENGTH_ERROR:
                return lengthIsAnError;
            case TABLE_VALUE_NOT_FOUND:
                return notFountValueIsAnError;
            case DATA_TYPE_ERROR:
                return wrongDatatypeIsAnError;
            case SEGMENT_SEQUENCE_ERROR:
                return wrongSequenceIsAnError;
            case CONDITIONAL:
                return false;
            default:
                return true;
        }
    }

    /**
     * <p>Getter for the field <code>notifications</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Object> getNotifications() {
        if (this.notifications == null) {
            this.notifications = new ArrayList<Object>();
        }
        return notifications;
    }

    /**
     * <p>Setter for the field <code>notifications</code>.</p>
     *
     * @param notifications a {@link java.util.List} object.
     */
    public void setNotifications(List<Object> notifications) {
        this.notifications = notifications;
    }

    /**
     * <p>getLastNotification.</p>
     *
     * @return a {@link java.lang.Object} object.
     */
    public Object getLastNotification() {
        if (!this.notifications.isEmpty()) {
            return this.notifications.get(this.notifications.size() - 1);
        } else {
            return null;
        }
    }

    /**
     * <p>printErrors.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String printErrors() {
        StringBuffer errors = new StringBuffer();
        for (Object notification : getNotifications()) {
            if (notification instanceof Error
                    && !((Error) notification).getGazelleErrorCode().equals(GazelleErrorCode.LENGTH_ERROR)
                    && !((Error) notification).getGazelleErrorCode().equals(GazelleErrorCode.TABLE_VALUE_NOT_FOUND)) {
                Error error = (Error) notification;

                errors.append(error.getHl7Path());
                errors.append(" : ");
                errors.append(error.getMessage());
                errors.append("\n");
            }
        }
        return errors.toString();
    }

    /**
     * <p>printProfileException.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String printProfileException() {
        StringBuffer errors = new StringBuffer();
        for (Object notification : getNotifications()) {
            if (notification instanceof GazelleProfileException) {
                GazelleProfileException exception = (GazelleProfileException) notification;
                errors.append(exception.getFailureType());
                errors.append(":");
                errors.append(exception.getDescription());
                errors.append(" (");
                errors.append(exception.getHl7Path());
                errors.append(" )");
                errors.append("<br/>");
            }
        }
        return errors.toString();
    }

    /**
     * <p>Getter for the field <code>errorCounter</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getErrorCounter() {
        return errorCounter;
    }

     /**
     * <p>Getter for the field <code>warningCounter</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getWarningCounter() {
        return warningCounter;
    }

    /**
     * <p>Getter for the field <code>reportCounter</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getReportCounter() {
        return reportCounter;
    }

    /**
     * <p>Getter for the field <code>exceptionCounter</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getExceptionCounter() {
        return exceptionCounter;
    }


}
