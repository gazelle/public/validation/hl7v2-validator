package net.ihe.gazelle.hl7.validation.context;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>ValidationOptions class.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */

@XmlRootElement(name = "ValidationOptions")
@XmlAccessorType(XmlAccessType.NONE)
public class ValidationOptions {

    @XmlElement(name = "MessageStructure")
    private ValidationType messageStructure;

    @XmlElement(name = "Length")
    private ValidationType length;

    @XmlElement(name = "DataType")
    private ValidationType datatype;

    @XmlElement(name = "DataValue")
    private ValidationType dataValue;

    public ValidationOptions() {
    }

    public ValidationType getMessageStructure() {
        return messageStructure;
    }

    public void setMessageStructure(ValidationType messageStructure) {
        this.messageStructure = messageStructure;
    }

    public ValidationType getLength() {
        return length;
    }

    public void setLength(ValidationType length) {
        this.length = length;
    }

    public ValidationType getDatatype() {
        return datatype;
    }

    public void setDatatype(ValidationType datatype) {
        this.datatype = datatype;
    }

    public ValidationType getDataValue() {
        return dataValue;
    }

    public void setDataValue(ValidationType dataValue) {
        this.dataValue = dataValue;
    }
}
