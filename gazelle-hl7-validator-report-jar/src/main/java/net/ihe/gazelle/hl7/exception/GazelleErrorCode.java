package net.ihe.gazelle.hl7.exception;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum GazelleErrorCode {

	@XmlEnumValue("Message accepted")
	MESSAGE_ACCEPTED(0, "Message accepted"),

	@XmlEnumValue("Segment sequence error")
	SEGMENT_SEQUENCE_ERROR(100, "Segment sequence error"),

	@XmlEnumValue("Required field missing")
	REQUIRED_FIELD_MISSING(101, "Required field missing"),

	@XmlEnumValue("Data type error")
	DATA_TYPE_ERROR(102, "Data type error"),

	@XmlEnumValue("Table value not found")
	TABLE_VALUE_NOT_FOUND(103, "Table value not found"),

	@XmlEnumValue("Unsupported message type")
	UNSUPPORTED_MESSAGE_TYPE(200, "Unsupported message type"),

	@XmlEnumValue("Unsupported event code")
	UNSUPPORTED_EVENT_CODE(201, "Unsupported event code"),

	@XmlEnumValue("Unsupported processing id")
	UNSUPPORTED_PROCESSING_ID(202, "Unsupported processing id"),

	@XmlEnumValue("Unsupported version id")
	UNSUPPORTED_VERSION_ID(203, "Unsupported version id"),

	@XmlEnumValue("Unknown key identifier")
	UNKNOWN_KEY_IDENTIFIER(204, "Unknown key identifier"),

	@XmlEnumValue("Duplicate key identifier")
	DUPLICATE_KEY_IDENTIFIER(205, "Duplicate key identifier"),

	@XmlEnumValue("Application record locked")
	APPLICATION_RECORD_LOCKED(206, "Application record locked"),

	@XmlEnumValue("Application internal error")
	APPLICATION_INTERNAL_ERROR(207, "Application internal error"),

	@XmlEnumValue("Element not allowed")
	NOT_ALLOWED(1010, "Element not allowed"),

	@XmlEnumValue("Cardinality mismatch")
	CARDINALITY_ERROR(1012, "Cardinality mismatch"),

	@XmlEnumValue("Length exceeded")
	LENGTH_ERROR(1013, "Length exceeded"),

	@XmlEnumValue("Wrong constant value")
	WRONG_CONSTANT_VALUE(1017, "Wrong constant value"),

	@XmlEnumValue("Wrong usage")
	USAGE(1018, "Wrong usage"),

	@XmlEnumValue("Problem parsing message")
	FORMAT(1019, "Problem parsing message"),

	@XmlEnumValue("Primitive rule not fulfilled")
	PRIMITIVE_RULE(1020, "Primitive rule not fulfilled"),

	@XmlEnumValue("Unknown error type")
	UNKNOWN(1021, "Unknown error type"),

	@XmlEnumValue("Conditional")
	CONDITIONAL (1022, "Conditional"),

	@XmlEnumValue("Warning")
    WARNING(9001, "Warning"),

	////////////////////////////////////////////////////////
	//----GVT SPECIFIC WARNING, ERROR, AND SPEC ERROR----//
	///////////////////////////////////////////////////////
	@XmlEnumValue("Code Not Found")
	CODE_NOT_FOUND(1,"Code Not Found"),
	@XmlEnumValue("EVS")
	EVS(2,"EVS"),
	@XmlEnumValue("Constraint Failure")
	CONSTRAINT_FAILURE(3,"Constraint Failure"),
	@XmlEnumValue("Unresolved Field Model")
	UNRESOLVED_FIELD(4,"Unresolved Field Model"),
	@XmlEnumValue("Coded Element")
	CODED_ELEMENT(5,"Coded Element"),
	@XmlEnumValue("Invalid Content")
	INVALID_CONTENT(6,"Invalid Content"),
	@XmlEnumValue("Unexpected")
	UNEXPECTED(7,"Unexpected"),
	@XmlEnumValue("Predicate Failure")
	PREDICATE_FAILURE(8,"Predicate Failure"),
	@XmlEnumValue("CoConstraint Failure")
	COCONSTRAINT_FAILURE(9,"CoConstraint Failure"),
	@XmlEnumValue("High-level Content Error")
	HIGH_LEVEL_CONTENT_ERROR(10,"High-level Content Error"),
	@XmlEnumValue("Unescaped Separator")
	UNESCAPED_SEPARATOR(11,"Unescaped Separator"),
	@XmlEnumValue("Content Failure")
	CONTENT_FAILURE(12, "Content Failure"),
	@XmlEnumValue("Co-Constraint")
	CO_CONSTRAINT(13,"Co-Constraint"),
	@XmlEnumValue("No type found from GVT")
	DEFAULT(14,"No type found from GVT"),
	@XmlEnumValue("Length")
	LENGTH(15,"Length"),
	@XmlEnumValue("Cardinality")
	CARDINALITY(16,"Cardinality"),
	@XmlEnumValue("VS Not Found")
	VS_NOT_FOUND(17,"VS Not Found"),
	@XmlEnumValue("Binding Location")
	BINDING_LOCATION(18,"Binding Location"),
	@XmlEnumValue("Extra")
	EXTRA(19,"Extra"),
	@XmlEnumValue("Usage")
	USAGE_GVT(20,"Usage"),
	@XmlEnumValue("Format")
	FORMAT_GVT(21, "Format"),
	@XmlEnumValue("Single Code Failure")
	SINGLE_CODE_FAILURE(22, "Single Code Failure"),
	@XmlEnumValue("Constant Value")
	CONSTANT_VALUE(23, "Constant Value"),
	@XmlEnumValue("VS Error")
	VS_ERROR(24,"VS Error"),
	@XmlEnumValue("Length Spec Error")
	LENGTH_SPEC_ERROR(25, "Length Spec Error"),
	@XmlEnumValue("Constraint Spec Error")
	CONSTRAINT_SPEC_ERROR(26, "Constraint Spec Error"),
	@XmlEnumValue("Constant Spec Value")
	CONSTANT_VALUE_SPEC(27, "Constant Value Spec Error"),
	@XmlEnumValue("Content Spec Error")
	CONTENT_SPEC_ERROR(28, "Content Spec Error"),
	@XmlEnumValue("Predicate Spec Error")
	PREDICATE_SPEC_ERROR(29, "Predicate Spec Error"),
	@XmlEnumValue("Constant Spec Value")
	VALUE_SET_BINDING_SPEC_ERROR(30, "Value Set Binding Spec Error"),
	@XmlEnumValue("Duplicate Code")
	DUPLICATE_CODE(31, "Duplicate Code"),
	@XmlEnumValue("Duplicate Code & Code System")
	DUPLICATE_CODE_CODESYSTEM(32, "Duplicate Code & Code System"),
	@XmlEnumValue("Slicing Spec Value")
	SLICING_SPEC_ERROR(33, "Slicing Spec Error"),
	@XmlEnumValue("Co-Constraint Spec Error")
	CO_CONSTRAINT_SPEC_ERROR(34, "Co-Constraint Spec Error");

	////////////////////////////////////////////////////////////////////////
	private static final String HL70357 = "HL70357";
	private final int code;
	private final String message;

	GazelleErrorCode(int errCode, String message) {
		this.code = errCode;
		this.message = message;
	}

	/**
	 * @return the integer error code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return the error code message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the ErrorCode for the given integer
	 * 
	 * @param errCode
	 *            integer error code
	 * @return ErrorCode
	 */
	public static GazelleErrorCode errorCodeFor(int errCode) {
		for (GazelleErrorCode err : GazelleErrorCode.values()) {
			if (err.code == errCode) {
				return err;
			}
		}
		return UNKNOWN;
	}

	/**
	 * @return the HL7 table number
	 */
	public static String codeTable() {
		return HL70357;
	}


}
