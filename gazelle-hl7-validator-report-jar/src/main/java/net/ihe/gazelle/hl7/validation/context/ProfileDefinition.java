package net.ihe.gazelle.hl7.validation.context;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>ProfileDefinition class.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */

@XmlRootElement(name = "ProfileDefinition")
@XmlAccessorType(XmlAccessType.NONE)
public class ProfileDefinition {

    @XmlElement(name = "DomainKeyword")
    private String domainKeyword;

    @XmlElement(name = "ActorKeyword")
    private String actorKeyword;

    @XmlElement(name = "TransactionKeyword")
    private String transactionKeyword;

    @XmlElement(name = "MessageType")
    private String messageType;

    @XmlElement(name = "HL7Version")
    private String hl7Version;

    @XmlElement(name = "MessageOrderControlCode")
    private String messageOrdreControlCode;

    public ProfileDefinition() {
    }

    public String getDomainKeyword() {
        return domainKeyword;
    }

    public void setDomainKeyword(String domainKeyword) {
        this.domainKeyword = domainKeyword;
    }

    public String getActorKeyword() {
        return actorKeyword;
    }

    public void setActorKeyword(String actorKeyword) {
        this.actorKeyword = actorKeyword;
    }

    public String getTransactionKeyword() {
        return transactionKeyword;
    }

    public void setTransactionKeyword(String transactionKeyword) {
        this.transactionKeyword = transactionKeyword;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }

    public String getMessageOrdreControlCode() {
        return messageOrdreControlCode;
    }

    public void setMessageOrdreControlCode(String messageOrdreControlCode) {
        this.messageOrdreControlCode = messageOrdreControlCode;
    }
}
