package net.ihe.gazelle.hl7.assertion;

import net.ihe.gazelle.hl7.utils.PathBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class description <b>GazelleHL7Rule</b>
 * 
 * This class is used to store the rules which are applied to a message during its validation in order to report them to the final user.
 *
 * @author Anne-Gaëlle Bergé / IHE Europe
 * 
 */
@XmlRootElement(name = "Report")
@XmlAccessorType(XmlAccessType.NONE)
public class GazelleHL7Assertion {

	@XmlElement(name = "Description")
	private String assertion;

	@XmlElement(name = "Location")
	private String path;

	@XmlElement(name = "Type")
	private AssertionType type;



	public GazelleHL7Assertion() {

	}

	public GazelleHL7Assertion(String inAssertion, PathBuilder inPath, AssertionType inType) {
		this.assertion = inAssertion;
		if (inPath!=null){
			this.path = inPath.toString();
		}else {
			this.path=null;
		}

		this.type = inType;
	}

	/**
	 * Get the assertion.
	 *
	 * @return The assertion.
     */
	public String getAssertion() {
		return assertion;
	}

	/**
	 * Get the path.
	 *
	 * @return The path.
     */
	public String getPath() {
		return path;
	}

	/**
	 * <p>Setter for the field <code>path</code>.</p>
	 *
	 */
	public void setHl7Path(String hl7Path) {
		this.path = hl7Path;
	}

	/**
	 * Get the assertion type.
	 *
	 * @return The assertion type.
     */
	public AssertionType getType() {
		return type;
	}

	@Override
	public String toString() {
		return this.path + ": " + this.assertion;
	}
}
